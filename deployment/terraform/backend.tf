terraform {
  backend "s3" {
    region              = "${var.aws_region}"
    bucket              = "lamark-ci"
    key                 = "ci/terraform/${var.env_id}_cms.tfstate"
  }
}