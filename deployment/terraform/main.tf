provider "aws" {
  version               = "~> 1.46.0"
  region                = "${var.aws_region}"
  access_key            = "${var.aws_access_key}"
  secret_key            = "${var.aws_secret_key}"
  allowed_account_ids   = ["${var.aws_account_id}"]
}

module "cms_api" {
  source                = "./cms-api"
  role                  = "${aws_iam_role.lambda_basic_execution.arn}"
  env_id                = "${var.env_id}"
  env_name              = "${var.env_name}"
  function_prefix       = "${var.app_cms_prefix}"
  api_gateway_prefix    = "${var.api_gateway_prefix}"
  aws_account_id        = "${var.aws_account_id}"
  aws_region            = "${var.aws_region}"
  api_version           = "${var.api_version}"
  build_number          = "${var.build_number}"
  s3_tools_bucket       = "${var.s3_tools_bucket}"
}

// module "dynamo_db" {
//   source = "./dynamo-db"
//  env_name = "${var.env_name}"
// }
