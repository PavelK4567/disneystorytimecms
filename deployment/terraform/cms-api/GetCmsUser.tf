resource "aws_lambda_function" "get_cms_user" {
  function_name     = "${var.function_prefix}GetCmsUser"
  handler           = "com.lamark.lambda.handlers.GetCmsUserByIDHandler"
  role              = "${var.role}"
  runtime           = "${var.runtime_java8}"
  timeout           = "15"
  memory_size       = "512"

  s3_bucket         = "${aws_s3_bucket_object.api.bucket}"
  s3_key            = "${aws_s3_bucket_object.api.key}"
  s3_object_version = "${aws_s3_bucket_object.api.version_id}"

  environment {
    variables = {
      DISNEY_ENV = "${var.env_name}"
    }
  }
}

# API Gateway
resource "aws_api_gateway_method" "get_cms_user_get" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  resource_id       = "${aws_api_gateway_resource.cms_user_get_cms_user.id}"
  http_method       = "GET"
  authorization     = "NONE"
}

resource "aws_api_gateway_integration" "get_cms_user_get" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  resource_id       = "${aws_api_gateway_method.get_cms_user_get.resource_id}"
  http_method       = "${aws_api_gateway_method.get_cms_user_get.http_method}"
  type              = "AWS"
  uri               = "arn:aws:apigateway:${var.aws_region}:lambda:path/2015-03-31/functions/${aws_lambda_function.get_cms_user.arn}/invocations"
  integration_http_method   = "POST"
}

resource "aws_api_gateway_method_response" "get_cms_user_get" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  resource_id       = "${aws_api_gateway_integration.get_cms_user_get.resource_id}"
  http_method       = "${aws_api_gateway_method.get_cms_user_get.http_method}"
  status_code       = "200"

  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = true
  }
}

resource "aws_api_gateway_integration_response" "get_cms_user_get_response" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  resource_id       = "${aws_api_gateway_integration.get_cms_user_get.resource_id}"
  http_method       = "${aws_api_gateway_method_response.get_cms_user_get.http_method}"
  status_code       = "${aws_api_gateway_method_response.get_cms_user_get.status_code}"

  response_templates  = { "application/json" = "" }
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = "'*'"
  }
}

resource "aws_lambda_permission" "get_cms_user" {
  action            = "lambda:InvokeFunction"
  function_name     = "${aws_lambda_function.get_cms_user.function_name}"
  principal         = "apigateway.amazonaws.com"
  source_arn        = "arn:aws:execute-api:${var.aws_region}:${var.aws_account_id}:${aws_api_gateway_rest_api.kantoo_cms_api.id}/*/GET/cmsUser/{cmsUserId}"
}