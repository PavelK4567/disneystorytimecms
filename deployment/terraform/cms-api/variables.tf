variable "role" {}

variable "env_id" {}
variable "env_name" {}
variable "function_prefix" {}
variable "api_gateway_prefix" {}
variable "aws_account_id" {}
variable "aws_region" {}

variable "runtime_java8" { default = "java8" }
variable "runtime_nodejs8_10" { default = "nodejs8.10" }
variable "runtime_python3_6" { default = "python3.6" }
variable "runtime_python2_7" { default = "python2.7" }


variable "filename_all_jar" { default = "../../original-LamarkCMS-1.0-SNAPSHOT.jar" }

variable "api_version" {}
variable "build_number" {}

variable "s3_tools_bucket" {}


resource "aws_s3_bucket_object" "api" {
  bucket    = "${var.s3_tools_bucket}"
  key       = "ci/deployment/${var.env_id}/lamark-cms-api.jar"
  source    = "${var.filename_all_jar}"
  etag      = "${md5(file("${var.filename_all_jar}"))}"
}