# CMS API
resource "aws_api_gateway_rest_api" "kantoo_cms_api" {
  name              = "${var.api_gateway_prefix}cms-api"
  description       = ""
}

resource "aws_api_gateway_resource" "media" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_rest_api.kantoo_cms_api.root_resource_id}"
  path_part         = "media"
}

resource "aws_api_gateway_resource" "media_edit" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.media.id}"
  path_part         = "edit"
}

resource "aws_api_gateway_resource" "media_edit_batch" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.media_edit.id}"
  path_part         = "batch"
}

resource "aws_api_gateway_resource" "media_filter" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.media.id}"
  path_part         = "filter"
}

resource "aws_api_gateway_resource" "media_info" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.media.id}"
  path_part         = "info"
}

resource "aws_api_gateway_resource" "media_copy" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.media.id}"
  path_part         = "copy"
}

//resource "aws_api_gateway_resource" "media_publish" {
//  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
//  parent_id         = "${aws_api_gateway_resource.media.id}"
//  path_part         = "publish"
//}

resource "aws_api_gateway_resource" "media_info_location" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.media_info.id}"
  path_part         = "location"
}

resource "aws_api_gateway_resource" "metatag" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_rest_api.kantoo_cms_api.root_resource_id}"
  path_part         = "metatag"
}

resource "aws_api_gateway_resource" "metatag_allMetaTags" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.metatag.id}"
  path_part         = "getAllMetaTags"
}

resource "aws_api_gateway_resource" "metatag_searchBy" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.metatag.id}"
  path_part         = "searchBy"
}

resource "aws_api_gateway_resource" "metatag_searchBy_metatagType" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.metatag_searchBy.id}"
  path_part         = "{metatagType}"
}

resource "aws_api_gateway_resource" "metatag_metatagId" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.metatag.id}"
  path_part         = "{metatagId}"
}

resource "aws_api_gateway_resource" "product" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_rest_api.kantoo_cms_api.root_resource_id}"
  path_part         = "product"
}

resource "aws_api_gateway_resource" "product_get_all_products" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.product.id}"
  path_part         = "getAllProducts"
}

resource "aws_api_gateway_resource" "product_lang" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.product.id}"
  path_part         = "{lang}"
}

//resource "aws_api_gateway_resource" "version" {
//  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
//  parent_id         = "${aws_api_gateway_rest_api.kantoo_cms_api.root_resource_id}"
//  path_part         = "version"
//}
//
//resource "aws_api_gateway_resource" "version_module" {
//  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
//  parent_id         = "${aws_api_gateway_resource.version.id}"
//  path_part         = "{module}"
//}


resource "aws_api_gateway_resource" "cms_user" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_rest_api.kantoo_cms_api.root_resource_id}"
  path_part         = "cmsUser"
}

resource "aws_api_gateway_resource" "cms_user_delete_cms_user" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.cms_user.id}"
  path_part         = "deleteCMSUser"
}

resource "aws_api_gateway_resource" "cms_user_get_cms_users" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.cms_user.id}"
  path_part         = "getAllCmsUsers"
}

resource "aws_api_gateway_resource" "cms_user_user_forgot_password" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.cms_user.id}"
  path_part         = "getPassword"
}

resource "aws_api_gateway_resource" "cms_user_login_cms_user" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.cms_user.id}"
  path_part         = "login"
}

resource "aws_api_gateway_resource" "cms_user_get_cms_user" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.cms_user.id}"
  path_part         = "{cmsUserId}"
}

resource "aws_api_gateway_resource" "common_parametars" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_rest_api.kantoo_cms_api.root_resource_id}"
  path_part         = "commonParametars"
}

resource "aws_api_gateway_resource" "common_parametars_get_all_parametars" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.common_parametars.id}"
  path_part         = "getAllParametars"
}

resource "aws_api_gateway_resource" "common_parametars_get_parametar" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.common_parametars.id}"
  path_part         = "{parametarName}"
}

resource "aws_api_gateway_resource" "common_parametars_get_parametar_by_productId" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.common_parametars_get_parametar.id}"
  path_part         = "{productId}"
}

resource "aws_api_gateway_resource" "games" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_rest_api.kantoo_cms_api.root_resource_id}"
  path_part         = "games"
}

resource "aws_api_gateway_resource" "labels" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_rest_api.kantoo_cms_api.root_resource_id}"
  path_part         = "labels"
}

resource "aws_api_gateway_resource" "labels_search" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.labels.id}"
  path_part         = "search"
}

resource "aws_api_gateway_resource" "label_delete" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.labels.id}"
  path_part         = "delete"
}


resource "aws_api_gateway_resource" "labels_search_get" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.labels_search.id}"
  path_part         = "get"
}


resource "aws_api_gateway_resource" "languages" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_rest_api.kantoo_cms_api.root_resource_id}"
  path_part         = "languages"
}

resource "aws_api_gateway_resource" "languages_getAllLanguages" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  parent_id         = "${aws_api_gateway_resource.languages.id}"
  path_part         = "getAllLanguages"
}


resource "aws_api_gateway_deployment" "cms_deployment" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  stage_name        = "${var.env_name}"

  variables = {
    "lambdaAlias"   = "${var.env_name}-${var.build_number}"
    "stageName"     = "${var.env_name}"
  }

  depends_on        = [
    "aws_api_gateway_integration_response.get_cms_users_get_response", 
    "aws_api_gateway_integration_response.get_all_products_response"
  ]

  lifecycle {
    create_before_destroy = true
  }
}
