data "archive_file" "copy_media" {
  type = "zip"
  source_dir = "../../src/main/python/copy-media"
  output_path = "../../dest/copy-media.zip"
}

resource "aws_lambda_function" "media_copy" {
  function_name = "${var.function_prefix}media_copy"
  handler = "lambda_function.lambda_handler"
  filename = "${data.archive_file.copy_media.output_path}"
  source_code_hash = "${data.archive_file.copy_media.output_base64sha256}"
  role = "${var.role}"
  runtime = "${var.runtime_python2_7}"
  timeout = "30"
  memory_size = "512"

  environment {
    variables = {
      BUILD_VERSION = "${var.api_version}.${var.build_number}"
      DISNEY_ENV = "${lower(var.env_name)}"
    }
  }
}

# API Gateway
resource "aws_api_gateway_method" "media_copy_post" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  resource_id       = "${aws_api_gateway_resource.media_copy.id}"
  http_method       = "POST"
  authorization     = "NONE"
}

resource "aws_api_gateway_integration" "media_copy_post" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  resource_id       = "${aws_api_gateway_method.media_copy_post.resource_id}"
  http_method       = "${aws_api_gateway_method.media_copy_post.http_method}"
  type              = "AWS"
  uri               = "arn:aws:apigateway:${var.aws_region}:lambda:path/2015-03-31/functions/${aws_lambda_function.media_copy.arn}/invocations"
  integration_http_method   = "POST"
}

resource "aws_api_gateway_method_response" "media_copy_post" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  resource_id       = "${aws_api_gateway_integration.media_copy_post.resource_id}"
  http_method       = "${aws_api_gateway_method.media_copy_post.http_method}"
  status_code       = "200"

  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = true
  }
}

resource "aws_api_gateway_integration_response" "media_copy_post_response" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  resource_id       = "${aws_api_gateway_integration.media_copy_post.resource_id}"
  http_method       = "${aws_api_gateway_method_response.media_copy_post.http_method}"
  status_code       = "${aws_api_gateway_method_response.media_copy_post.status_code}"

  response_templates  = { "application/json" = "" }
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = "'*'"
  }
}

resource "aws_lambda_permission" "media_copy" {
  action            = "lambda:InvokeFunction"
  function_name     = "${aws_lambda_function.media_copy.function_name}"
  principal         = "apigateway.amazonaws.com"
  source_arn        = "arn:aws:execute-api:${var.aws_region}:${var.aws_account_id}:${aws_api_gateway_rest_api.kantoo_cms_api.id}/*/POST/media/copy"
}
