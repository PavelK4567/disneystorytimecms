resource "aws_lambda_function" "media_filter" {
  function_name     = "${var.function_prefix}GetAllMedia"
  handler           = "com.lamark.lambda.handlers.GetAllMediaHandler"
  role              = "${var.role}"
  runtime           = "${var.runtime_java8}"
  timeout           = "180"
  memory_size       = "1600"

  s3_bucket         = "${aws_s3_bucket_object.api.bucket}"
  s3_key            = "${aws_s3_bucket_object.api.key}"
  s3_object_version = "${aws_s3_bucket_object.api.version_id}"

  environment {
    variables = {
      DISNEY_ENV = "${var.env_name}"
    }
  }
}

# API Gateway
resource "aws_api_gateway_method" "media_filter_post" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  resource_id       = "${aws_api_gateway_resource.media_filter.id}"
  http_method       = "POST"
  authorization     = "NONE"
}

resource "aws_api_gateway_integration" "media_filter_post" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  resource_id       = "${aws_api_gateway_method.media_filter_post.resource_id}"
  http_method       = "${aws_api_gateway_method.media_filter_post.http_method}"
  type              = "AWS"
  uri               = "arn:aws:apigateway:${var.aws_region}:lambda:path/2015-03-31/functions/${aws_lambda_function.media_filter.arn}/invocations"
  integration_http_method   = "POST"
}

resource "aws_api_gateway_method_response" "media_filter_post" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  resource_id       = "${aws_api_gateway_integration.media_filter_post.resource_id}"
  http_method       = "${aws_api_gateway_method.media_filter_post.http_method}"
  status_code       = "200"

  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = true
  }
}

resource "aws_api_gateway_integration_response" "media_filter_post_response" {
  rest_api_id       = "${aws_api_gateway_rest_api.kantoo_cms_api.id}"
  resource_id       = "${aws_api_gateway_integration.media_filter_post.resource_id}"
  http_method       = "${aws_api_gateway_method_response.media_filter_post.http_method}"
  status_code       = "${aws_api_gateway_method_response.media_filter_post.status_code}"

  response_templates  = { "application/json" = "" }
  response_parameters = {
    "method.response.header.Access-Control-Allow-Origin" = "'*'"
  }
}

resource "aws_lambda_permission" "media_filter" {
  action            = "lambda:InvokeFunction"
  function_name     = "${aws_lambda_function.media_filter.function_name}"
  principal         = "apigateway.amazonaws.com"
  source_arn        = "arn:aws:execute-api:${var.aws_region}:${var.aws_account_id}:${aws_api_gateway_rest_api.kantoo_cms_api.id}/*/POST/media/filter"
}