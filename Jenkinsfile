def getWorkspace() {
    pwd().replace("%2F", "_")
}

environment {
    AWS_ACCESS_KEY_ID = credentials('AWS_ACCESS_KEY_ID')
    AWS_SECRET_ACCESS_KEY = credentials('AWS_SECRET_ACCESS_KEY')
    AWS_DEFAULT_REGION = credentials('AWS_DEFAULT_REGION')
}

try {
  node('linux') {
    wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'XTerm', 'defaultFg': 1, 'defaultBg': 2]) {
      ws(getWorkspace()) {
        workspace = pwd()

        deleteDir()

        checkout([
          $class: 'GitSCM',
          branches: scm.branches,
          extensions: scm.extensions + [[$class: 'CheckoutOption', timeout: 60], [$class: 'CloneOption', reference: '', timeout: 60]],
          userRemoteConfigs: scm.userRemoteConfigs
        ])

        stash name: 'java', includes: "**/*"
        stash name: 'python', includes: "src/main/python/**/*"
        stash name: 'deployment', includes: "deployment/**/*"
        stash name: 'cms.api.tests', includes: "src/test/cms-api-tests/**/*"

      }
    }
  }

  stage('Build') {

     parallel(
      'java': {
        node('docker') {
          unstash 'java'

          docker
            .image('maven:3.6-jdk-8-alpine')
            .inside("-v maven-repo:/var/maven/.m2 -u root -e MAVEN_CONFIG=/var/maven/.m2") { c->
              sh "mvn clean install -DskipTests"
              sh "cp target/original-LamarkCMS-1.0-SNAPSHOT.jar ./"
            }
          archiveArtifacts artifacts: '*.jar', fingerprint: true
          sh "sudo chmod 777 original-LamarkCMS-1.0-SNAPSHOT.jar"
          stash name: 'java_dist', includes: "original-LamarkCMS-1.0-SNAPSHOT.jar"
        }
      },

      'python': {
        node('linux') {
        unstash 'python'

          zip zipFile: 'kantoo.api.python.zip', archive: false, dir: 'src/main/python'
          archiveArtifacts artifacts: '*.zip', fingerprint: true
          stash name: 'python_dist', includes: "kantoo.api.python.zip"
        }
      }
    )
  }

  stage('Terraform') {
    node('docker') {
      wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'XTerm', 'defaultFg': 1, 'defaultBg': 2]) {


        unzip zipFile: 'kantoo.api.python.zip', quiet: true

        docker
          .image('hashicorp/terraform')
          .withRun() { c->
            sh 'cd ./deployment && sudo chmod u+x deploy.sh && ./deploy.sh'
          }
      }
    }
  }

  def testsFailed = false

  stage('Integration tests') {
    parallel(

      'cms-api-tests': {
        node('docker') {
          wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'XTerm', 'defaultFg': 1, 'defaultBg': 2]) {

            sh "ls"
            unstash 'cms.api.tests'


            def statusCode = 0
            docker
              .image('node:8.14.1-alpine')
              .withRun() { c->
                statusCode = sh script: 'cd ./src/test/cms-api-tests && sudo chmod u+x run.sh && ./run.sh', returnStatus: true
              }

            testsFailed = testsFailed || (statusCode > 0)

            dir('src/test/cms-api-tests/out') {
              stash name: 'cms.api.tests.report', includes: 'allure-results/**/*'
            }
          }
        }
      }
    )

    // Build test reports
    node('linux') {
      wrap([$class: 'AnsiColorBuildWrapper', 'colorMapName': 'XTerm', 'defaultFg': 1, 'defaultBg': 2]) {

        unstash 'cms.api.tests.report'

        // step([$class: 'CucumberReportPublisher', jsonReportDirectory: "./", jenkinsBasePath: '', fileIncludePattern: '**/*.json'])

        allure([
          includeProperties: false,
          jdk: '',
          properties: [],
          reportBuildPolicy: 'ALWAYS',
          results: [[path: 'allure-results']]
        ])
      }
    }

    if (testsFailed) {
      error "Tests failed!"
    }
  }

  slackSend channel: 'kantoo-ci', message: "*Build success* ${env.JOB_NAME} #${env.BUILD_NUMBER} (<${env.BUILD_URL}|Open>) 🚀", color: "good"
  currentBuild.result = 'SUCCESS'
} catch (e) {
  slackSend channel: 'kantoo-ci', message: "*Build failed* ${env.JOB_NAME} #${env.BUILD_NUMBER} (<${env.BUILD_URL}|Open>)", color: "danger"
  currentBuild.result = 'FAILURE'
  throw e
} finally {
  if (env.TF_VAR_env_name == "prod" || env.TF_VAR_env_name == "qa") {
    node {
      println currentBuild.result  // this prints null
      step([$class: 'Mailer', notifyEveryUnstableBuild: true, recipients: 'shurkhovetskyie@mydigicode.com, denysenkoa@mydigicode.com, sharon.s@la-mark.com, krupinay@mydigicode.com, uri.z@la-mark.com, tatyanach@mydigicode.com, victoria.s@la-mark.com', sendToIndividuals: true])
    }
  }
}
