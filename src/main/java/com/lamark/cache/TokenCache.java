package com.lamark.cache;

import org.cache2k.Cache;
import org.cache2k.Cache2kBuilder;

import java.util.Date;

public class TokenCache {

    public static String TOKEN_KEY="Key";
    public static String TOKEN_EXPIRE_DATE="ExpireDate";
    public static String TOKEN="Token";

    private static Cache<String, Token> tokenCache = new Cache2kBuilder<String, Token>() {
    }.name("Token")
            .eternal(true)
            .entryCapacity(100)
            .build();

    public static void addToCache(String key, String token, Date expireDate) {
        Token t = new Token(key, token, expireDate);
        tokenCache.put(key, t);
    }

    public static Token getFromCache(String key) {
        if (tokenCache.containsKey(key)) {
            Token t = tokenCache.get(key);
            if(t.expireDate.before(new Date())){
                tokenCache.remove(key);
                return null;
            }
            return t;
        }
        return null;

    }

    public static class Token {
        private String token;
        private Date expireDate;

        public Token(String key, String token, Date expireDate) {
            this.token = token;
            this.expireDate = expireDate;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public Date getExpireDate() {
            return expireDate;
        }

        public void setExpireDate(Date expireDate) {
            this.expireDate = expireDate;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Token)) return false;

            Token token1 = (Token) o;

            if (token != null ? !token.equals(token1.token) : token1.token != null) return false;
            return expireDate != null ? expireDate.equals(token1.expireDate) : token1.expireDate == null;
        }

        @Override
        public int hashCode() {
            int result = token != null ? token.hashCode() : 0;
            result = 31 * result + (expireDate != null ? expireDate.hashCode() : 0);
            return result;
        }
    }


}
