package com.lamark.cache;

import java.util.List;

import com.amazonaws.services.lambda.runtime.LambdaLogger;

import com.lamark.models.Game;
import com.lamark.models.Label;
import com.lamark.models.Media;
import com.lamark.interfaces.BasicRepository;

public class CMSCache {
	public static final String LABELS_CACHED = "allLabelCache";
	public static final String GAMES_CACHED = "allGamesCache";
	public static final String MEDIA_CACHED = "allMediaCache";
//    private static Cache<String, List<Label>> labelCache = new Cache2kBuilder<String, List<Label>>() {
//    }.name(LABELS_CACHED)
//            .eternal(true)
//            .entryCapacity(1)
//            .build();;
//    private static Cache<String, List<Game>> gameCache = new Cache2kBuilder<String, List<Game>>() {
//    }.name(GAMES_CACHED)
//            .eternal(true)
//            .entryCapacity(1)
//            .build();
//    private static Cache<String, List<Media>> mediaCache = new Cache2kBuilder<String, List<Media>>() {
//    }.name(MEDIA_CACHED)
//            .eternal(true)
//            .entryCapacity(1)
//            .build();


    public static final Integer DEFAULT_CACHING_TIMEOUT = 180;
    public static final String CACHE_PARAMETAR = "refresh_cache";
    public static final String CACHE_PARAMETAR_VALUE = "true";


    public static List<Label> getLabels(BasicRepository dao, LambdaLogger logger) {
//        if (labelCache == null) {
//            labelCache = new Cache2kBuilder<String, List<Label>>() {
//            }
//                    .name("AllDataListCache")
//                    .expireAfterWrite(DEFAULT_CACHING_TIMEOUT, TimeUnit.HOURS)
//                    .resilienceDuration(30, TimeUnit.SECONDS)
//                    .permitNullValues(true)
//                    .loader(new CacheLoader<String, List<Label>>() {
//                        @Override
//                        public List<Label> load(String s) {
//                            logger.log("Initcializing cache from databse");
//                            logger.log("Reading labels");
//                            List<Label> labels = dao.findAll();
//                            logger.log("found labels " + labels.size());
//                            return labels;
//                        }
//                    })
//                    .build();
//        }
//        return labelCache.get(LABELS_CACHED);
    	List<Label> labels = dao.findAll();
        logger.log("found labels " + labels.size());
        return labels;
    }

//    public static void refreshLabelCache(BasicRepository dao, LambdaLogger logger) {
//        logger.log("Initcializing cache from databse");
//        logger.log("Reading labels");
//        List<Label> labels = dao.findAll();
//        logger.log("found labels " + labels.size());
//        labelCache.put(LABELS_CACHED, labels);
//    }

    public static List<Game> getGames(BasicRepository dao, LambdaLogger logger) {
    	logger.log("Reading games");
        List<Game> games = dao.findAll();
        logger.log("found games " + games.size());
        return games;
//        if (gameCache == null) {
//            gameCache = new Cache2kBuilder<String, List<Game>>() {
//            }
//                    .name("AllGamesCache")
//                    .expireAfterWrite(DEFAULT_CACHING_TIMEOUT, TimeUnit.MINUTES)
//                    .resilienceDuration(30, TimeUnit.SECONDS)
//                    .permitNullValues(true)
//                    .loader(new CacheLoader<String, List<Game>>() {
//                        @Override
//                        public List<Game> load(String s) {
//                            logger.log("Initcializing cache from databse");
//                            logger.log("Reading games");
//                            List<Game> games = dao.findAll();
//                            logger.log("found games " + games.size());
//                            return games;
//                        }
//                    })
//                    .build();
//        }
//        return gameCache.get(GAMES_CACHED);
    }

    public static void refreshGameCache(BasicRepository dao, LambdaLogger logger) {
//        logger.log("Initcializing cache from databse");
//        logger.log("Reading games");
//        List<Game> games = dao.findAll();
//        logger.log("found games " + games.size());
//        gameCache.put(GAMES_CACHED, games);
    }

    public static List<Media> getMedia(BasicRepository dao, LambdaLogger logger) {
        
        logger.log("Reading media");
        
        List<Media> data = dao.findAll();
        
        logger.log("found media " + data.size());
        
    	return data;
//        if (mediaCache.get(MEDIA_CACHED) == null || mediaCache.get(MEDIA_CACHED).size() == 0) {
//            refreshMediaCache(dao, logger);
//        }
//        return mediaCache.get(MEDIA_CACHED);
    }

    public static void refreshMediaCache(BasicRepository dao, LambdaLogger logger) {
//        logger.log("Initcializing cache from databse");
//        logger.log("Reading media");
//        List<Media> data = dao.findAll();
//        logger.log("found media " + data.size());
//        mediaCache.put(MEDIA_CACHED, data);
    }

}
