package com.lamark.repositories;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.lamark.models.Game;

public class GameRepository extends BasicRepositoryModel<Game> {
    public GameRepository(DynamoDBMapper dynamoDBMapper) {
        super(dynamoDBMapper);
    }
}
