package com.lamark.repositories;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.lamark.models.MetaTag;

public class MetaTagRepositoryImpl extends BasicRepositoryModel<MetaTag> {

    public MetaTagRepositoryImpl(DynamoDBMapper dynamoDBMapper) {
        super(dynamoDBMapper);
    }
}
