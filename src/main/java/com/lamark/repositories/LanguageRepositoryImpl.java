package com.lamark.repositories;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.lamark.models.Language;

public class LanguageRepositoryImpl extends BasicRepositoryModel<Language> {

    public LanguageRepositoryImpl(DynamoDBMapper dynamoDBMapper) {
        super(dynamoDBMapper);
    }

}
