package com.lamark.repositories;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.lamark.models.Counter;

public class CounterRepository {

    private final DynamoDBMapper db;

    public CounterRepository(DynamoDBMapper dynamoDBMapper) {
        this.db = dynamoDBMapper;
    }


    public Counter getCounter(String value){
        Counter c= db.load(Counter.class, value);
        if(c==null) {c=saveAndGet(value);}else{
            db.save(c);
        }
        return c;
    }


    public Integer getValue(String value){
        return getCounter(value).getValue();
    }

    public String getPaddedValue(String value, String prefix){
        Counter c=  getCounter(value);
        return c.getPaddedValue(prefix);
    }

    private Counter saveAndGet(String value) {
        Counter c= new Counter(value);
        db.save(c);
        return c;
    }
}
