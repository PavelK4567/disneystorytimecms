package com.lamark.repositories;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.lamark.models.Media;

import java.util.List;
import java.util.stream.Collectors;

public class MediaRepository extends BasicRepositoryModel<Media> {
    public MediaRepository(DynamoDBMapper dynamoDBMapper) {
        super(dynamoDBMapper);
    }

    @Override
    public List<Media> findAll() {
        List<Media> list = super.findAll();
        list = list.stream().distinct().sorted((o1, o2) -> o2.getCreationDate()
                    .compareTo(o1.getCreationDate())).collect(Collectors.toList());
        return list;
    }

}
