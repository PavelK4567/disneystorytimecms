package com.lamark.repositories;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.lamark.models.Product;

public class ProductRepositoryImpl extends BasicRepositoryModel<Product> {

  public ProductRepositoryImpl(DynamoDBMapper dynamoDBMapper) {
    super(dynamoDBMapper);
  }
}
