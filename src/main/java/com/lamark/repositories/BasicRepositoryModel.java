package com.lamark.repositories;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.lamark.interfaces.BasicRepository;

import java.lang.reflect.ParameterizedType;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class BasicRepositoryModel<T> implements BasicRepository<T> {

    protected final DynamoDBMapper dynamoDBMapper;

    public BasicRepositoryModel(DynamoDBMapper dynamoDBMapper) {
        this.dynamoDBMapper = dynamoDBMapper;
    }

    @SuppressWarnings("unchecked")
    private Class<T> getClassOfT() {
        final ParameterizedType type = (ParameterizedType) this.getClass().getGenericSuperclass();
        return (Class<T>) type.getActualTypeArguments()[0];
    }

    public void put(T data) {
        dynamoDBMapper.save(data);
    }

    public T get(String id) {
        return Optional.ofNullable(dynamoDBMapper.load(getClassOfT(), id))
                .orElseThrow(() -> new IllegalArgumentException("Item was not found from table " + getClassOfT() + " with id " + id));
    }

    public T getWithRangeKey(String id, Object rangeKey) {
        return Optional.ofNullable(dynamoDBMapper.load(getClassOfT(), id, rangeKey))
                .orElseThrow(() -> new IllegalArgumentException("Item was not found from table " + getClassOfT() + " with id " + id));
    }

    public List<T> findAll() {
        return dynamoDBMapper.scan(getClassOfT(), new DynamoDBScanExpression());
    }

    public List<T> findByDBCriteria(DynamoDBScanExpression criteria) {
        final List<T> data = dynamoDBMapper.scan(getClassOfT(), criteria);
        return data == null ? Collections.emptyList() : data;
    }

    @Override
    public List<T> findByDBIndex(T model, String indexName) {

        DynamoDBQueryExpression<T> queryExpression =
                new DynamoDBQueryExpression<>();
        queryExpression.withIndexName(indexName);
        queryExpression.withConsistentRead(false);
        queryExpression.withHashKeyValues(model);

        final List<T> data = dynamoDBMapper.query(getClassOfT(), queryExpression);
        return data == null ? Collections.emptyList() : data;
    }

    public void delete(T data) {
        dynamoDBMapper.delete(data);
    }

    @Override
    public T getByCompositeKey(String hash, String range) throws IllegalArgumentException {
        return Optional.ofNullable(dynamoDBMapper.load(getClassOfT(), hash, range))
                .orElseThrow(() -> new IllegalArgumentException(
                        "Item was not found from table " + getClassOfT() + " with hash " + hash + " range " + range));
    }

    public List<T> findByDBCriteria(DynamoDBQueryExpression criteria) {
        final List<T> data = dynamoDBMapper.query(getClassOfT(), criteria);
        return data == null ? Collections.emptyList() : data;
    }

}
