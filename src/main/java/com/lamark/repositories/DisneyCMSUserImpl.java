package com.lamark.repositories;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.lamark.models.DisneyCMSUser;

public class DisneyCMSUserImpl extends BasicRepositoryModel<DisneyCMSUser>{

	public DisneyCMSUserImpl(DynamoDBMapper dynamoDBMapper) {
		super(dynamoDBMapper);
	}

}
