package com.lamark.repositories;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.lamark.models.Parametar;

public class ParametarImpl extends BasicRepositoryModel<Parametar> {

  public ParametarImpl(DynamoDBMapper dynamoDBMapper) {
    super(dynamoDBMapper);
  }

}
