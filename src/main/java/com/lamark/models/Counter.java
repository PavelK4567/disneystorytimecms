package com.lamark.models;

import com.lamark.annotations.DynamoDbTableMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBVersionAttribute;

@DynamoDBTable(tableName = "Counter")
@DynamoDbTableMapper
public class Counter {

    @DynamoDBHashKey(attributeName = "id")
    private String id;

    @DynamoDBVersionAttribute
    private Integer value;

    public Counter() {
    }

    public Counter(String id) {
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public Integer getValue() {
        return value;
    }

    public void setValue(Integer value) {
        this.value = value;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Counter)) return false;

        Counter counter = (Counter) o;

        if (id != null ? !id.equals(counter.id) : counter.id != null) return false;
        return value != null ? value.equals(counter.value) : counter.value == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (value != null ? value.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Counter{" +
                "id='" + id + '\'' +
                ", value=" + value +
                '}';
    }

    public String getPaddedValue(String prefix){
        return prefix+"_"+String.format("%03d", value);

    }
}
