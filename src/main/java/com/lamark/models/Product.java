package com.lamark.models;

import com.lamark.annotations.DynamoDbTableMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.model.StreamViewType;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;

/**
 * The Class Product is a model for Product DDB table, where is defined products with id
 * to_lowercase(trimedNameOfTheProduct-language) (e.g. disneystorytime-en, disneystorytime-pt,
 * disneybaby-ES….).
 * 
 * @author elvira
 */
@DynamoDBTable(tableName = "Product")
@DynamoDbTableMapper(keyStream = true, keyStreamType = StreamViewType.NEW_IMAGE)
public class Product {

  public Product() {
    super();
    creationDate = new Date();
    lastUpdateDate = creationDate;
  }

  @DynamoDBHashKey(attributeName = "id")
  private String id;

  @DynamoDBAttribute(attributeName = "pName")
  private String pName;

  @DynamoDBAttribute(attributeName = "pType")
  private Integer pType;

  @DynamoDBAttribute(attributeName = "lang")
  private String lang;

  @DynamoDBAttribute(attributeName = "pState")
  private Boolean pState;

  @DynamoDBAttribute(attributeName = "pImage")
  private String pImage;

  @DynamoDBAttribute(attributeName = "creationDate")
  @JsonIgnore
  private Date creationDate;

  @DynamoDBAttribute(attributeName = "lastUpdateDate")
  @JsonIgnore
  private Date lastUpdateDate;

  public Product(String pName, String lang, Boolean pState, String pImage, Integer pType) {
    super();
    this.pName = pName;
    this.lang = lang;
    this.pState = pState;
    this.pImage = pImage;
    this.pType = pType;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getpName() {
    return pName;
  }

  public void setpName(String pName) {
    this.pName = pName;
  }

  public Integer getpType() {
    return pType;
  }

  public void setpType(Integer pType) {
    this.pType = pType;
  }

  public String getLang() {
    return lang;
  }

  public void setLang(String lang) {
    this.lang = lang;
  }

  public Boolean getpState() {
    return pState;
  }

  public void setpState(Boolean pState) {
    this.pState = pState;
  }

  public String getpImage() {
    return pImage;
  }

  public void setpImage(String pImage) {
    this.pImage = pImage;
  }

  public Date getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(Date creationDate) {
    this.creationDate = creationDate;
  }

  public Date getLastUpdateDate() {
    return lastUpdateDate;
  }

  public void setLastUpdateDate(Date lastUpdateDate) {
    this.lastUpdateDate = lastUpdateDate;
  }

  @Override
  public String toString() {
    return "Product [id=" + id + ", pName=" + pName + ", lang=" + lang + ", pState=" + pState
        + ", pImage=" + pImage + ", creationDate=" + creationDate + ", lastUpdateDate="
        + lastUpdateDate + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((lastUpdateDate == null) ? 0 : lastUpdateDate.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Product other = (Product) obj;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    if (lastUpdateDate == null) {
        return other.lastUpdateDate == null;
    } else return lastUpdateDate.equals(other.lastUpdateDate);
  }

}
