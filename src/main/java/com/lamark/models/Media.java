package com.lamark.models;

import com.lamark.annotations.DynamoDbTableMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import java.util.Date;
import java.util.List;

@DynamoDBTable(tableName = "Media")
@DynamoDbTableMapper
public class Media {

    public Media() {
        creationDate = new Date();
        lastUpdateDate = creationDate;
    }

    @DynamoDBHashKey(attributeName = "id")
    private String mediaId;

    @DynamoDBAttribute(attributeName = "screenName")
    private String screenName;

    @DynamoDBAttribute(attributeName = "productId")
    private String productId;

    @DynamoDBAttribute(attributeName = "type")
    private String type;

    @DynamoDBAttribute(attributeName = "size")
    private Long size;

    @DynamoDBAttribute(attributeName = "awsRegion")
    private String awsRegion;

    @DynamoDBAttribute(attributeName = "bucketName")
    private String bucketName;

    @DynamoDBAttribute(attributeName = "creationDate")
    private Date creationDate;

    @DynamoDBAttribute(attributeName = "lastUpdateDate")
    private Date lastUpdateDate;

    @DynamoDBAttribute(attributeName = "assigned")
    private boolean assigned = false;

    @DynamoDBAttribute(attributeName = "published")
    private boolean published = false;

    @DynamoDBAttribute(attributeName = "awsUser")
    private String awsUser;

    @DynamoDBAttribute(attributeName = "tags")
    private List<String> tags;

    @DynamoDBAttribute(attributeName = "metadata")
    private List<MetaTagSimple> metadata;


    public String getMediaId() {
        return mediaId;
    }

    public void setMediaId(String mediaId) {
        this.mediaId = mediaId;
    }

    public String getScreenName() {
        return screenName;
    }

    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getAwsRegion() {
        return awsRegion;
    }

    public void setAwsRegion(String awsRegion) {
        this.awsRegion = awsRegion;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public boolean isAssigned() {
        return assigned;
    }

    public void setAssigned(boolean assigned) {
        this.assigned = assigned;
    }

    public boolean isPublished() {
        return published;
    }

    public void setPublished(boolean published) {
        this.published = published;
    }

    public String getAwsUser() {
        return awsUser;
    }

    public void setAwsUser(String awsUser) {
        this.awsUser = awsUser;
    }

    public Long getSize() {
        return size;
    }

    public List<String> getTags() {
        return tags;
    }

    public void setTags(List<String> tags) {
        this.tags = tags;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type.toUpperCase();
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public List<MetaTagSimple> getMetadata() {
        return metadata;
    }

    public void setMetadata(List<MetaTagSimple> metadata) {
        this.metadata = metadata;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Media)) return false;

        Media media = (Media) o;

        if (assigned != media.assigned) return false;
        if (published != media.published) return false;
        if (mediaId != null ? !mediaId.equals(media.mediaId) : media.mediaId != null) return false;
        if (screenName != null ? !screenName.equals(media.screenName) : media.screenName != null) return false;
        if (productId != null ? !productId.equals(media.productId) : media.productId != null) return false;
        if (type != null ? !type.equals(media.type) : media.type != null) return false;
        if (size != null ? !size.equals(media.size) : media.size != null) return false;
        if (awsRegion != null ? !awsRegion.equals(media.awsRegion) : media.awsRegion != null) return false;
        if (bucketName != null ? !bucketName.equals(media.bucketName) : media.bucketName != null) return false;
        if (creationDate != null ? !creationDate.equals(media.creationDate) : media.creationDate != null) return false;
        if (lastUpdateDate != null ? !lastUpdateDate.equals(media.lastUpdateDate) : media.lastUpdateDate != null)
            return false;
        if (awsUser != null ? !awsUser.equals(media.awsUser) : media.awsUser != null) return false;
        return tags != null ? tags.equals(media.tags) : media.tags == null;
    }

    @Override
    public int hashCode() {
        int result = mediaId != null ? mediaId.hashCode() : 0;
        result = 31 * result + (screenName != null ? screenName.hashCode() : 0);
        result = 31 * result + (productId != null ? productId.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (size != null ? size.hashCode() : 0);
        result = 31 * result + (awsRegion != null ? awsRegion.hashCode() : 0);
        result = 31 * result + (bucketName != null ? bucketName.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (lastUpdateDate != null ? lastUpdateDate.hashCode() : 0);
        result = 31 * result + (assigned ? 1 : 0);
        result = 31 * result + (published ? 1 : 0);
        result = 31 * result + (awsUser != null ? awsUser.hashCode() : 0);
        result = 31 * result + (tags != null ? tags.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Media{" +
                "mediaId='" + mediaId + '\'' +
                ", screenName='" + screenName + '\'' +
                ", productId='" + productId + '\'' +
                ", type='" + type + '\'' +
                ", size=" + size +
                ", awsRegion='" + awsRegion + '\'' +
                ", bucketName='" + bucketName + '\'' +
                ", creationDate=" + creationDate +
                ", lastUpdateDate=" + lastUpdateDate +
                ", assigned=" + assigned +
                ", published=" + published +
                ", awsUser='" + awsUser + '\'' +
                ", tags=" + tags +
                ", metadata=" + metadata +
                '}';
    }
}
