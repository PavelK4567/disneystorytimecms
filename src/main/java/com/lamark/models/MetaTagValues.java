package com.lamark.models;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;

/**
 * The Class MetaTagValues is a model related to MetaTag model.
 * 
 * @author elvira
 */
@DynamoDBDocument
public class MetaTagValues {
  @DynamoDBAttribute(attributeName = "vName")
  private String vName;

  @DynamoDBAttribute(attributeName = "active")
  private Boolean active;

  @DynamoDBAttribute(attributeName = "id")
  private Integer id;

  public MetaTagValues() {
    super();
  }

  public MetaTagValues(String vName, Boolean active) {
    super();
    this.vName = vName;
    this.active = active;
  }

  public String getvName() {
    return vName;
  }

  public void setvName(String vName) {
    this.vName = vName;
  }

  public Boolean getActive() {
    return active;
  }

  public void setActive(Boolean active) {
    this.active = active;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((active == null) ? 0 : active.hashCode());
    result = prime * result + ((vName == null) ? 0 : vName.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    MetaTagValues other = (MetaTagValues) obj;
    if (active == null) {
      if (other.active != null)
        return false;
    } else if (!active.equals(other.active))
      return false;
    if (vName == null) {
        return other.vName == null;
    } else return vName.equals(other.vName);
  }

  @Override
  public String toString() {
    return "MetaTagValues [vName=" + vName + ", active=" + active + "]";
  }

}
