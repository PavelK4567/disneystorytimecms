package com.lamark.models;

import com.lamark.annotations.DynamoDbTableMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

/**
 * The Class Language is a model for Language DDB table, where we enter predefined languages
 * manually.
 */
@DynamoDBTable(tableName = "Language")
@DynamoDbTableMapper(keyStream = true, lambdaConnect = "CommonModulesStreamListener")
public class Language {

  @DynamoDBHashKey(attributeName = "id")
  private String id;

  @DynamoDBAttribute(attributeName = "lname")
  private String name;

  @DynamoDBAttribute(attributeName = "origName")
  private String origName;

  @DynamoDBAttribute(attributeName = "shortCode")
  private String shortCode;

  public Language() {
    super();
  }

  public Language(String id, String name, String origName, String shortCode) {
    super();
    this.id = id;
    this.name = name;
    this.origName = origName;
    this.shortCode = shortCode;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getOrigName() {
    return origName;
  }

  public void setOrigName(String origName) {
    this.origName = origName;
  }

  public String getShortCode() {
    return shortCode;
  }

  public void setShortCode(String shortCode) {
    this.shortCode = shortCode;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((name == null) ? 0 : name.hashCode());
    result = prime * result + ((origName == null) ? 0 : origName.hashCode());
    result = prime * result + ((shortCode == null) ? 0 : shortCode.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Language other = (Language) obj;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    if (name == null) {
      if (other.name != null)
        return false;
    } else if (!name.equals(other.name))
      return false;
    if (origName == null) {
      if (other.origName != null)
        return false;
    } else if (!origName.equals(other.origName))
      return false;
    if (shortCode == null) {
      if (other.shortCode != null)
        return false;
    } else if (!shortCode.equals(other.shortCode))
      return false;
    return true;
  }

  @Override
  public String toString() {
    return "Language [id=" + id + ", name=" + name + ", origName=" + origName + ", shortCode="
        + shortCode + "]";
  }

}
