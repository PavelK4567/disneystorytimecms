package com.lamark.models;

import com.lamark.annotations.DynamoDbTableMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBRangeKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.util.Date;

/**
 * Parametar table has a composite key ( hash and range). For java to support datatypes with
 * composite keys, the composite keys must be encapuslated in a separate class, resulting in a
 * single primary key attribute annotated with @Id.
 *
 * @author Lidija Gligorova
 */
@DynamoDBTable(tableName = "Parametar")
@DynamoDbTableMapper
public class Parametar {

  public Parametar() {
    super();
    creationDate = new Date();
    lastUpdateDate = creationDate;
  }

  @DynamoDBHashKey(attributeName = "parametarName")
  private String parametarName;

  @DynamoDBRangeKey(attributeName = "productId")
  private String productId;

  @DynamoDBAttribute(attributeName = "parametarValue")
  private String parametarValue;

  @DynamoDBAttribute(attributeName = "creationDate")
  @JsonIgnore
  private Date creationDate;

  @DynamoDBAttribute(attributeName = "lastUpdateDate")
  @JsonIgnore
  private Date lastUpdateDate;


  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public Date getCreationDate() {
    return creationDate;
  }

  public void setCreationDate(Date creationDate) {
    this.creationDate = creationDate;
  }

  public Date getLastUpdateDate() {
    return lastUpdateDate;
  }

  public void setLastUpdateDate(Date lastUpdateDate) {
    this.lastUpdateDate = lastUpdateDate;
  }


  public String getParametarName() {
    return parametarName;
  }

  public void setParametarName(String parametarName) {
    this.parametarName = parametarName;
  }

  public String getParametarValue() {
    return parametarValue;
  }

  public void setParametarValue(String parametarValue) {
    this.parametarValue = parametarValue;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((lastUpdateDate == null) ? 0 : lastUpdateDate.hashCode());
    result = prime * result + ((parametarName == null) ? 0 : parametarName.hashCode());
    result = prime * result + ((productId == null) ? 0 : productId.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    Parametar other = (Parametar) obj;
    if (lastUpdateDate == null) {
      if (other.lastUpdateDate != null)
        return false;
    } else if (!lastUpdateDate.equals(other.lastUpdateDate))
      return false;
    if (parametarName == null) {
      if (other.parametarName != null)
        return false;
    } else if (!parametarName.equals(other.parametarName))
      return false;
    if (productId == null) {
      return other.productId == null;
    } else
      return productId.equals(other.productId);
  }

  @Override
  public String toString() {
    return "Parametar [parametarName=" + parametarName + ", productId=" + productId
        + ", parametarValue=" + parametarValue + ", creationDate=" + creationDate
        + ", lastUpdateDate=" + lastUpdateDate + "]";
  }

}
