package com.lamark.models;

import com.lamark.annotations.DynamoDbTableMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import java.util.ArrayList;
import java.util.List;

/**
 * The Class MetaTag is a model for MetaTag DDB table, where we enter metadata tags manually.
 * 
 * @author elvira
 */
@DynamoDBTable(tableName = "MetaTag")
@DynamoDbTableMapper(keyStream = true, lambdaConnect = "CommonModulesStreamListener")
public class MetaTag {

  @DynamoDBHashKey(attributeName = "id")
  private String id;

  @DynamoDBAttribute(attributeName = "tName")
  private String tName;

  @DynamoDBAttribute(attributeName = "tType")
  private String tType;

  @DynamoDBAttribute(attributeName = "tValues")
  List<MetaTagValues> tValues = new ArrayList<MetaTagValues>();

  public MetaTag() {
    super();
  }

  public MetaTag(String id, String tName, String tType, List<MetaTagValues> tValues) {
    super();
    this.id = id;
    this.tName = tName;
    this.tType = tType;
    this.tValues = tValues;
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String gettName() {
    return tName;
  }

  public void settName(String tName) {
    this.tName = tName;
  }

  public String gettType() {
    return tType;
  }

  public void settType(String tType) {
    this.tType = tType;
  }

  public List<MetaTagValues> gettValues() {
    return tValues;
  }

  public void settValues(List<MetaTagValues> tValues) {
    this.tValues = tValues;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    result = prime * result + ((tName == null) ? 0 : tName.hashCode());
    result = prime * result + ((tType == null) ? 0 : tType.hashCode());
    result = prime * result + ((tValues == null) ? 0 : tValues.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj)
      return true;
    if (obj == null)
      return false;
    if (getClass() != obj.getClass())
      return false;
    MetaTag other = (MetaTag) obj;
    if (id == null) {
      if (other.id != null)
        return false;
    } else if (!id.equals(other.id))
      return false;
    if (tName == null) {
      if (other.tName != null)
        return false;
    } else if (!tName.equals(other.tName))
      return false;
    if (tType == null) {
      if (other.tType != null)
        return false;
    } else if (!tType.equals(other.tType))
      return false;
    if (tValues == null) {
        return other.tValues == null;
    } else return tValues.equals(other.tValues);
  }

  @Override
  public String toString() {
    return "MetaTag [id=" + id + ", tName=" + tName + ", tType=" + tType + ", tValues=" + tValues
        + "]";
  }

}
