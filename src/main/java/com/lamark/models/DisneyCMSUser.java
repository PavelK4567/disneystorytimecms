package com.lamark.models;

import com.lamark.annotations.DynamoDbTableMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;

import java.util.Calendar;
import java.util.Date;
import java.util.UUID;


@DynamoDBTable(tableName = "DisneyCMSUser")
@DynamoDbTableMapper
public class DisneyCMSUser {

    private CmsUserPremissions permissions;

    public DisneyCMSUser() {
        super();
        id = UUID.randomUUID().toString();
        creationDate = new Date();
        lastUpdateDate = creationDate;

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, 100); // get 100 year in future - hopefully all be dead by then
        passwordExpireDate = cal.getTime();
    }

    @DynamoDBHashKey(attributeName = "id")
    private String id;

    @DynamoDBAttribute(attributeName = "username")
    private String username;

    @DynamoDBAttribute(attributeName = "email")
    private String email;

    @DynamoDBAttribute(attributeName = "token")
    private String token;

    @DynamoDBAttribute(attributeName = "loginToken")
    private int loginToken;

    @DynamoDBAttribute(attributeName = "tokenExparationDate")
    private Date tokenExparationDate;

    @DynamoDBAttribute(attributeName = "password")
    private String password;

    @DynamoDBAttribute(attributeName = "passwordExpireDate")
    private Date passwordExpireDate;

    @DynamoDBAttribute(attributeName = "expirationDate")
    private Date expirationDate;

    @DynamoDBAttribute(attributeName = "creationDate")
    private Date creationDate;

    @DynamoDBAttribute(attributeName = "lastUpdateDate")
    private Date lastUpdateDate;

    @DynamoDBAttribute(attributeName = "avatarLink")
    private String avatarLink;

    @DynamoDBDocument
    public static class CmsUserPremissions {
        @DynamoDBAttribute(attributeName = "products")
        private boolean products;

        @DynamoDBAttribute(attributeName = "tagsAndRules")
        private boolean tagsAndRules;

        @DynamoDBAttribute(attributeName = "labelManager")
        private boolean labelManager;

        @DynamoDBAttribute(attributeName = "userManagement")
        private boolean userManagement;

        @DynamoDBAttribute(attributeName = "publish")
        private boolean publish;


        public boolean isProducts() {
            return products;
        }

        public void setProducts(boolean products) {
            this.products = products;
        }

        public boolean isTagsAndRules() {
            return tagsAndRules;
        }

        public void setTagsAndRules(boolean tagsAndRules) {
            this.tagsAndRules = tagsAndRules;
        }

        public boolean isLabelManager() {
            return labelManager;
        }

        public void setLabelManager(boolean labelManager) {
            this.labelManager = labelManager;
        }

        public boolean isUserManagement() {
            return userManagement;
        }

        public void setUserManagement(boolean userManagement) {
            this.userManagement = userManagement;
        }

        public boolean isPublish() {
            return publish;
        }

        public void setPublish(boolean publish) {
            this.publish = publish;
        }

        @Override
        public String toString() {
            return "CmsUserPremissions{" +
                    "products=" + products +
                    ", tagsAndRules=" + tagsAndRules +
                    ", labelManager=" + labelManager +
                    ", userManagement=" + userManagement +
                    ", publish=" + publish +
                    '}';
        }

        @Override
        public boolean equals(Object o) {
            if (this == o)
                return true;
            if (o == null || getClass() != o.getClass())
                return false;

            CmsUserPremissions that = (CmsUserPremissions) o;

            if (products != that.products)
                return false;
            return tagsAndRules == that.tagsAndRules && labelManager == that.labelManager
                    && userManagement == that.userManagement && publish == that.publish;
        }

        @Override
        public int hashCode() {
            int result = (products ? 1 : 0);
            result = 31 * result + (tagsAndRules ? 1 : 0);
            result = 31 * result + (labelManager ? 1 : 0);
            result = 31 * result + (userManagement ? 1 : 0);
            result = 31 * result + (publish ? 1 : 0);
            return result;
        }
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public String getAvatarLink() {
        return avatarLink;
    }

    public void setAvatarLink(String avatarLink) {
        this.avatarLink = avatarLink;
    }

    public CmsUserPremissions getPermissions() {
        return permissions;
    }

    public void setPermissions(CmsUserPremissions permissions) {
        this.permissions = permissions;
    }

    public Date getPasswordExpireDate() {
        return passwordExpireDate;
    }

    public void setPasswordExpireDate(Date passwordExpireDate) {
        this.passwordExpireDate = passwordExpireDate;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public Date getTokenExparationDate() {
        return tokenExparationDate;
    }

    public void setTokenExparationDate(Date tokenExparationDate) {
        this.tokenExparationDate = tokenExparationDate;
    }

    public int getLoginToken() {
        return loginToken;
    }

    public void setLoginToken(int loginToken) {
        this.loginToken = loginToken;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DisneyCMSUser)) return false;

        DisneyCMSUser that = (DisneyCMSUser) o;

        if (loginToken != that.loginToken) return false;
        if (permissions != null ? !permissions.equals(that.permissions) : that.permissions != null) return false;
        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (username != null ? !username.equals(that.username) : that.username != null) return false;
        if (email != null ? !email.equals(that.email) : that.email != null) return false;
        if (token != null ? !token.equals(that.token) : that.token != null) return false;
        if (tokenExparationDate != null ? !tokenExparationDate.equals(that.tokenExparationDate) : that.tokenExparationDate != null)
            return false;
        if (password != null ? !password.equals(that.password) : that.password != null) return false;
        if (passwordExpireDate != null ? !passwordExpireDate.equals(that.passwordExpireDate) : that.passwordExpireDate != null)
            return false;
        if (expirationDate != null ? !expirationDate.equals(that.expirationDate) : that.expirationDate != null)
            return false;
        if (creationDate != null ? !creationDate.equals(that.creationDate) : that.creationDate != null) return false;
        if (lastUpdateDate != null ? !lastUpdateDate.equals(that.lastUpdateDate) : that.lastUpdateDate != null)
            return false;
        return avatarLink != null ? avatarLink.equals(that.avatarLink) : that.avatarLink == null;
    }

    @Override
    public int hashCode() {
        int result = permissions != null ? permissions.hashCode() : 0;
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (username != null ? username.hashCode() : 0);
        result = 31 * result + (email != null ? email.hashCode() : 0);
        result = 31 * result + (token != null ? token.hashCode() : 0);
        result = 31 * result + loginToken;
        result = 31 * result + (tokenExparationDate != null ? tokenExparationDate.hashCode() : 0);
        result = 31 * result + (password != null ? password.hashCode() : 0);
        result = 31 * result + (passwordExpireDate != null ? passwordExpireDate.hashCode() : 0);
        result = 31 * result + (expirationDate != null ? expirationDate.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (lastUpdateDate != null ? lastUpdateDate.hashCode() : 0);
        result = 31 * result + (avatarLink != null ? avatarLink.hashCode() : 0);
        return result;
    }

    public DisneyCMSUser(DisneyCMSUser clonable) {
        this();
        this.permissions = clonable.getPermissions();
        this.username = clonable.getUsername();
        this.email = clonable.getEmail();
        this.password = clonable.getPassword();
        this.expirationDate = clonable.getExpirationDate();
        this.avatarLink = clonable.getAvatarLink();
    }

    @Override
    public String toString() {
        return "DisneyCMSUser{" +
                "permissions=" + permissions +
                ", id='" + id + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", token='" + token + '\'' +
                ", tokenExparationDate=" + tokenExparationDate +
                ", password='" + password + '\'' +
                ", passwordExpireDate=" + passwordExpireDate +
                ", expirationDate=" + expirationDate +
                ", creationDate=" + creationDate +
                ", lastUpdateDate=" + lastUpdateDate +
                ", avatarLink='" + avatarLink + '\'' +
                '}';
    }


}
