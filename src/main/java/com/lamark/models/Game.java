package com.lamark.models;

import com.lamark.annotations.DynamoDbTableMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.model.StreamViewType;

import java.util.Date;
import java.util.UUID;

@DynamoDBTable(tableName = "Game")
@DynamoDbTableMapper(lambdaConnect = "CacheStreamListenerHandlerImpl", keyStream =true, keyStreamType = StreamViewType.KEYS_ONLY)
public class Game {

    public Game() {
        id = UUID.randomUUID().toString();
        creationDate = new Date();
        lastUpdateDate = creationDate;
    }

    @DynamoDBHashKey(attributeName = "id")
    private String id;

    @DynamoDBAttribute(attributeName = "ltype")
    private String type;

    @DynamoDBAttribute(attributeName = "name")
    private String name;

    @DynamoDBAttribute(attributeName = "creationDate")
    private Date creationDate;

    @DynamoDBAttribute(attributeName = "lastUpdateDate")
    private Date lastUpdateDate;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Game)) return false;

        Game game = (Game) o;

        if (id != null ? !id.equals(game.id) : game.id != null) return false;
        if (type != null ? !type.equals(game.type) : game.type != null) return false;
        if (name != null ? !name.equals(game.name) : game.name != null) return false;
        if (creationDate != null ? !creationDate.equals(game.creationDate) : game.creationDate != null) return false;
        return lastUpdateDate != null ? lastUpdateDate.equals(game.lastUpdateDate) : game.lastUpdateDate == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (lastUpdateDate != null ? lastUpdateDate.hashCode() : 0);
        return result;
    }
}
