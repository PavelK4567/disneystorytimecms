package com.lamark.models;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;

import java.util.List;

@DynamoDBDocument
public class MetaTagSimple {

    @DynamoDBAttribute(attributeName = "tId")
    private String id;

    @DynamoDBAttribute(attributeName = "tName")
    private List<String> tName;

    @DynamoDBAttribute(attributeName = "tValues")
    private List<String> tValues;

    public List<String> gettName() {
        return tName;
    }

    public void settName(List<String> tName) {
        this.tName = tName;
    }

    public List<String> gettValues() {
        return tValues;
    }

    public void settValues(List<String> tValues) {
        this.tValues = tValues;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof MetaTagSimple)) return false;

        MetaTagSimple that = (MetaTagSimple) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (tName != null ? !tName.equals(that.tName) : that.tName != null) return false;
        return tValues != null ? tValues.equals(that.tValues) : that.tValues == null;
    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (tName != null ? tName.hashCode() : 0);
        result = 31 * result + (tValues != null ? tValues.hashCode() : 0);
        return result;
    }
}
