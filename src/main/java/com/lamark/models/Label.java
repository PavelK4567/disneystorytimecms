package com.lamark.models;


import com.lamark.annotations.DynamoDbTableMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBAttribute;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBDocument;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBHashKey;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBTable;
import com.amazonaws.services.dynamodbv2.model.StreamViewType;

import java.util.*;

@DynamoDBTable(tableName = "Label")
@DynamoDbTableMapper(lambdaConnect = "CacheStreamListenerHandlerImpl", keyStream = true, keyStreamType = StreamViewType.KEYS_ONLY)
public class Label {

    private List<Languages> languages;
    //private List<MetaTagSimple> metatags;
    private MetaTagSimple metatags;
    private List<Games> games;
	private List<LabelButtons> buttons;
    private HashMap<String,String> images;
    private List<String> labelUniqueIds;

    public Label() {
        id = UUID.randomUUID().toString();
        creationDate = new Date();
        lastUpdateDate = creationDate;
    }
    
    @DynamoDBAttribute(attributeName="pausePopUp")
    private PausePopUp pausePopUp;

    @DynamoDBAttribute(attributeName="bestScorePopUp")
    private BestScorePopUp bestScorePopUp;
    
    @DynamoDBAttribute(attributeName="summaryPopUp")
    private SummaryPopUp summaryPopUp;
    
    @DynamoDBAttribute(attributeName="genericPopUp")
    private GenericPopUp genericPopUp;

    @DynamoDBAttribute(attributeName="areYouGrownUp")
    private AreYouGrownUpPopUp areYouGrownUp;
    
    @DynamoDBHashKey(attributeName = "id")
    private String id;

    @DynamoDBAttribute(attributeName = "ltype")
    private Integer type;

    @DynamoDBAttribute(attributeName = "lname")
    private String name;

    @DynamoDBAttribute(attributeName = "uniqueID")
    private String uniqueID;

    @DynamoDBAttribute(attributeName = "image1")
    private String image1;

    @DynamoDBAttribute(attributeName = "image2")
    private String image2;

    @DynamoDBAttribute(attributeName = "image3")
    private String image3;

    @DynamoDBAttribute(attributeName = "image4")
    private String image4;

    @DynamoDBAttribute(attributeName = "audio")
    private String audio;

    @DynamoDBAttribute(attributeName = "expirationDate")
    private Date expirationDate;

    @DynamoDBAttribute(attributeName = "creationDate")
    private Date creationDate;

    @DynamoDBAttribute(attributeName = "lastUpdateDate")
    private Date lastUpdateDate;

//    public Label(Label input) {
//        this();
//        this.languages=input.getLanguages();
//        this.metatags=input.getMetatags();
//        this.games=input.getGames();
//        this.pausePopUp=input.getPausePopUp();
//        this.type=input.getType();
//        this.image1=input.getImage1();
//        this.image2=input.getImage2();
//        this.image3=input.getImage3();
//        this.image4=input.getImage4();
//        this.audio=input.getAudio();
//        this.expirationDate=input.getExpirationDate();
//        this.uniqueID=input.getUniqueID();
//        this.name=input.getName();
//        
//
//    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(String uniqueID) {
        this.uniqueID = uniqueID;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public Date getLastUpdateDate() {
        return lastUpdateDate;
    }

    public void setLastUpdateDate(Date lastUpdateDate) {
        this.lastUpdateDate = lastUpdateDate;
    }

    public List<Languages> getLanguages() {
        return languages;
    }

    public void setLanguages(List<Languages> languages) {
        this.languages = languages;
    }

    public MetaTagSimple getMetatags() {
        return metatags;
    }

    public void setMetatags(MetaTagSimple metatags) {
        this.metatags = metatags;
    }

    public List<Games> getGames() {
        return games;
    }

    public void setGames(List<Games> games) {
        this.games = games;
    }

    public String getImage1() {
        return image1;
    }

    public void setImage1(String image1) {
        this.image1 = image1;
    }

    public String getImage2() {
        return image2;
    }

    public void setImage2(String image2) {
        this.image2 = image2;
    }

    public String getImage3() {
        return image3;
    }

    public void setImage3(String image3) {
        this.image3 = image3;
    }

    public String getImage4() {
        return image4;
    }

    public void setImage4(String image4) {
        this.image4 = image4;
    }

    public String getAudio() {
        return audio;
    }

    public void setAudio(String audio) {
        this.audio = audio;
    }

    public List<LabelButtons> getButtons() {
        return buttons;
    }

    public void setButtons(List<LabelButtons> buttons) {
        this.buttons = buttons;
    }

    public HashMap<String, String> getImages() {
        return images;
    }

    public void setImages(HashMap<String, String> images) {
        this.images = images;
    }

    public List<String> getLabelUniqueIds() {
        return labelUniqueIds;
    }

    public void setLabelUniqueIds(List<String> labelUniqueIds) {
        this.labelUniqueIds = labelUniqueIds;
    }
    
    public PausePopUp getPausePopUp() {
		return pausePopUp;
	}

	public void setPausePopUp(PausePopUp pausePopUp) {
		this.pausePopUp = pausePopUp;
	}
	
	public BestScorePopUp getBestScorePopUp() {
		return bestScorePopUp;
	}

	public void setBestScorePopUp(BestScorePopUp bestScorePopUp) {
		this.bestScorePopUp = bestScorePopUp;
	}

	public SummaryPopUp getSummaryPopUp() {
		return summaryPopUp;
	}

	public void setSummaryPopUp(SummaryPopUp summaryPopUp) {
		this.summaryPopUp = summaryPopUp;
	}
	
	public GenericPopUp getGenericPopUp() {
		return genericPopUp;
	}

	public void setGenericPopUp(GenericPopUp genericPopUp) {
		this.genericPopUp = genericPopUp;
	}

    public AreYouGrownUpPopUp getAreYouGrownUp() {
        return areYouGrownUp;
    }

    public void setAreYouGrownUp(AreYouGrownUpPopUp areYouGrownUp) {
        this.areYouGrownUp = areYouGrownUp;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Label)) return false;

        Label label = (Label) o;

        if (languages != null ? !languages.equals(label.languages) : label.languages != null) return false;
        if (metatags != null ? !metatags.equals(label.metatags) : label.metatags != null) return false;
        if (games != null ? !games.equals(label.games) : label.games != null) return false;
        if (pausePopUp != null ? !pausePopUp.equals(label.pausePopUp) : label.pausePopUp != null) return false;
        if (bestScorePopUp != null ? !bestScorePopUp.equals(label.bestScorePopUp) : label.bestScorePopUp != null) return false;
        if (summaryPopUp != null ? !summaryPopUp.equals(label.summaryPopUp) : label.summaryPopUp != null) return false;
        if (genericPopUp != null ? !genericPopUp.equals(label.genericPopUp) : label.genericPopUp != null) return false;
        if (id != null ? !id.equals(label.id) : label.id != null) return false;
        if (type != null ? !type.equals(label.type) : label.type != null) return false;
        if (name != null ? !name.equals(label.name) : label.name != null) return false;
        if (uniqueID != null ? !uniqueID.equals(label.uniqueID) : label.uniqueID != null) return false;
        if (image1 != null ? !image1.equals(label.image1) : label.image1 != null) return false;
        if (image2 != null ? !image2.equals(label.image2) : label.image2 != null) return false;
        if (audio != null ? !audio.equals(label.audio) : label.audio != null) return false;
        if (expirationDate != null ? !expirationDate.equals(label.expirationDate) : label.expirationDate != null)
            return false;
        if (creationDate != null ? !creationDate.equals(label.creationDate) : label.creationDate != null) return false;
        return lastUpdateDate != null ? lastUpdateDate.equals(label.lastUpdateDate) : label.lastUpdateDate == null;
    }

    @Override
    public int hashCode() {
        int result = languages != null ? languages.hashCode() : 0;
        result = 31 * result + (metatags != null ? metatags.hashCode() : 0);
        result = 31 * result + (games != null ? games.hashCode() : 0);
        result = 31 * result + (pausePopUp != null ? pausePopUp.hashCode() : 0);
        result = 31 * result + (bestScorePopUp != null ? bestScorePopUp.hashCode() : 0);
        result = 31 * result + (summaryPopUp != null ? summaryPopUp.hashCode() : 0);
        result = 31 * result + (genericPopUp != null ? genericPopUp.hashCode() : 0);
        result = 31 * result + (id != null ? id.hashCode() : 0);
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (uniqueID != null ? uniqueID.hashCode() : 0);
        result = 31 * result + (image1 != null ? image1.hashCode() : 0);
        result = 31 * result + (image2 != null ? image2.hashCode() : 0);
        result = 31 * result + (audio != null ? audio.hashCode() : 0);
        result = 31 * result + (expirationDate != null ? expirationDate.hashCode() : 0);
        result = 31 * result + (creationDate != null ? creationDate.hashCode() : 0);
        result = 31 * result + (lastUpdateDate != null ? lastUpdateDate.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Label{" +
                "languages=" + languages +
                ", metatags=" + metatags +
                ", games=" + games +
                ", pausePopUp=" + pausePopUp +
                ", bestScorePopUp=" + bestScorePopUp +
                ", summaryPopUp=" + summaryPopUp +
                ", genericPopUp=" + genericPopUp +
                ", id='" + id + '\'' +
                ", type=" + type +
                ", name='" + name + '\'' +
                ", uniqueID='" + uniqueID + '\'' +
                ", image1='" + image1 + '\'' +
                ", image2='" + image2 + '\'' +
                ", image3='" + image3 + '\'' +
                ", image4='" + image4 + '\'' +
                ", audio='" + audio + '\'' +
                ", expirationDate=" + expirationDate +
                ", creationDate=" + creationDate +
                ", lastUpdateDate=" + lastUpdateDate +
                '}';
    }

    @DynamoDBDocument
    public static class Games {
        @DynamoDBHashKey(attributeName = "id")
        private String id;

        @DynamoDBAttribute(attributeName = "name")
        private String name;


        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Games)) return false;

            Games games = (Games) o;

            if (id != null ? !id.equals(games.id) : games.id != null) return false;
            return name != null ? name.equals(games.name) : games.name == null;
        }

        @Override
        public int hashCode() {
            int result = id != null ? id.hashCode() : 0;
            result = 31 * result + (name != null ? name.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "Games{" +
                    "id='" + id + '\'' +
                    ", name='" + name + '\'' +
                    '}';
        }
    }

    @DynamoDBDocument
    public static class Languages {
        @DynamoDBAttribute(attributeName = "id")
        private String id;

        @DynamoDBAttribute(attributeName = "shortCode")
        private String shortCode;

        @DynamoDBAttribute(attributeName = "text")
        private String text;

        public String isId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getShortCode() {
            return shortCode;
        }

        public void setShortCode(String shortCode) {
            this.shortCode = shortCode;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof Languages)) return false;

            Languages languages = (Languages) o;

            if (id != null ? !id.equals(languages.id) : languages.id != null) return false;
            if (shortCode != null ? !shortCode.equals(languages.shortCode) : languages.shortCode != null) return false;
            return text != null ? text.equals(languages.text) : languages.text == null;
        }

        @Override
        public int hashCode() {
            int result = id != null ? id.hashCode() : 0;
            result = 31 * result + (shortCode != null ? shortCode.hashCode() : 0);
            result = 31 * result + (text != null ? text.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "Languages{" +
                    "id='" + id + '\'' +
                    ", shortCode='" + shortCode + '\'' +
                    ", text='" + text + '\'' +
                    '}';
        }
    }

    @DynamoDBDocument
    public static class LabelImages {
        @DynamoDBAttribute(attributeName = "name")
        private String name;

        @DynamoDBAttribute(attributeName = "image")
        private String image;

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof LabelImages)) return false;

            LabelImages that = (LabelImages) o;

            if (name != null ? !name.equals(that.name) : that.name != null) return false;
            return image != null ? image.equals(that.image) : that.image == null;
        }

        @Override
        public int hashCode() {
            int result = name != null ? name.hashCode() : 0;
            result = 31 * result + (image != null ? image.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "LabelImages{" +
                    "name='" + name + '\'' +
                    ", image='" + image + '\'' +
                    '}';
        }
    }

    @DynamoDBDocument
    public static class LabelButtons {
        @DynamoDBAttribute(attributeName = "bgWide")
        private LabelImages bgWide;

        @DynamoDBAttribute(attributeName = "bgClassic")
        private LabelImages bgClassic;

        public LabelImages getBgWide() {
            return bgWide;
        }

        public void setBgWide(LabelImages bgWide) {
            this.bgWide = bgWide;
        }

        public LabelImages getBgClassic() {
            return bgClassic;
        }

        public void setBgClassic(LabelImages bgClassic) {
            this.bgClassic = bgClassic;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof LabelButtons)) return false;

            LabelButtons that = (LabelButtons) o;

            if (bgWide != null ? !bgWide.equals(that.bgWide) : that.bgWide != null) return false;
            return bgClassic != null ? bgClassic.equals(that.bgClassic) : that.bgClassic == null;
        }

        @Override
        public int hashCode() {
            int result = bgWide != null ? bgWide.hashCode() : 0;
            result = 31 * result + (bgClassic != null ? bgClassic.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
            return "LabelButtons{" +
                    "bgWide=" + bgWide +
                    ", bgClassic=" + bgClassic +
                    '}';
        }
    }

    @DynamoDBDocument
    public static class PausePopUp {
    	@DynamoDBAttribute(attributeName = "labels")
        private Map<String, Object> labels = new HashMap<String, Object>();

        @DynamoDBAttribute(attributeName = "images")
        private Map<String,String> images;
        
        @DynamoDBAttribute(attributeName = "audio")
        private Map<String, Object> audio = new HashMap<String, Object>();
        
        
        public Map<String,String> getImages() {
			return images;
		}
		public void setImages(Map<String,String> images) {
			this.images = images;
		}
		public Map<String, Object> getLabels() {
			return labels;
		}
		public void setLabels(Map<String, Object> labels) {
			this.labels = labels;
			if(this.labels == null) {
				this.labels = new HashMap<String, Object>();
			}
		}
		public Map<String, Object> getAudio() {
			return audio;
		}
		public void setAudio(Map<String, Object> audio) {
			this.audio = audio;
			if(this.audio == null) {
				this.audio = new HashMap<String, Object>();
			}
		}
		@Override
        public int hashCode() {
        	int result = labels != null ? labels.hashCode() : 0;
        	result = images != null ? images.hashCode() : result;
        	result = audio != null ? audio.hashCode() : result;
        	result = 31 * result + (audio != null ? audio.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
    		return "PopupPause{" +
        		"labels=" + Arrays.toString(labels.entrySet().toArray()) + ',' +
        		"images=" + Arrays.toString(images.entrySet().toArray()) + ',' +
        		"audio=" + Arrays.toString(audio.entrySet().toArray()) + ',' +
        		'}';
        }
    }

    @DynamoDBDocument
    public static class BestScorePopUp {
    	@DynamoDBAttribute(attributeName = "labels")
        private Map<String,String> labels;

        @DynamoDBAttribute(attributeName = "images")
        private Map<String,String> images;
        
        @DynamoDBAttribute(attributeName = "audio")
        private Map<String, Object> audio = new HashMap<String, Object>();
        
        public Map<String,String> getImages() {
			return images;
		}
		public void setImages(Map<String,String> images) {
			this.images = images;
		}
		public Map<String,String> getLabels() {
			return labels;
		}
		public void setLabels(Map<String,String> labels) {
			this.labels = labels;
		}
		public Map<String, Object> getAudio() {
			return audio;
		}
		public void setAudio(Map<String, Object> audio) {
			this.audio = audio;
			if(this.audio == null) {
				this.audio = new HashMap<String, Object>();
			}
		}
		@Override
        public int hashCode() {
        	int result = labels != null ? labels.hashCode() : 0;
        	result = images != null ? images.hashCode() : result;
        	result = audio != null ? audio.hashCode() : result;
        	result = 31 * result + (audio != null ? audio.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
    		return "BestScorePopUp{" +
        		"labels=" + Arrays.toString(labels.entrySet().toArray()) + ',' +
        		"images=" + Arrays.toString(images.entrySet().toArray()) + ',' +
        		"audio=" + Arrays.toString(audio.entrySet().toArray()) + ',' +
        		'}';
        }
    }
    
    @DynamoDBDocument
    public static class SummaryPopUp {
    	@DynamoDBAttribute(attributeName = "labels")
        private Map<String,String> labels;

        @DynamoDBAttribute(attributeName = "images")
        private Map<String,String> images;
        
        @DynamoDBAttribute(attributeName = "audio")
        private Map<String,String> audio;
        
        public Map<String,String> getImages() {
			return images;
		}
		public void setImages(Map<String,String> images) {
			this.images = images;
		}
		public Map<String,String> getLabels() {
			return labels;
		}
		public void setLabels(Map<String,String> labels) {
			this.labels = labels;
		}
		public Map<String,String> getAudio() {
			return audio;
		}
		public void setAudio(Map<String,String> audio) {
			this.audio = audio;
		}
		@Override
        public int hashCode() {
        	int result = labels != null ? labels.hashCode() : 0;
        	result = images != null ? images.hashCode() : result;
        	result = audio != null ? audio.hashCode() : result;
        	result = 31 * result + (audio != null ? audio.hashCode() : 0);
            return result;
        }

        @Override
        public String toString() {
    		return "SummaryPopUp{" +
        		"labels=" + Arrays.toString(labels.entrySet().toArray()) + ',' +
        		"images=" + Arrays.toString(images.entrySet().toArray()) + ',' +
        		"audio=" + Arrays.toString(audio.entrySet().toArray()) + ',' +
        		'}';
        }
    }
    
    @DynamoDBDocument
    public static class GenericPopUp {
        private List<Map<String,String>> texts;
        private List<Map<String,String>> buttons;
        private Map<String,String> images;

        @DynamoDBAttribute(attributeName = "texts")
		public List<Map<String,String>> getTexts() {
			return texts;
		}
		public void setTexts(List<Map<String,String>> texts) {
			this.texts = texts;
		}
		@DynamoDBAttribute(attributeName = "buttons")
		public List<Map<String,String>> getButtons() {
			return buttons;
		}
		public void setButtons(List<Map<String,String>> buttons) {
			this.buttons = buttons;
		}
		@DynamoDBAttribute(attributeName = "images")
		public Map<String,String> getImages() {
			return images;
		}
		public void setImages(Map<String,String> images) {
			this.images = images;
		}
		@Override
        public int hashCode() {
        	int result = texts != null ? texts.hashCode() : 0;
        	result = buttons != null ? buttons.hashCode() : result;
        	result = images != null ? images.hashCode() : result;
        	result = 31 * result + result;
            return result;
        }

        @Override
        public String toString() {
    		return "GenericPopUp{" +
        		"texts=" + texts + ',' +
        		"buttons=" + buttons + ',' +
        		"images=" + Arrays.toString(images.entrySet().toArray()) + ',' +
        		'}';
        }
    }

    @DynamoDBDocument
    public static class AreYouGrownUpPopUp {

        @DynamoDBAttribute(attributeName = "background")
        private Background background;

        @DynamoDBAttribute(attributeName = "btnClose")
        private BtnClose btnClose;

        @DynamoDBAttribute(attributeName = "text")
        private String text;

        @DynamoDBAttribute(attributeName = "errorText")
        private String errorText;

        @DynamoDBAttribute(attributeName = "correctAnswerID")
        private String correctAnswerID;

        @DynamoDBAttribute(attributeName = "answers")
        private List<Answer> answers;


        public AreYouGrownUpPopUp() {
        }

        public Background getBackground() {
            return background;
        }

        public void setBackground(Background background) {
            this.background = background;
        }

        public BtnClose getBtnClose() {
            return btnClose;
        }

        public void setBtnClose(BtnClose btnClose) {
            this.btnClose = btnClose;
        }

        public String getText() {
            return text;
        }

        public void setText(String text) {
            this.text = text;
        }

        public String getErrorText() {
            return errorText;
        }

        public void setErrorText(String errorText) {
            this.errorText = errorText;
        }

        public String getCorrectAnswerID() {
            return correctAnswerID;
        }

        public void setCorrectAnswerID(String correctAnswerID) {
            this.correctAnswerID = correctAnswerID;
        }

        public List<Answer> getAnswers() {
            return answers;
        }

        public void setAnswers(List<Answer> answers) {
            this.answers = answers;
        }

        @Override
        public String toString() {
            return "AreYouGrownUpPopUp{" +
                    "background=" + background +
                    ", btnClose=" + btnClose +
                    ", text='" + text + '\'' +
                    ", errorText='" + errorText + '\'' +
                    ", correctAnswerID='" + correctAnswerID + '\'' +
                    '}';
        }
    }

    @DynamoDBDocument
    public static class BtnClose {

        @DynamoDBAttribute(attributeName = "ID")
        private String ID;

        @DynamoDBAttribute(attributeName = "targetPageID")
        private String targetPageID;

        @DynamoDBAttribute(attributeName = "image")
        private Image image;

        public BtnClose() {
        }

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public String getTargetPageID() {
            return targetPageID;
        }

        public void setTargetPageID(String targetPageID) {
            this.targetPageID = targetPageID;
        }

        public Image getImage() {
            return image;
        }

        public void setImage(Image image) {
            this.image = image;
        }

        @Override
        public String toString() {
            return "BtnClose{" +
                    "ID='" + ID + '\'' +
                    ", targetPageID='" + targetPageID + '\'' +
                    ", image=" + image +
                    '}';
        }
    }

    @DynamoDBDocument
    public static class Background {

        @DynamoDBAttribute(attributeName = "staticImage")
        private String staticImage;

        @DynamoDBAttribute(attributeName = "animatedImage")
        private AnimatedImage animatedImage;

        public Background() {
        }

        public String getStaticImage() {
            return staticImage;
        }

        public void setStaticImage(String staticImage) {
            this.staticImage = staticImage;
        }

        public AnimatedImage getAnimatedImage() {
            return animatedImage;
        }

        public void setAnimatedImage(AnimatedImage animatedImage) {
            this.animatedImage = animatedImage;
        }

        @Override
        public String toString() {
            return "Background{" +
                    "staticImage='" + staticImage + '\'' +
                    ", animatedImage=" + animatedImage +
                    '}';
        }
    }

    @DynamoDBDocument
    public static class Answer {
        @DynamoDBAttribute(attributeName = "ID")
        private String ID;

        @DynamoDBAttribute(attributeName = "targetPageID")
        private String targetPageID;

        @DynamoDBAttribute(attributeName = "image")
        private Image image;

        public Answer() {
        }

        public String getID() {
            return ID;
        }

        public void setID(String ID) {
            this.ID = ID;
        }

        public String getTargetPageID() {
            return targetPageID;
        }

        public void setTargetPageID(String targetPageID) {
            this.targetPageID = targetPageID;
        }

        public Image getImage() {
            return image;
        }

        public void setImage(Image image) {
            this.image = image;
        }

        @Override
        public String toString() {
            return "Answer{" +
                    "ID='" + ID + '\'' +
                    ", targetPageID='" + targetPageID + '\'' +
                    ", image=" + image +
                    '}';
        }
    }

    @DynamoDBDocument
    public static class Image {

        @DynamoDBAttribute(attributeName = "staticImage")
        private String staticImage;

        @DynamoDBAttribute(attributeName = "animatedImage")
        private AnimatedImage animatedImage;

        public Image() {
        }

        public String getStaticImage() {
            return staticImage;
        }

        public void setStaticImage(String staticImage) {
            this.staticImage = staticImage;
        }

        public AnimatedImage getAnimatedImage() {
            return animatedImage;
        }

        public void setAnimatedImage(AnimatedImage animatedImage) {
            this.animatedImage = animatedImage;
        }

        @Override
        public String toString() {
            return "Image{" +
                    "staticImage='" + staticImage + '\'' +
                    ", animatedImage=" + animatedImage +
                    '}';
        }
    }

    @DynamoDBDocument
    public static class AnimatedImage {
        @DynamoDBAttribute(attributeName = "high")
        private String high;

        @DynamoDBAttribute(attributeName = "medium")
        private String medium;

        @DynamoDBAttribute(attributeName = "low")
        private String low;

        public AnimatedImage() {
        }

        public String getHigh() {
            return high;
        }

        public void setHigh(String high) {
            this.high = high;
        }

        public String getMedium() {
            return medium;
        }

        public void setMedium(String medium) {
            this.medium = medium;
        }

        public String getLow() {
            return low;
        }

        public void setLow(String low) {
            this.low = low;
        }

        @Override
        public String toString() {
            return "AnimatedImage{" +
                    "high='" + high + '\'' +
                    ", medium='" + medium + '\'' +
                    ", low='" + low + '\'' +
                    '}';
        }
    }


}
