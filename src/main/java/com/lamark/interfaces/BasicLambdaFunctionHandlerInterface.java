package com.lamark.interfaces;


public interface BasicLambdaFunctionHandlerInterface<T> {
	
	T generateResponse();

}
