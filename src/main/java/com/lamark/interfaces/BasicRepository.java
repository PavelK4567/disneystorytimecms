package com.lamark.interfaces;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBQueryExpression;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;

import java.util.List;

public interface BasicRepository<T>
{

    void put(T user);

    T get(String id) throws IllegalArgumentException;
    
    T getWithRangeKey(String id, Object rangeKey) throws IllegalArgumentException;

    List<T> findAll();

    List<T> findByDBCriteria(DynamoDBScanExpression criteria);

    List<T> findByDBIndex(T model, String indexName);

    void delete(T data);

    T getByCompositeKey(String id, String secondId) throws IllegalArgumentException;

    List<T> findByDBCriteria(DynamoDBQueryExpression criteria);


}
