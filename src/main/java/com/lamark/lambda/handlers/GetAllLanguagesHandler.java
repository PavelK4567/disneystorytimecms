package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.lambda.handlers.handlers.impl.GetAllLanguagesHandlerImpl;
import com.lamark.models.Language;
import com.lamark.global.ApplicationContext;

import java.util.List;

public class GetAllLanguagesHandler implements RequestHandler<Object, List<Language>> {

  @Override
  public List<Language> handleRequest(Object input, Context context) {
    return new GetAllLanguagesHandlerImpl(input, new ApplicationContext(context)).generateResponse();
  }
}
