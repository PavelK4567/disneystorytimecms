package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.lambda.handlers.handlers.impl.GetAllMetaTagsHandlerImpl;
import com.lamark.models.MetaTag;
import com.lamark.global.ApplicationContext;

import java.util.List;

/**
 * The Class GetAllMetaTagsHandler is a lambda handler for reading all metadata tags from DDB.
 * 
 * @author elvira
 */
public class GetAllMetaTagsHandler implements RequestHandler<Object, List<MetaTag>> {

  @Override
  public List<MetaTag> handleRequest(Object input, Context context) {
    return new GetAllMetaTagsHandlerImpl(input, new ApplicationContext(context)).getResponse();
  }
}
