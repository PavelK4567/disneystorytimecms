package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.lambda.handlers.handlers.impl.GetMetaTagByIdHandlerImpl;
import com.lamark.lambda.handlers.requests.ApiRequest;
import com.lamark.models.MetaTag;
import com.lamark.global.ApplicationContext;

/**
 * The Class GetMetaTagsByIdHandler is a lambda handler for reading metadata tag by Id from DDB.
 * 
 * @author elvira
 */
// public class GetMetaTagsByIdHandler implements RequestHandler<HashMap<String, String>, MetaTag> {
public class GetMetaTagByIdHandler implements RequestHandler<ApiRequest, MetaTag> {

  @Override
  // public MetaTag handleRequest(HashMap<String, String> input, Context context) {
  public MetaTag handleRequest(ApiRequest input, Context context) {
    // return new GetMetaTagsByIdImpl(input.get("id"), new ApplicationContext(context))
    return new GetMetaTagByIdHandlerImpl(input.getInputArgument(), new ApplicationContext(context))
        .getResponse();
  }
}
