package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.lambda.handlers.handlers.impl.GetMetaTagByTypeHandlerImpl;
import com.lamark.lambda.handlers.requests.ApiRequest;
import com.lamark.models.MetaTag;
import com.lamark.global.ApplicationContext;

import java.util.List;

/**
 * The Class GetMetaTagByTypeHandler is a lambda handler for reading list of metadata tags by type
 * from DDB.
 * 
 * @author elvira
 */
public class GetMetaTagByTypeHandler implements RequestHandler<ApiRequest, List<MetaTag>> {

  @Override
  public List<MetaTag> handleRequest(ApiRequest input, Context context) {
    return new GetMetaTagByTypeHandlerImpl(input.getInputArgument(), new ApplicationContext(context))
        .getResponse();
  }
}
