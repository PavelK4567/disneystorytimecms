package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.global.ApplicationContext;
import com.lamark.lambda.handlers.handlers.impl.DeleteCmsUserByIdHandlerImpl;

import java.util.HashMap;

public class DeleteCmsUserByIdHandler implements RequestHandler<HashMap<String, String>, String> {

    @Override
    public String handleRequest(HashMap<String, String> input, Context context) {
        context.getLogger().log("Running DeleteCmsUserByIdHandlerImpl with Input: " + input);
        return new DeleteCmsUserByIdHandlerImpl(input.get("id"),new ApplicationContext(context)).generateResponse();
    }

}