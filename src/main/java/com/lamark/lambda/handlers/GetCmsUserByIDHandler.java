package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.lambda.handlers.handlers.impl.GetCmsUserByIDHandlerImpl;
import com.lamark.models.DisneyCMSUser;
import com.lamark.global.ApplicationContext;

import java.util.HashMap;

public class GetCmsUserByIDHandler implements RequestHandler<HashMap<String, String>, DisneyCMSUser> {

    @Override
    public DisneyCMSUser handleRequest(HashMap<String, String> input, Context context) {
        context.getLogger().log("Running GetCmsUserByIDHandlerImpl with Input: " + input);
        return new GetCmsUserByIDHandlerImpl(input.get("id"), new ApplicationContext(context)).getResponse();
    }

}
