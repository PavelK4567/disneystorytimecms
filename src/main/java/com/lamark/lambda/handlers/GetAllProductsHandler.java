package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.lambda.handlers.handlers.impl.GetAllProductsHandlerImpl;
import com.lamark.dto.ProductDTO;
import com.lamark.global.ApplicationContext;

import java.util.List;

public class GetAllProductsHandler implements RequestHandler<Object, List<ProductDTO>> {

  @Override
  public List<ProductDTO> handleRequest(Object input, Context context) {
    // context.getLogger().log("invoke arn: " + context.getInvokedFunctionArn());
    return new GetAllProductsHandlerImpl(input, new ApplicationContext(context)).getResponse();
  }
}
