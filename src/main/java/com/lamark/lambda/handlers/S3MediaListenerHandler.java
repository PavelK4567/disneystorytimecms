package com.lamark.lambda.handlers;

import java.util.Date;
import java.util.Objects;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import com.lamark.filecreator.MediaIndex;
import com.lamark.repositories.MediaRepository;
import com.lamark.s3.S3AccessRigthManager;
import com.lamark.models.Media;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;

public class S3MediaListenerHandler implements RequestHandler<S3Event, String> {

	private AmazonS3 s3 = AmazonS3ClientBuilder.standard().build();
    private BasicRepository<Media> dao;

    @Override
    public String handleRequest(S3Event event, Context context) {
        context.getLogger().log("Received event: " + event);
        try {
        	String result = createDynamoDBData(event, new ApplicationContext(context));
            String bucketName=event.getRecords().get(0).getS3().getBucket().getName();
            String key=event.getRecords().get(0).getS3().getObject().getKey();
            context.getLogger().log("Going to set ACL for item: " + bucketName + "/" + key + ", where latest action was: " + result);
            if (result.equals("new") || result.equals("update")) {
            	context.getLogger().log("Set ACL for item: " + bucketName + "/" + key);
                S3AccessRigthManager sarm = new S3AccessRigthManager(s3);
                sarm.grantPublicReadAccess(bucketName, key);
            }
            return "200";
        } catch (Exception e) {
            e.printStackTrace();
            context.getLogger().log("Error getting object %s from bucket %s. Make sure they exist and"
                    + " your bucket is in the same region as this function." + event);
            throw e;
        }
    }
    
    private String createDynamoDBData(S3Event input, ApplicationContext context) {
    	dao = new MediaRepository(context.getDynamoDbMapper());
        String event = input.getRecords().get(0).getEventName();
        context.logger.log("Event type registered "+event);
        ObjectMapper mapper =  new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        try {
			context.logger.log(mapper.writeValueAsString(input));
		} catch (JsonProcessingException e1) {
			e1.printStackTrace();
		}
        dao = new MediaRepository(context.getDynamoDbMapper());
        String[] data=input.getRecords().get(0).getS3().getObject().getKey().split("/");
        Media media=null;
        String mediaId=input.getRecords().get(0).getS3().getObject().getKey();
        String result="";

        if (event.startsWith("ObjectCreated:")) {
            try {
                media = dao.get(mediaId);
                media.setLastUpdateDate(new Date());
                media.setAwsUser(input.getRecords().get(0).getUserIdentity().getPrincipalId());
                result="update";
            } catch (IllegalArgumentException e) {
                media = new Media();
    			context.logger.log("creating new media with id "+mediaId);
                media.setBucketName(input.getRecords().get(0).getS3().getBucket().getName());
                media.setAwsUser(input.getRecords().get(0).getUserIdentity().getPrincipalId());
                media.setMediaId(mediaId);
                media.setType(data[1]);
                media.setProductId(data[0]);
                media.setScreenName(data[2]);
                media.setSize(input.getRecords().get(0).getS3().getObject().getSizeAsLong());
    			context.logger.log("new media created "+mediaId);
                result="new";
            }
            dao.put(media);
        } else if (event.equals("ObjectRemoved:Delete")) {
            context.logger.log("Deleting file "+mediaId);
            media = dao.get(mediaId);
            result="delete";
            dao.delete(media);
        }
        try{
            MediaIndex index = new MediaIndex();
            index.createIndexFile(dao,context, Objects.requireNonNull(media).getProductId());
        }catch (Exception e){
            context.logger.log("Index file creation failed");
        }
        return result;
    }
}