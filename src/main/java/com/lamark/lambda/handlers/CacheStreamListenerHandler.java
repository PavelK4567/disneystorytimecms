package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent;
import com.lamark.lambda.handlers.handlers.impl.CacheStreamListenerHandlerImpl;
import com.lamark.global.ApplicationContext;


public class CacheStreamListenerHandler implements RequestHandler<DynamodbEvent, Integer> {

  public Integer handleRequest(DynamodbEvent event, Context context) {
    context.getLogger().log("Received event: " + event);

    return new CacheStreamListenerHandlerImpl<>(event, new ApplicationContext(context))
        .generateResponse();
  }
}

