package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.lambda.handlers.handlers.impl.PutCmsUserHandlerImpl;
import com.lamark.models.DisneyCMSUser;
import com.lamark.global.ApplicationContext;

public class PutCmsUserHandler implements RequestHandler<DisneyCMSUser, String> {

	public String handleRequest(DisneyCMSUser input, Context context) {
        context.getLogger().log("Running PutCmsUserHandlerImpl with Input: " + input);
		return  new PutCmsUserHandlerImpl(input, new ApplicationContext(context)).getResponse();
	}

}
