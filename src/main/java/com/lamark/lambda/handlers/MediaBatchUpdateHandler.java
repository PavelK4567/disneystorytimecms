package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.lambda.handlers.handlers.impl.MediaBatchUpdateHandlerImpl;
import com.lamark.lambda.handlers.requests.MediaUpdateRequest;
import com.lamark.global.ApplicationContext;

public class MediaBatchUpdateHandler implements RequestHandler<MediaUpdateRequest, String> {

    @Override
    public String handleRequest(MediaUpdateRequest input, Context context) {
        context.getLogger().log("MediaBatchUpdateHandlerImpl Input: " + input);
        return new MediaBatchUpdateHandlerImpl(input,new ApplicationContext(context)).getResponse();
    }
}
