package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.facades.SimpleLabel;
import com.lamark.lambda.handlers.handlers.impl.GetAllLabelsHandlerImpl;
import com.lamark.lambda.handlers.responses.SearchResponse;
import com.lamark.global.ApplicationContext;

import java.util.HashMap;

public class GetAllLabelsHandler implements RequestHandler<HashMap<String, Object>, SearchResponse<SimpleLabel>> {

    @Override
    public  SearchResponse handleRequest(HashMap<String, Object> input, Context context) {
        context.getLogger().log("GetLabelsByRyleHandler Input: " + input);
        return new GetAllLabelsHandlerImpl(input,new ApplicationContext(context)).getResponse();
    }

}
