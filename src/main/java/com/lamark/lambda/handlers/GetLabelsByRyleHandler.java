package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.lambda.handlers.handlers.impl.GetLablesByRuleHandlerImpl;
import com.lamark.models.Label;
import com.lamark.global.ApplicationContext;

import java.util.HashMap;
import java.util.List;

public class GetLabelsByRyleHandler implements RequestHandler<HashMap<String, Object>, List<Label>> {

    @Override
    public  List<Label> handleRequest(HashMap<String, Object> input, Context context) {
        context.getLogger().log("GetLabelsByRyleHandler Input: " + input);
        return new GetLablesByRuleHandlerImpl(input,new ApplicationContext(context)).getResponse();
    }
}
