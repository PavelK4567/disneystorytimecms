package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.facades.SimpleMedia;
import com.lamark.lambda.handlers.handlers.impl.PutMediaHandlerImpl;
import com.lamark.global.ApplicationContext;

public class PutMediaHandler implements RequestHandler<SimpleMedia, String> {

    @Override
    public String handleRequest(SimpleMedia input, Context context) {
        context.getLogger().log("PutLabelHandlerImpl Input: " + input);
        return new PutMediaHandlerImpl(input,new ApplicationContext(context)).getResponse();
    }
}
