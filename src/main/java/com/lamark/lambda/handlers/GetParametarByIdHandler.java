package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.lambda.handlers.handlers.impl.GetParametarByIdHandlerImpl;
import com.lamark.lambda.handlers.requests.ParametarRequest;
import com.lamark.models.Parametar;
import com.lamark.global.ApplicationContext;

public class GetParametarByIdHandler implements RequestHandler<ParametarRequest, Parametar> {

  @Override
  public Parametar handleRequest(ParametarRequest input, Context context) {
    return new GetParametarByIdHandlerImpl(input, new ApplicationContext(context))
        .getResponse();
  }

}
