package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.lambda.handlers.handlers.impl.GetAllMediaHandlerImpl;
import com.lamark.lambda.handlers.responses.SearchResponse;
import com.lamark.global.ApplicationContext;

import java.util.HashMap;

public class GetAllMediaHandler implements RequestHandler<HashMap<String, String>, SearchResponse<GetAllMediaHandler>> {

    @Override
    public SearchResponse handleRequest(HashMap<String, String> input, Context context) {
        context.getLogger().log("GetAllMediaHandlerImpl Input: " + input);

        ApplicationContext applicationContext = new ApplicationContext(context);

        if (input != null && input.containsKey("WARM_UP_INVOCATION__")) {
            context.getLogger().log("Warm up");
            return null;
        }

        return new GetAllMediaHandlerImpl(input, applicationContext).getResponse();
    }

}
