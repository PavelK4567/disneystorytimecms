package com.lamark.lambda.handlers.requests;

public class DeleteLabelRequest {

    private String id;

    public DeleteLabelRequest() {
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "DeleteLabelRequest{" +
                "id='" + id + '\'' +
                '}';
    }
}
