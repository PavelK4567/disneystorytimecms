package com.lamark.lambda.handlers.requests;

/**
 * The Class ApiRequest is for general use for input arguments.
 * 
 * @author elvira
 */
public class ApiRequest {
  private String inputArgument;

  public ApiRequest() {
    super();
  }

  public ApiRequest(String inputArgument) {
    super();
    this.inputArgument = inputArgument;
  }

  public String getInputArgument() {
    return inputArgument;
  }

  public void setInputArgument(String inputArgument) {
    this.inputArgument = inputArgument;
  }

}
