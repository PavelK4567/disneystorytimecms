package com.lamark.lambda.handlers.requests;

import java.util.List;

public class MediaUpdateRequest {
    public static final String ASSIGNED="assigned";
    public static final String PUBLISHED="published";

    String field;
    List<String> keys;

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public List<String> getKeys() {
        return keys;
    }

    public void setKeys(List<String> keys) {
        this.keys = keys;
    }
}
