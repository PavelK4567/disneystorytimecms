package com.lamark.lambda.handlers.requests;

public class ParametarRequest {

  private String parametarName;
  private String productId;

  public ParametarRequest() {
    super();
  }

  public ParametarRequest(String parametarName, String productId) {
    super();
    this.parametarName = parametarName;
    this.productId = productId;
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getParametarName() {
    return parametarName;
  }

  public void setParametarName(String parametarName) {
    this.parametarName = parametarName;
  }

}
