package com.lamark.lambda.handlers.responses;

import java.util.List;

public class SearchResponse<T> {

    private Integer currentPage;
    private Integer maxPage;
    private Integer size;
    private List<T> data;

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

    public Integer getMaxPage() {
        return maxPage;
    }

    public void setMaxPage(Integer maxPage) {
        this.maxPage = maxPage;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public List<T> getData() {
        return data;
    }

    public void setData(List<T> data) {
        this.data = data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SearchResponse)) return false;

        SearchResponse<?> that = (SearchResponse<?>) o;

        if (currentPage != null ? !currentPage.equals(that.currentPage) : that.currentPage != null) return false;
        if (maxPage != null ? !maxPage.equals(that.maxPage) : that.maxPage != null) return false;
        if (size != null ? !size.equals(that.size) : that.size != null) return false;
        return data != null ? data.equals(that.data) : that.data == null;
    }

    @Override
    public int hashCode() {
        int result = currentPage != null ? currentPage.hashCode() : 0;
        result = 31 * result + (maxPage != null ? maxPage.hashCode() : 0);
        result = 31 * result + (size != null ? size.hashCode() : 0);
        result = 31 * result + (data != null ? data.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SearchResponse{" +
                "currentPage=" + currentPage +
                ", maxPage=" + maxPage +
                ", size=" + size +
                ", data=" + data +
                '}';
    }
}
