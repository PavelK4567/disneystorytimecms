package com.lamark.lambda.handlers.responses;



public class DeleteLabelResponse extends GeneralLambdaResponse {

    private int isLabelDeleted;

    public DeleteLabelResponse(String message, int statusCode, String status, int isLabelDeleted) {
        super(message, statusCode, status);
        this.isLabelDeleted = isLabelDeleted;
    }

    public int getIsLabelDeleted() {
        return isLabelDeleted;
    }

    public void setIsLabelDeleted(int isLabelDeleted) {
        this.isLabelDeleted = isLabelDeleted;
    }

    @Override
    public String toString() {
        return "DeleteLabelResponse{" +
                "isLabelDeleted=" + isLabelDeleted +
                '}';
    }
}
