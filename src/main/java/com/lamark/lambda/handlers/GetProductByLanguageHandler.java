package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.lambda.handlers.handlers.impl.GetProductByLanguageHandlerImpl;
import com.lamark.lambda.handlers.requests.ApiRequest;
import com.lamark.models.Product;
import com.lamark.global.ApplicationContext;

import java.util.List;

/**
 * The Class GetProductByLanguageHandler is a Lambda function for reading product by language.
 * 
 * @author elvira
 */
public class GetProductByLanguageHandler implements RequestHandler<ApiRequest, List<Product>> {

  @Override
  public List<Product> handleRequest(ApiRequest input, Context context) {
    return new GetProductByLanguageHandlerImpl(input, new ApplicationContext(context)).getResponse();
  }
}
