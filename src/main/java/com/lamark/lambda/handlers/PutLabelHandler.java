package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.lambda.handlers.handlers.impl.PutLabelHandlerImpl;
import com.lamark.models.Label;
import com.lamark.global.ApplicationContext;

public class PutLabelHandler implements RequestHandler<Label, String> {

    @Override
    public String handleRequest(Label input, Context context) {
        context.getLogger().log("PutLabelHandlerImpl Input: " + input);
        return new PutLabelHandlerImpl(input,new ApplicationContext(context)).getResponse();
    }
}
