package com.lamark.lambda.handlers.errors;

public class PasswordValidationException extends RuntimeException {

    private static final long serialVersionUID = 1L;

    public PasswordValidationException(String message) {
        super(message);
    }


}
