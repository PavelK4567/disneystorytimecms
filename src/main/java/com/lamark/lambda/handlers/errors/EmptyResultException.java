package com.lamark.lambda.handlers.errors;

public class EmptyResultException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  public EmptyResultException(String message) {
    super(message);
  }


}
