package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.lambda.handlers.handlers.impl.GetAllGamesHandlerImpl;
import com.lamark.models.Game;
import com.lamark.global.ApplicationContext;

import java.util.HashMap;
import java.util.List;

public class GetAllGamesHandler implements RequestHandler<HashMap<String, String>, List<Game>> {

    @Override
    public List<Game> handleRequest(HashMap<String, String> input, Context context) {
        context.getLogger().log("Running GetAllGamesHandlerImpl Input: " + input);

        ApplicationContext applicationContext = new ApplicationContext(context);

        if (input != null && input.containsKey("WARM_UP_INVOCATION__")) {
            context.getLogger().log("Warm up");
            return null;
        }

        return new GetAllGamesHandlerImpl(input, applicationContext).getResponse();
    }
}
