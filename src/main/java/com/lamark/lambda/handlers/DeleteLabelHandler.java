package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.lambda.handlers.requests.DeleteLabelRequest;
import com.lamark.lambda.handlers.responses.DeleteLabelResponse;
import com.lamark.repositories.LabelRepository;
import com.lamark.models.Label;
import com.lamark.utils.Constants;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;

public class DeleteLabelHandler implements RequestHandler<DeleteLabelRequest, DeleteLabelResponse> {

    private BasicRepository<Label> labelsDAO;

    @Override
    public DeleteLabelResponse handleRequest(DeleteLabelRequest input, Context context) {
        context.getLogger().log("Running delete label with input = " + input.toString());

        ApplicationContext applicationContext = new ApplicationContext(context);


        if (input == null) {
            return new DeleteLabelResponse("Input = null", Constants.BAD_REQUEST_STATUS_CODE, Constants.BAD_REQUEST_STATUS, 0);
        }

        if (input.getId() == null || input.getId().isEmpty()) {
            return new DeleteLabelResponse("id = null", Constants.BAD_REQUEST_STATUS_CODE, Constants.BAD_REQUEST_STATUS, 0);
        }

        labelsDAO = new LabelRepository(applicationContext.getDynamoDbMapper());
        Label label = null;
        try {
            label = labelsDAO.get(input.getId());
        } catch (IllegalArgumentException e) {
            return new DeleteLabelResponse("Cannot find label with id = " + input.getId(), Constants.NOT_FOUND_STATUS_CODE, Constants.NOT_FOUND_STATUS, 0);

        }

        try {
            labelsDAO.delete(label);
        } catch (Exception e) {
            return new DeleteLabelResponse("Could not delete label with id = " + input.getId() + " ." + e.getMessage(),
                    Constants.INTERNAL_SERVER_ERROR_STATUS_CODE, Constants.INTERNAL_SERVER_ERROR_STATUS, 0);
        }

        return new DeleteLabelResponse("Label with id = " + input.getId() + "was deleted",
                Constants.OK_STATUS_CODE, Constants.OK_STATUS, 1);
    }
}
