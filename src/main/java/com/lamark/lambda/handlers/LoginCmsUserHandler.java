package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.invoke.LambdaFunction;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.global.ApplicationContext;
import com.lamark.cache.TokenCache;
import com.lamark.facades.CMSUSerData;
import com.lamark.lambda.handlers.handlers.impl.LoginCmsUserHandlerImpl;

import java.util.HashMap;

public class LoginCmsUserHandler implements RequestHandler<HashMap<String, String>, CMSUSerData> {

    @Override
    public CMSUSerData handleRequest(HashMap<String, String> input, Context context) {
        context.getLogger().log("Running LoginCmsUserHandlerImpl with Input: " + input);
        CMSUSerData data = new LoginCmsUserHandlerImpl(input, new ApplicationContext(context)).getResponse();
        HashMap<String, String> response = createResponse(data);
        TokenCacheService service = LambdaInvokerFactory.builder()
                .lambdaClient(AWSLambdaClientBuilder.defaultClient()).build(TokenCacheService.class);
        service.refreshCache(response);
        return data;
    }

    private HashMap<String, String> createResponse(CMSUSerData data) {
        HashMap<String, String> param = new HashMap<>();
        param.put(TokenCache.TOKEN_KEY, data.getID());
        param.put(TokenCache.TOKEN_EXPIRE_DATE, String.valueOf(data.getLoginTokenExparationDate().getTime()));
        param.put(TokenCache.TOKEN, String.valueOf(data.getLoginToken()));
        param.put("UPDATE", "true");
        return param;
    }

    public interface TokenCacheService {
        @LambdaFunction(functionName = "ValidateToken")
        void refreshCache(HashMap<String, String> input);
    }

}
