package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.lambda.handlers.handlers.impl.GetAllLanguagesHandlerImpl;
import com.lamark.models.Language;
import com.lamark.global.ApplicationContext;

import java.util.HashMap;
import java.util.List;

public class GetListOfLanguagesHandler implements RequestHandler<Object, HashMap<String, List<Language>>> {

  @Override
  public HashMap<String, List<Language>>handleRequest(Object input, Context context) {
    HashMap<String, List<Language>> result= new HashMap<>();
    result.put("languagesList" ,new GetAllLanguagesHandlerImpl(input, new ApplicationContext(context)).generateResponse());
    return result;
  }
}
