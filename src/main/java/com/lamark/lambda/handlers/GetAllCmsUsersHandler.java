package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.global.ApplicationContext;
import com.lamark.facades.SimpleCMSUser;
import com.lamark.lambda.handlers.handlers.impl.GetAllCmsUsersHandlerImpl;

import java.util.List;

public class GetAllCmsUsersHandler implements RequestHandler<Object, List<SimpleCMSUser>> {

	public List<SimpleCMSUser> handleRequest(Object input, Context context) {
        context.getLogger().log("Running GetAllCmsUsersHandlerImpl with Input: " + input);
		//noinspection unchecked
		return new GetAllCmsUsersHandlerImpl(input, new ApplicationContext(context)).getResponse();
	}

}
