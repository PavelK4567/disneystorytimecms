package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.lambda.handlers.handlers.impl.GetMediLocationHandlerImpl;
import com.lamark.global.ApplicationContext;

public class GetMediaLocationHandler implements RequestHandler<Object, String> {

    @Override
    public String handleRequest(Object input, Context context) {

        //return ConfigLoader.getProperty("S3_BUCKET_MEDIA_LOCATION");
        return  new GetMediLocationHandlerImpl(input,new ApplicationContext(context)).getResponse();
    }
}
