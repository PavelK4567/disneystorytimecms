package com.lamark.lambda.handlers.handlers.impl;

import com.lamark.repositories.MetaTagRepositoryImpl;
import com.lamark.models.MetaTag;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;

import java.security.InvalidParameterException;

/**
 * The Class GetMetaTagsByIdImpl is a implementation class with business logic for reading the
 * Metadata tag by Id.
 * 
 * @author elvira
 */
public class GetMetaTagByIdHandlerImpl extends BasicLambdaFunctionHandlerImpl<String, MetaTag> {

  private BasicRepository<MetaTag> dao;

  public GetMetaTagByIdHandlerImpl(String input, ApplicationContext context) {
    super(input, context);
  }

  @Override
  public MetaTag generateResponse() {
    if (input == null || input.isEmpty())
      throw new InvalidParameterException("Invalid argument supplied");
    dao = new MetaTagRepositoryImpl(context.getDynamoDbMapper());
    MetaTag tag = dao.get(input);
    // context.logger.log("metadata found with data " + tag);
    return tag;
  }
}
