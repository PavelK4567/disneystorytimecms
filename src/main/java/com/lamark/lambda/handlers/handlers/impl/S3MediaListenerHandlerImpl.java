package com.lamark.lambda.handlers.handlers.impl;

import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import com.lamark.filecreator.MediaIndex;
import com.lamark.repositories.MediaRepository;
import com.lamark.models.Media;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;

import java.util.Date;
import java.util.Objects;

public class S3MediaListenerHandlerImpl extends BasicLambdaFunctionHandlerImpl<S3Event, String> {

    BasicRepository<Media> dao;

    public S3MediaListenerHandlerImpl(S3Event input, ApplicationContext applicationContext) {
        super(input, applicationContext);
    }

    @Override
    public String generateResponse() {
        String event = input.getRecords().get(0).getEventName();
        context.logger.log("Event type registered "+event);
        ObjectMapper mapper =  new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);
        try {
			context.logger.log(mapper.writeValueAsString(input));
		} catch (JsonProcessingException e1) {
			e1.printStackTrace();
		}
        dao = new MediaRepository(context.getDynamoDbMapper());
        String[] data=input.getRecords().get(0).getS3().getObject().getKey().split("/");
        Media media=null;
        String mediaId=input.getRecords().get(0).getS3().getObject().getKey();
        String result="";

        if (event.startsWith("ObjectCreated:")) {
            try {
                media = dao.get(mediaId);
                media.setLastUpdateDate(new Date());
                media.setAwsUser(input.getRecords().get(0).getUserIdentity().getPrincipalId());
                result="update";
            } catch (IllegalArgumentException e) {
                media = new Media();
    			context.logger.log("creating new media with id "+mediaId);
                media.setBucketName(input.getRecords().get(0).getS3().getBucket().getName());
                media.setAwsUser(input.getRecords().get(0).getUserIdentity().getPrincipalId());
                media.setMediaId(mediaId);
                media.setType(data[1]);
                media.setProductId(data[0]);
                media.setScreenName(data[2]);
                media.setSize(input.getRecords().get(0).getS3().getObject().getSizeAsLong());
    			context.logger.log("new media created "+mediaId);
                result="new";
            }
            dao.put(media);
        } else if (event.equals("ObjectRemoved:Delete")) {
            context.logger.log("Deleting file "+mediaId);
            media = dao.get(mediaId);
            result="delete";
            dao.delete(media);
        }
        try{
            MediaIndex index = new MediaIndex();
            index.createIndexFile(dao,context, Objects.requireNonNull(media).getProductId());
        }catch (Exception e){
            context.logger.log("Index file creation failed");
        }
        return result;
    }

}
