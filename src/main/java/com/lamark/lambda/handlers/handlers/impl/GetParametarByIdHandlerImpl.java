package com.lamark.lambda.handlers.handlers.impl;

import com.lamark.lambda.handlers.errors.EmptyResultException;
import com.lamark.lambda.handlers.requests.ParametarRequest;
import com.lamark.repositories.ParametarImpl;
import com.lamark.models.Parametar;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;

import java.util.Optional;

public class GetParametarByIdHandlerImpl
    extends BasicLambdaFunctionHandlerImpl<ParametarRequest, Parametar> {


  public GetParametarByIdHandlerImpl(ParametarRequest input,
                                     ApplicationContext applicationContext) {
    super(input, applicationContext);
  }

  @Override
  public Parametar generateResponse() {
    BasicRepository<Parametar> dao = new ParametarImpl(context.getDynamoDbMapper());
    Parametar parametar = dao.getByCompositeKey(input.getParametarName(),
        input.getProductId() != null && input.getProductId().trim().length() > 0
            ? input.getProductId() : "0");

    return Optional.ofNullable(parametar)
        .orElseThrow(() -> new EmptyResultException("no data found"));
  }

}
