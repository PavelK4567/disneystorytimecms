package com.lamark.lambda.handlers.handlers.impl;

import com.lamark.cache.CMSCache;
import com.lamark.commons.SearchCommons;
import com.lamark.repositories.GameRepository;
import com.lamark.models.Game;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class GetAllGamesHandlerImpl extends BasicLambdaFunctionHandlerImpl<HashMap<String, String>, List<Game>> {

    private BasicRepository<Game> dao;

    public GetAllGamesHandlerImpl(HashMap<String, String> input, ApplicationContext applicationContext) {
        super(input, applicationContext);
    }

    @Override
    public List<Game> generateResponse() {
        if (input == null || input.isEmpty()) throw new IllegalArgumentException();
        //if (input.containsKey(CMSCache.CACHE_PARAMETAR) && input.get(CMSCache.CACHE_PARAMETAR).equals(CMSCache.CACHE_PARAMETAR_VALUE)) {
        //	context.logger.log("cache refresh");
        //	CMSCache.refreshGameCache(dao, context.getContext().getLogger());
        //	if(!input.containsKey("predicament")) return Collections.EMPTY_LIST;
        //}
        //context.logger.log("cache refresh");
    	//CMSCache.refreshGameCache(dao, context.getContext().getLogger());
    	if(!input.containsKey("predicament")) return Collections.EMPTY_LIST;
        dao = new GameRepository(context.getDynamoDbMapper());
        List<Game> games = CMSCache.getGames(dao, context.getContext().getLogger());
        List<Game> result;
        switch (input.get(SearchCommons.QUERY)) {
            case SearchCommons.TYPE:
                result = games.stream().filter(e -> e.getType().startsWith(input.get(SearchCommons.PARAMETAR))).collect(Collectors.toList());
                break;
            case SearchCommons.NAME:
                result = games.stream().filter(e -> e.getName().startsWith(input.get(SearchCommons.PARAMETAR))).collect(Collectors.toList());
                break;
            default:
                result = games;
                break;
        }
        return result;
    }
}
