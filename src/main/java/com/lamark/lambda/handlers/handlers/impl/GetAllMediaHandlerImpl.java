package com.lamark.lambda.handlers.handlers.impl;

import com.lamark.cache.CMSCache;
import com.lamark.commons.SearchCommons;
import com.lamark.facades.SimpleMedia;
import com.lamark.lambda.handlers.responses.SearchResponse;
import com.lamark.repositories.MediaRepository;
import com.lamark.repositories.ParametarImpl;
import com.lamark.models.Media;
import com.lamark.models.MetaTagSimple;
import com.lamark.models.Parametar;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

public class GetAllMediaHandlerImpl
        extends BasicLambdaFunctionHandlerImpl<HashMap<String, String>, SearchResponse<SimpleMedia>> {

    private BasicRepository<Media> dao;
    private BasicRepository<Parametar> pdao;

    public GetAllMediaHandlerImpl(HashMap<String, String> input, ApplicationContext applicationContext) {
        super(input, applicationContext);
    }

    @Override
    public SearchResponse generateResponse() {
        context.logger.log("generating response");
        if (input == null || input.isEmpty())
            throw new IllegalArgumentException();

        dao = new MediaRepository(context.getDynamoDbMapper());
        pdao = new ParametarImpl(context.getDynamoDbMapper());

        SearchResponse<SimpleMedia> sr = new SearchResponse();

        context.logger.log("cache refresh");
        CMSCache.refreshMediaCache(dao, context.getContext().getLogger());

        if (!input.containsKey(SearchCommons.QUERY)) {
            sr.setData(Collections.EMPTY_LIST);
        }

        // Disable cache until cache issue fixed
        // if (input.containsKey(CMSCache.CACHE_PARAMETAR) &&
        // input.get(CMSCache.CACHE_PARAMETAR).equals(CMSCache.CACHE_PARAMETAR_VALUE)) {
        // context.logger.log("cache refresh");
        // CMSCache.refreshMediaCache(dao, context.getContext().getLogger());
        // if (!input.containsKey(SearchCommons.QUERY))
        // sr.setData(Collections.EMPTY_LIST);
        // }

        long startTime = System.currentTimeMillis();
        List<Media> media = CMSCache.getMedia(dao, context.getContext().getLogger());
        long timeSpent = System.currentTimeMillis() - startTime;

        context.logger.log("Time spent to get all data from db = " + timeSpent + " ms");

        List<Media> result;

        int page = 0;
        int pageSize = 10000;
        double maxPage = 1;

        sr.setCurrentPage(0);

        if (input.containsKey(SearchCommons.PAGE)) {
            page = Integer.parseInt(input.get(SearchCommons.PAGE));
            sr.setCurrentPage(page);
            page--;
        }
        if (input.containsKey(SearchCommons.PAGESIZE)) {
            pageSize = Integer.parseInt(input.get(SearchCommons.PAGESIZE));
        }

        switch (input.get(SearchCommons.QUERY)) {
        case SearchCommons.TYPE:
            result = Objects.requireNonNull(media).stream()
                    .filter(e -> e.getType().equals(input.get(SearchCommons.PARAMETAR))).collect(Collectors.toList());
            break;
        case SearchCommons.NAME:
            result = Objects.requireNonNull(media).stream().filter(
                    e -> e.getScreenName() != null && e.getScreenName().contains(input.get(SearchCommons.PARAMETAR)))
                    .collect(Collectors.toList());
            break;
        case SearchCommons.ID:
            result = Objects.requireNonNull(media).stream()
                    .filter(e -> e.getMediaId() != null && e.getMediaId().equals(input.get(SearchCommons.PARAMETAR)))
                    .collect(Collectors.toList());
            break;
        case SearchCommons.PRODUCT:
            result = Objects.requireNonNull(media).stream().filter(
                    e -> e.getProductId() != null && e.getProductId().equals(input.get(SearchCommons.PARAMETAR)))
                    .collect(Collectors.toList());
            break;
        case SearchCommons.TAG_VALUE:
            result = Objects.requireNonNull(media).stream()
                    .filter(e -> e.getMetadata() != null && e.getMetadata().size() > 0
                            && metaTagHasValue(e.getMetadata(), (input.get(SearchCommons.PARAMETAR))))
                    .collect(Collectors.toList());
            break;
        case SearchCommons.SEARCH_ALL:
            result = media;
            String param = input.get(SearchCommons.PARAMETAR);
            String[] params = param.split("&");
            for (String p : params) {
                if (p.contains("name like")) {
                    String o = p.replace("name like ", "");
                    result = result.stream()
                            .filter(e -> e.getScreenName() != null
                                    && e.getScreenName().toLowerCase().contains(o.trim().toLowerCase()))
                            .collect(Collectors.toList());
                } else if (p.contains("type")) {
                    String o = p.replace("type=", "");
                    result = result.stream().filter(e -> e.getType().equals(o.trim())).collect(Collectors.toList());
                } else if (p.contains("name=")) {
                    String o = p.replace("name=", "");
                    result = result.stream()
                            .filter(e -> e.getScreenName() != null && e.getScreenName().equals(o.trim()))
                            .collect(Collectors.toList());
                } else if (p.contains("product=")) {
                    String o = p.replace("product=", "");
                    result = Objects.requireNonNull(result).stream()
                            .filter(e -> e.getProductId() != null && e.getProductId().equals(o.trim()))
                            .collect(Collectors.toList());
                } else {
                    result = result.stream().filter(e -> e.toString().contains(p.trim())).collect(Collectors.toList());
                }
            }
            break;
        case "*":
            result = media;
            break;
        default:
            result = null;
            break;
        }

        Parametar mediaPath = pdao.getByCompositeKey("MEDIA_S3_URL", "0");

        if (input.containsKey(SearchCommons.PAGE) && result != null) {
            sr.setData(getCurrnetPage(result, page, pageSize, mediaPath.getParametarValue()));
        } else {
            sr.setData(result != null ? result.stream().map(e -> new SimpleMedia(e, mediaPath.getParametarValue()))
                    .collect(Collectors.toList()) : Collections.EMPTY_LIST);
        }

        sr.setSize(Objects.requireNonNull(media).size());

        if (input.containsKey(SearchCommons.PAGESIZE)) {
            maxPage = Math.ceil((double) Objects.requireNonNull(result).size() / pageSize);
        }
        sr.setMaxPage((int) maxPage);

        return sr;
    }

    private boolean metaTagHasValue(List<MetaTagSimple> data, String s) {
        for (MetaTagSimple m : data) {
            if (m.gettValues().contains(s))
                return true;

        }
        return false;
    }

    private List<SimpleMedia> getCurrnetPage(List<Media> result, int page, int pageSize, String path) {
        return result.stream().skip(page * pageSize).limit(pageSize).map(e -> new SimpleMedia(e, path))
                .collect(Collectors.toList());
    }
}
