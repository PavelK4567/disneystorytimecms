package com.lamark.lambda.handlers.handlers.impl;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.lamark.repositories.DisneyCMSUserImpl;
import com.lamark.models.DisneyCMSUser;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;
import com.lamark.lambda.handlers.errors.EmptyResultException;

import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.util.*;
import java.util.concurrent.ThreadLocalRandom;


public class CMSUserForgotenPasswordHandlerImpl extends BasicLambdaFunctionHandlerImpl<HashMap<String, String>, String> {

    // Replace with your "From" address. This address must be verified.
    static final String SUBJECT = "Password Recovery";

    final String username = "automail@la-mark.com";
    final String password = "am123456*";
    protected BasicRepository<DisneyCMSUser> dao;



    public CMSUserForgotenPasswordHandlerImpl(HashMap<String, String> input, ApplicationContext applicationContext) {
        super(input, applicationContext);
    }


    @Override
    public String generateResponse() {
        //TODO: replace password with real value
        Integer psw= ThreadLocalRandom.current().nextInt(100000000, 999999999);
        String messageBody = getMailBodyFromContact(input.get("email"), psw, context.getGlobalVariables().get("PASSWORD_PATH_LINK"));
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "outlook.office365.com");
        props.put("mail.smtp.port", "587");

        Session session = Session.getInstance(props,
                new javax.mail.Authenticator() {
        			@Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication(username, password);
                    }
                });

            javax.mail.Message message = new MimeMessage(session);


        try {
            message.setFrom(new InternetAddress(context.getGlobalVariables().get("CONTACT_FORM_SENDER")));
            message.setRecipients(javax.mail.Message.RecipientType.TO,
                    InternetAddress.parse(input.get("email")));
            message.setSubject(SUBJECT);
            message.setText(messageBody);
            message.setContent(messageBody, "text/html; charset=UTF-8");
            Transport.send(message);
        } catch (MessagingException e) {
            context.logger.log(e.toString());
            throw new RuntimeException("email failed to send");
        }

        dao = new DisneyCMSUserImpl(context.getDynamoDbMapper());
        List<DisneyCMSUser> users = dao.findByDBCriteria(
                new DynamoDBScanExpression().withFilterExpression("email = :email").withExpressionAttributeValues(
                        Collections.singletonMap(":email", new AttributeValue().withS(input.get("email")))));

        if(users.isEmpty() || users.size()>1) throw new EmptyResultException("Invalid CMS user supplied. Contact admin");
        DisneyCMSUser changedUser=users.get(0);
        changedUser.setPassword(psw.toString());

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.HOUR, 24); // get 100 year in future - hopefully all be dead by then
        changedUser.setPasswordExpireDate(cal.getTime());
        dao.put(changedUser);

        return "200";
    }

    /**
     *
     * @param email email address for login
     * @param password encripted password
     * @return text message to be send
     */
    private String getMailBodyFromContact(String email, Integer password, String link) {
        return "Hello,     " +
                email +
                "<br>" +
                "lease click on the linke below to create your new (or first) password to La Mark CMS." +
                "<br>" +
                "<br>" +
                "token: <b>" +
                password +
                "</b>" +
                "<br>" +
                "<br>" +
                "Link: <b>" +
                link +"</b>" ;

    }
}
