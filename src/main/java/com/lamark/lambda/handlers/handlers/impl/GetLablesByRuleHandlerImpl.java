package com.lamark.lambda.handlers.handlers.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

import com.lamark.commons.SearchCommons;
import com.lamark.repositories.LabelRepository;
import com.lamark.models.Label;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;

public class GetLablesByRuleHandlerImpl extends BasicLambdaFunctionHandlerImpl<HashMap<String, Object>, List<Label>> {

    public GetLablesByRuleHandlerImpl(HashMap<String, Object> input, ApplicationContext applicationContext) {
		super(input, applicationContext);
	}

	private Integer size;

	@Override
    public List<Label> generateResponse() {
    	context.logger.log("generating response");
        if(input==null || input.isEmpty()) throw new IllegalArgumentException();
        BasicRepository<Label> dao = new LabelRepository(context.getDynamoDbMapper());
        //if (input.containsKey(CMSCache.CACHE_PARAMETAR) && input.get(CMSCache.CACHE_PARAMETAR).equals(CMSCache.CACHE_PARAMETAR_VALUE)) {
        //	context.logger.log("cache refresh");
        //	CMSCache.refreshLabelCache(dao, context.getContext().getLogger());
        //	if(!input.containsKey(SearchCommons.QUERY)) return Collections.EMPTY_LIST;
        //}
    	//CMSCache.refreshLabelCache(dao, context.getContext().getLogger());
    	if(!input.containsKey(SearchCommons.QUERY)) return Collections.EMPTY_LIST;
        List<Label> labels= dao.findAll();
        context.getContext().getLogger().log("found labels " + labels.size());
        List<Label> result;

        int page=0;
        int pageSize=5;
        if(input.containsKey(SearchCommons.PAGE)){
            page=Integer.parseInt((String)input.get(SearchCommons.PAGE));
            page--;
        }
        if(input.containsKey(SearchCommons.PAGESIZE)){
            pageSize=Integer.parseInt((String)input.get(SearchCommons.PAGESIZE));
        }
        final List<String> parameters = new ArrayList<String>();
        try {
        	for (String p : (List<String>)input.get(SearchCommons.PARAMETAR)) {
        		parameters.add(p);
			}
		} catch (Exception e2) {
			parameters.add((String) input.get(SearchCommons.PARAMETAR));
		}
        switch ((String)input.get(SearchCommons.QUERY)) {
            case SearchCommons.TYPE:
            	result = labels.stream().filter(e -> parameters.contains(e.getType().toString()))
                        .collect(Collectors.toList());
                break;
            case SearchCommons.NAME:
                result = labels.stream().filter(e -> e.getName()!=null
                        && parameters.parallelStream().anyMatch(p-> p.equalsIgnoreCase(e.getName())))
                        .collect(Collectors.toList());
                break;
            case SearchCommons.UID:
                result = labels.stream().filter(e ->e.getUniqueID()!=null && parameters.parallelStream().anyMatch(e.getUniqueID()::startsWith))
                        .collect(Collectors.toList());
                break;
            case SearchCommons.ID:
                result = labels.stream().filter(e ->e.getId()!=null && parameters.contains(e.getId()))
                        .collect(Collectors.toList());
                break;
            case SearchCommons.LANG:
                result = labels.stream().filter(e -> e.getLanguages()!=null
                        && !e.getLanguages().isEmpty()
                        && checkIfContainsLanguage(e.getLanguages(), parameters))
                        .collect(Collectors.toList());
                break;
            case SearchCommons.TAG:
                result = labels.stream().filter(l -> l.getMetatags() !=null
                        && l.getMetatags().gettName() != null
                		&& l.getMetatags().gettName().parallelStream().anyMatch(m -> parameters.stream().anyMatch(p-> p.equalsIgnoreCase(m))))
                        .collect(Collectors.toList());
                break;
            case SearchCommons.SEARCH_ALL:
            	// TODO: support multiple parameters
            	result = labels;
            	if(parameters.size() > 0) {
            		String param=parameters.get(0);
            		if(param != null) {
            			String[] params= param.split("&");
                		for (String p:params) {
                			if(p.contains("name like")){
                				String o=p.replace("name like ","");
                				result = result.stream().filter(e ->e.getName() != null
                						&& e.getName().toLowerCase().contains(o.trim().toLowerCase()))
                						.collect(Collectors.toList());
                			}else if(p.contains("type")){
                				String o=p.replace("type=","");
                				try {
                					result = result.stream().filter(e -> e.getType() == Integer.parseInt(o.trim()))
                							.collect(Collectors.toList());
                				} catch (Exception e2) {
                				}
                			}else if(p.contains("name=")){
                				String o=p.replace("name=","");
                				result = result.stream().filter(e ->e.getName() != null
                						&& e.getName().equals(o.trim()))
                						.collect(Collectors.toList());
                			}
                			else {
                				result = result.stream().filter(e ->e.toString().contains(p.trim()))
                						.collect(Collectors.toList());
                			}
                		}	
            		}
            	}
                break;
            case "*":
                result = labels;
                break;
            default:
                result = null;
                break;
        }
        size=result!=null?result.size():0;

        if(input.containsKey(SearchCommons.PAGE) && result!=null) {
            result = getCurrnetPage(result, page, pageSize);
        }
        return result;
    }

    private List<Label> getCurrnetPage(List<Label> result, int page, int pageSize) {
        return result.stream().skip(page*pageSize).limit(pageSize).collect(Collectors.toList());
    }

    private boolean checkIfContainsLanguage(List<Label.Languages> languages, List<String> shortCode) {
        return languages.stream().anyMatch(e -> shortCode.contains(e.getShortCode()));
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }
}
