package com.lamark.lambda.handlers.handlers.impl;

import com.lamark.lambda.handlers.errors.EmptyResultException;
import com.lamark.repositories.DisneyCMSUserImpl;
import com.lamark.models.DisneyCMSUser;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;

import java.security.InvalidParameterException;

public class GetCmsUserByIDHandlerImpl extends BasicLambdaFunctionHandlerImpl<String, DisneyCMSUser> {

  protected BasicRepository<DisneyCMSUser> dao;

  public GetCmsUserByIDHandlerImpl(String input, ApplicationContext applicationContext) {
    super(input, applicationContext);
  }


  @Override
  public DisneyCMSUser generateResponse() {
    if (input == null || input.isEmpty())
      throw new InvalidParameterException("Invalid CMS user supplied");
    dao = new DisneyCMSUserImpl(context.getDynamoDbMapper());
    DisneyCMSUser user = dao.get(input);
    if (user == null)
      throw new EmptyResultException("Invalid CMS user supplied");
    context.logger.log("user found with data " + user);
    user.setPassword(null);
    return user;
  }
}
