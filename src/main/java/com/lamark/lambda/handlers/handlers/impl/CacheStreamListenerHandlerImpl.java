package com.lamark.lambda.handlers.handlers.impl;

import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.invoke.LambdaFunction;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent;
import com.amazonaws.services.lambda.runtime.events.DynamodbEvent.DynamodbStreamRecord;
import com.amazonaws.util.StringUtils;
import com.lamark.cache.CMSCache;
import com.lamark.global.ApplicationContext;

import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Listens for changes in tables game, media and label and sends requests for cache to be updated
 *
 * @param <T>
 */
public class CacheStreamListenerHandlerImpl<T>
    extends BasicLambdaFunctionHandlerImpl<DynamodbEvent, Integer> {

  public CacheStreamListenerHandlerImpl(DynamodbEvent input, ApplicationContext applicationContext) {
    super(input, applicationContext);
  }

  @Override
  public Integer generateResponse() {
    Pattern p = Pattern.compile("\\/(.*?)\\/");

    for (DynamodbStreamRecord record : input.getRecords()) {
      String eventSourceARN = record.getEventSourceARN();
      String tableName = "";

      if (!StringUtils.isNullOrEmpty(eventSourceARN)) {
        Matcher m = p.matcher(eventSourceARN);
        if (m.find()) {
          tableName = m.group(1);
        }
      }
      context.logger
          .log("INFO: Event type: " + record.getEventName() + ", tableName: " + tableName);

      HashMap<String, String> parametar= new HashMap<>();
      parametar.put(CMSCache.CACHE_PARAMETAR, CMSCache.CACHE_PARAMETAR_VALUE);

      switch (tableName) {
        case "Game": {
          GameCacheService service = LambdaInvokerFactory.builder()
                  .lambdaClient(AWSLambdaClientBuilder.defaultClient()).build(GameCacheService.class);
          service.refreshCache(parametar);
          break;
        }
        case "Label": {
          LabelCacheService service = LambdaInvokerFactory.builder()
                  .lambdaClient(AWSLambdaClientBuilder.defaultClient()).build(LabelCacheService.class);
          service.refreshCache(parametar);
          break;
        }
        case "Media": {
          MediaCacheService service = LambdaInvokerFactory.builder()
                  .lambdaClient(AWSLambdaClientBuilder.defaultClient()).build(MediaCacheService.class);
          service.refreshCache(parametar);
          break;
        }
      }

    }
    return 1;
  }

  public interface LabelCacheService {
    @LambdaFunction(functionName="GetLabelsByRyleHandler")
    void refreshCache(HashMap<String, String> input);
  }

  public interface GameCacheService {
    @LambdaFunction(functionName="GetAllGamesHandlerImpl")
    void refreshCache(HashMap<String, String> input);
  }

  public interface MediaCacheService {
    @LambdaFunction(functionName="GetAllMediaHandlerImpl")
    void refreshCache(HashMap<String, String> input);
  }

}
