package com.lamark.lambda.handlers.handlers.impl;

import com.lamark.filecreator.MediaIndex;
import com.lamark.lambda.handlers.requests.MediaUpdateRequest;
import com.lamark.repositories.MediaRepository;
import com.lamark.models.Media;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;

import java.util.Date;
import java.util.HashSet;

public class MediaBatchUpdateHandlerImpl extends BasicLambdaFunctionHandlerImpl<MediaUpdateRequest, String> {
    private BasicRepository<Media> dao;

    private HashSet<String> products;

    public MediaBatchUpdateHandlerImpl(MediaUpdateRequest input, ApplicationContext applicationContext) {
        super(input, applicationContext);
    }

    @Override
    public String generateResponse() {
        dao = new MediaRepository(context.getDynamoDbMapper());
        products= new HashSet<>();
        if (input == null
                || input.getField() == null
                || (!input.getField().equals(MediaUpdateRequest.ASSIGNED) && !input.getField().equals(MediaUpdateRequest.PUBLISHED)))
            throw new IllegalArgumentException("Incomplete data");

        input.getKeys().stream().forEach(k -> processKey(k, input.getField()));

        products.stream().forEach(p -> {
            try{
                MediaIndex index = new MediaIndex();
                index.createIndexFile(dao,context, p);
            }catch (Exception e){
                context.logger.log("Index file creation failed for product "+p);
            }
        });
        return "200";
    }

    private void processKey(String key, String type) {
        Media m=null;
        context.logger.log("Updating " + key);

        try {
            m = dao.get(key);
            products.add(m.getProductId());
            if (MediaUpdateRequest.ASSIGNED.equals(type)) {
                m.setAssigned(true);
            }
            if (MediaUpdateRequest.PUBLISHED.equals(type)) {
                m.setPublished(true);
            }
            m.setLastUpdateDate(new Date());
            dao.put(m);

        } catch (Exception e) {
            context.logger.log("Failed to change " + m);
            context.logger.log("ERROR " + e.getMessage());
        }
    }

}
