package com.lamark.lambda.handlers.handlers.impl;

import com.lamark.repositories.CounterRepository;
import com.lamark.repositories.LabelRepository;
import com.lamark.models.Label;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;


public class PutLabelHandlerImpl extends BasicLambdaFunctionHandlerImpl<Label, String> {

    BasicRepository<Label> dao;

    public PutLabelHandlerImpl(Label input, ApplicationContext applicationContext) {
        super(input, applicationContext);
    }

    @Override
    public String generateResponse() {
        dao = new LabelRepository(context.getDynamoDbMapper());
        if (input == null || input.getType() == null || input.getName() == null)
            throw new IllegalArgumentException("Incomlete data");
        CounterRepository counterDao = new CounterRepository(context.getDynamoDbMapper());
        String uniqueID = null;
        if (input.getUniqueID() == null || input.getUniqueID().isEmpty()) {
            uniqueID = counterDao.getPaddedValue("Label", "TX");
        }
        input.setUniqueID(uniqueID);
        dao.put(input);
        return "200";
    }
}
