package com.lamark.lambda.handlers.handlers.impl;

import com.lamark.lambda.handlers.errors.EmptyResultException;
import com.lamark.repositories.MetaTagRepositoryImpl;
import com.lamark.models.MetaTag;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;

import java.util.List;

/**
 * The Class GetAllMetaTagsHandlerImpl is a implementation class with business logic for reading the all
 * Metadata tags from DDB.
 */
public class GetAllMetaTagsHandlerImpl extends BasicLambdaFunctionHandlerImpl<Object, List<MetaTag>> {

  BasicRepository<MetaTag> dao;

  public GetAllMetaTagsHandlerImpl(Object input, ApplicationContext context) {
    super(input, context);
  }

  @Override
  public List<MetaTag> generateResponse() {
    dao = new MetaTagRepositoryImpl(context.getDynamoDbMapper());
    List<MetaTag> metaTags = dao.findAll();
    if (metaTags.size() > 0) {
      return metaTags;
    } else {
      throw new EmptyResultException("no data found");
    }
  }
}
