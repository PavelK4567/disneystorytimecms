package com.lamark.lambda.handlers.handlers.impl;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.lamark.repositories.DisneyCMSUserImpl;
import com.lamark.models.DisneyCMSUser;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;
import com.lamark.utils.EDAlghoritam;
import com.lamark.utils.PasswordValidation;

import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.Date;
import java.util.List;

public class PutCmsUserHandlerImpl extends BasicLambdaFunctionHandlerImpl<DisneyCMSUser, String> {

    protected BasicRepository<DisneyCMSUser> dao;

    public PutCmsUserHandlerImpl(DisneyCMSUser input, ApplicationContext applicationContext) {
        super(input, applicationContext);
    }

    public String generateResponse() {

        dao = new DisneyCMSUserImpl(context.getDynamoDbMapper());
        List<DisneyCMSUser> users = dao.findByDBCriteria(
                new DynamoDBScanExpression().withFilterExpression("email = :email").withExpressionAttributeValues(
                        Collections.singletonMap(":email", new AttributeValue().withS(input.getEmail()))));
        if(input.getPassword()!=null && !input.getPassword().equals("")) {
            String val = PasswordValidation.validate(input.getPassword(), true);
            if (!val.equals(PasswordValidation.VALIDATION_PASS)) {
                return val;
            }
        }
        if (!isValidEmailAddress(input.getEmail())) {
            return "email format is not valid";
        }
        try {
            input.setPassword(EDAlghoritam.encodePassword(input.getPassword()));
        } catch (NoSuchAlgorithmException e) {
            context.logger.log("ERROR creating password for " + input.toString());
        }
        if (users.isEmpty()) {
            context.logger.log("Putting cms user to database " + input.toString());
            dao.put(new DisneyCMSUser(input));
        } else {
            context.logger.log("Edit user " + input.toString());
            input.setId(users.get(0).getId());
            input.setLastUpdateDate(new Date());
            input.setCreationDate(users.get(0).getCreationDate());
            dao.put(input);
        }
        return "200";
    }

    public boolean isValidEmailAddress(String email) {
        boolean result = true;
        try {
            InternetAddress emailAddr = new InternetAddress(email);
            emailAddr.validate();
        } catch (AddressException ex) {
            result = false;
        }
        return result;
    }

}
