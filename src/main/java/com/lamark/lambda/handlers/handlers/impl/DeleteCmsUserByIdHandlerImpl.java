package com.lamark.lambda.handlers.handlers.impl;

import com.lamark.repositories.DisneyCMSUserImpl;
import com.lamark.models.DisneyCMSUser;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;

import java.security.InvalidParameterException;


public class DeleteCmsUserByIdHandlerImpl extends BasicLambdaFunctionHandlerImpl<String, String> {

	protected BasicRepository<DisneyCMSUser> dao;

	public DeleteCmsUserByIdHandlerImpl(String input, ApplicationContext applicationContext) {
		super(input, applicationContext);
	}


	public String generateResponse() {
		dao = new DisneyCMSUserImpl(context.getDynamoDbMapper());
		if(input==null || input.isEmpty()) throw new InvalidParameterException("Invalid CMS user supplied");
		DisneyCMSUser user = new DisneyCMSUser();
		user.setId(input);
		dao.delete(user);
		context.logger.log("User deleted with id" + input);
		return "200";
	}

}
