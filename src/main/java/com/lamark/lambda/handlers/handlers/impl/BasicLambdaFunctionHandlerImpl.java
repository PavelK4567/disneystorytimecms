package com.lamark.lambda.handlers.handlers.impl;

import com.lamark.factories.ErrorMessagesFactory;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicLambdaFunctionHandlerInterface;
import com.lamark.lambda.handlers.errors.EmptyResultException;
import com.lamark.lambda.handlers.errors.PasswordValidationException;

import java.security.InvalidParameterException;

public abstract class BasicLambdaFunctionHandlerImpl<T, R> implements BasicLambdaFunctionHandlerInterface<R> {

    protected final T input;
    protected final ApplicationContext context;

    public BasicLambdaFunctionHandlerImpl(T input, ApplicationContext applicationContext) {
        this.input = input;
        this.context = applicationContext;
    }

    public R getResponse() {
        try {
            return generateResponse();
        } catch (InvalidParameterException e) {
            this.context.logger.log(e.getMessage());
            e.printStackTrace();
            throw new InvalidParameterException(ErrorMessagesFactory.getError(ErrorMessagesFactory.INVALID_PARAMETARS, context.getContext().getAwsRequestId(), null));
        } catch (IllegalArgumentException e) {
            this.context.logger.log(e.getMessage());
            e.printStackTrace();
            throw new IllegalArgumentException(ErrorMessagesFactory.getError(ErrorMessagesFactory.METHOD_NOT_ALLOWED, context.getContext().getAwsRequestId(), null));
        } catch (EmptyResultException e) {
            throw new EmptyResultException(ErrorMessagesFactory.getError(ErrorMessagesFactory.NOT_FOUND, context.getContext().getAwsRequestId(), e.getMessage()));
        } catch (PasswordValidationException e) {
            throw new PasswordValidationException(ErrorMessagesFactory.getError(ErrorMessagesFactory.UNAUORIZED, context.getContext().getAwsRequestId(), e.getMessage()));
        } catch (Exception e) {
            this.context.logger.log(e.getMessage());
            e.printStackTrace();
            throw new RuntimeException(ErrorMessagesFactory.getError(ErrorMessagesFactory.BAD_REQUEST, context.getContext().getAwsRequestId(), null));
        }
    }
}
