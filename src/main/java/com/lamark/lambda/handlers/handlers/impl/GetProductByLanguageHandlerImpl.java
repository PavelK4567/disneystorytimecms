package com.lamark.lambda.handlers.handlers.impl;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.util.StringUtils;
import com.lamark.lambda.handlers.errors.EmptyResultException;
import com.lamark.lambda.handlers.requests.ApiRequest;
import com.lamark.repositories.ParametarImpl;
import com.lamark.repositories.ProductRepositoryImpl;
import com.lamark.models.Parametar;
import com.lamark.models.Product;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;
import com.lamark.utils.Constants;

import java.security.InvalidParameterException;
import java.util.Collections;
import java.util.List;

/**
 * The Class GetProductByLanguageHandlerImpl is a implementation class for reading Product by language.
 * 
 * @author elvira
 */
public class GetProductByLanguageHandlerImpl
    extends BasicLambdaFunctionHandlerImpl<ApiRequest, List<Product>> {

  BasicRepository<Product> dao;
  BasicRepository<Parametar> daoParametar;

  public GetProductByLanguageHandlerImpl(ApiRequest input, ApplicationContext context) {
    super(input, context);
  }

  @Override
  public List<Product> generateResponse() {
    context.logger.log("INFO: input: " + input);
    if (input == null || StringUtils.isNullOrEmpty(input.getInputArgument()))
      throw new InvalidParameterException("Invalid Language supplied");
    dao = new ProductRepositoryImpl(context.getDynamoDbMapper());
    DynamoDBScanExpression criteria = new DynamoDBScanExpression()
        .withFilterExpression("lang = :lang").withExpressionAttributeValues(Collections
            .singletonMap(":lang", new AttributeValue().withS(input.getInputArgument())));

    List<Product> products = dao.findByDBCriteria(criteria);
    if (products.size() == 0)
      throw new EmptyResultException("No data found");
    context.logger.log("product found with data " + products.size());
    String s3region = context.getBacketRegion().getName();
    // System.out.println("reg: " + s3region);
    daoParametar = new ParametarImpl(context.getDynamoDbMapper());
    Parametar stageBucket = daoParametar.getByCompositeKey(Constants.STAGE_S3_LOCATION,
        context.getGlobalVariables().get(Constants.GLOBAL_PRODUCT_ID));
    // System.out.println("bucket: " + stageBucket.getParametarValue());
    StringBuilder sb = new StringBuilder();

    products.forEach(element -> {
      sb.setLength(0);
      sb.append(Constants.S3_PATH_1);
      sb.append(s3region);
      sb.append(Constants.S3_PATH_2);
      sb.append(stageBucket.getParametarValue());
      sb.append("/").append(element.getId());
      sb.append("/").append(element.getpImage());
      element.setpImage(sb.toString());
      // System.out.println("path for image: " + element.getpImage());
    });
    return products;
  }
}
