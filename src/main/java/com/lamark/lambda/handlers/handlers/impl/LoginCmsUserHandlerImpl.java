package com.lamark.lambda.handlers.handlers.impl;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.lamark.lambda.handlers.errors.EmptyResultException;
import com.lamark.lambda.handlers.errors.PasswordValidationException;
import com.lamark.repositories.DisneyCMSUserImpl;
import com.lamark.models.DisneyCMSUser;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;
import com.lamark.facades.CMSUSerData;
import com.lamark.utils.Random96;
import com.lamark.utils.EDAlghoritam;

import java.security.NoSuchAlgorithmException;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class LoginCmsUserHandlerImpl extends BasicLambdaFunctionHandlerImpl<HashMap<String, String>, CMSUSerData> {
    protected BasicRepository<DisneyCMSUser> dao;


    public LoginCmsUserHandlerImpl(HashMap<String, String> input, ApplicationContext applicationContext) {
        super(input, applicationContext);
    }

    @Override
    public CMSUSerData generateResponse() {
        dao = new DisneyCMSUserImpl(context.getDynamoDbMapper());
        List<DisneyCMSUser> users = dao.findByDBCriteria(
                new DynamoDBScanExpression().withFilterExpression("email = :email").withExpressionAttributeValues(
                        Collections.singletonMap(":email", new AttributeValue().withS(input.get("email")))));
        String pass;
        if(users.isEmpty()) throw new EmptyResultException("Invalid CMS user supplied");
        try {
            pass = EDAlghoritam.encodePassword(input.get("password"));
            if (!pass.equals(users.get(0).getPassword())){
                throw new PasswordValidationException("Password is wrong");
            }
        } catch (NoSuchAlgorithmException e) {
            context.logger.log("ERROR creating password for " + input.toString());
        }

        HashMap<String, String> parms=  new HashMap<>();
        parms.put(CMSUSerData.products,System.getenv("products"));
        parms.put(CMSUSerData.tagsAndRules,System.getenv("tagsAndRules"));
        parms.put(CMSUSerData.labelManager,System.getenv("labelManager"));
        parms.put(CMSUSerData.userPermission, System.getenv("userPermission"));
        Random96 random96= new Random96();

        CMSUSerData userData = new CMSUSerData(users.get(0), parms);
        userData.setLoginToken(random96.nextInt());
        userData.setLoginTokenExparationDate(new Date(new Date().getTime() + (1000 * 60 * 60 * 24 * 7)));
        //todo: put this in cache (file of bucket or something)

        return userData;
    }
}
