package com.lamark.lambda.handlers.handlers.impl;

import com.lamark.repositories.ParametarImpl;
import com.lamark.models.Game;
import com.lamark.models.Parametar;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;

public class GetMediLocationHandlerImpl extends BasicLambdaFunctionHandlerImpl<Object, String> {

    BasicRepository<Game> dao;

    public GetMediLocationHandlerImpl(Object input, ApplicationContext applicationContext) {
        super(input, applicationContext);
    }


    @Override
    public String generateResponse() {
        BasicRepository<Parametar> dao= new ParametarImpl(context.getDynamoDbMapper());
        Parametar mediaPath=dao.getByCompositeKey("MEDIA_S3_URL","0");
        return mediaPath.getParametarValue();
    }
}
