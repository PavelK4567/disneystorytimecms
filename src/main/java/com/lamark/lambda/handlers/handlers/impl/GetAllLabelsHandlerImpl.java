package com.lamark.lambda.handlers.handlers.impl;

import com.lamark.commons.SearchCommons;
import com.lamark.facades.SimpleLabel;
import com.lamark.lambda.handlers.responses.SearchResponse;
import com.lamark.global.ApplicationContext;

import java.util.HashMap;
import java.util.stream.Collectors;

public class GetAllLabelsHandlerImpl extends BasicLambdaFunctionHandlerImpl<HashMap<String, Object>, SearchResponse<SimpleLabel>> {


    public GetAllLabelsHandlerImpl(HashMap<String, Object> input, ApplicationContext applicationContext) {
        super(input, applicationContext);
    }

    @Override
    public SearchResponse generateResponse() {
    	//update
        GetLablesByRuleHandlerImpl handler= new GetLablesByRuleHandlerImpl(input,context);

        SearchResponse<SimpleLabel> response = new SearchResponse<>();
        response.setData(handler.generateResponse().stream().map(SimpleLabel::new).collect(Collectors.toList()));
        response.setSize(handler.getSize());

        int page=0;
        double pageSize=response.getSize();
        if(input.containsKey(SearchCommons.PAGE)){
            page=Integer.parseInt((String)input.get(SearchCommons.PAGE));
        }
        if(input.containsKey(SearchCommons.PAGESIZE)){
            pageSize=Double.parseDouble((String)input.get(SearchCommons.PAGESIZE));
        }
        response.setCurrentPage(page);
        double size=response.getSize();
        double maxPage=Math.ceil(size/pageSize);
        response.setMaxPage((int) maxPage);
        return response;
    }
}
