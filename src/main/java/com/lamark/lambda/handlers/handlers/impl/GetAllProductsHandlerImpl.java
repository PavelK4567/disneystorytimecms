package com.lamark.lambda.handlers.handlers.impl;

import com.lamark.dto.ProductDTO;
import com.lamark.lambda.handlers.errors.EmptyResultException;
import com.lamark.repositories.ParametarImpl;
import com.lamark.repositories.ProductRepositoryImpl;
import com.lamark.models.Parametar;
import com.lamark.models.Product;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;
import com.lamark.utils.Constants;

import java.util.List;
import java.util.stream.Collectors;

public class GetAllProductsHandlerImpl extends BasicLambdaFunctionHandlerImpl<Object, List<ProductDTO>> {

  BasicRepository<Product> dao;
  BasicRepository<Parametar> daoParametar;

  public GetAllProductsHandlerImpl(Object input, ApplicationContext context) {
    super(input, context);
  }

  @Override
  public List<ProductDTO> generateResponse() {
    System.out.println("ddb reg: " + context.getDynamoDBRegion());
    dao = new ProductRepositoryImpl(context.getDynamoDbMapper());
    daoParametar = new ParametarImpl(context.getDynamoDbMapper());
    List<Product> products = dao.findAll();
    if (products.size() > 0) {
      String s3region = context.getBacketRegion().getName();
      // System.out.println("reg: " + s3region);
      // TODO change type of bucket: stage or production?
      Parametar stageBucket = daoParametar.getByCompositeKey(Constants.STAGE_S3_LOCATION,
          context.getGlobalVariables().get(Constants.GLOBAL_PRODUCT_ID));
      // System.out.println("bucket: " + stageBucket.getParametarValue());
      StringBuilder sb = new StringBuilder();

      products =
          products.stream().filter(product -> product.getpState()).collect(Collectors.toList());
      products.stream().forEach(element -> {
        sb.setLength(0);
        sb.append(Constants.S3_PATH_1);
        sb.append(s3region);
        sb.append(Constants.S3_PATH_2);
        sb.append(stageBucket.getParametarValue());
        sb.append("/" + element.getId());
        sb.append("/" + element.getpImage());
        element.setpImage(sb.toString());
        // System.out.println("path for image: " + element.getpImage());
      });
      // products.forEach(System.out::println);

      List<ProductDTO> productDTOs = products.stream().map(product -> {
        return new ProductDTO(product);
      }).collect(Collectors.toList());
      return productDTOs;
    } else {
      throw new EmptyResultException("no data found");
    }
  }
}
