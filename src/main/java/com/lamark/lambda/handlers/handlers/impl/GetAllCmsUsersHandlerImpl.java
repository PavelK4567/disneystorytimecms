package com.lamark.lambda.handlers.handlers.impl;

import com.lamark.lambda.handlers.errors.EmptyResultException;
import com.lamark.repositories.DisneyCMSUserImpl;
import com.lamark.models.DisneyCMSUser;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;
import com.lamark.facades.SimpleCMSUser;

import java.util.List;
import java.util.stream.Collectors;

public class GetAllCmsUsersHandlerImpl extends BasicLambdaFunctionHandlerImpl<Object, List<SimpleCMSUser>> {

    protected BasicRepository<DisneyCMSUser> dao;

    public GetAllCmsUsersHandlerImpl(Object input, ApplicationContext applicationContext) {
        super(input, applicationContext);
    }

    public List<SimpleCMSUser> generateResponse() {
        dao = new DisneyCMSUserImpl(context.getDynamoDbMapper());
        List<DisneyCMSUser> users = dao.findAll();
        if (users.isEmpty())
            throw new EmptyResultException("CMS Users not found");
        return users.stream().map(SimpleCMSUser::new).collect(Collectors.toList());
    }

}
