package com.lamark.lambda.handlers.handlers.impl;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBScanExpression;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.lamark.lambda.handlers.errors.EmptyResultException;
import com.lamark.repositories.MetaTagRepositoryImpl;
import com.lamark.models.MetaTag;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;

import java.security.InvalidParameterException;
import java.util.Collections;
import java.util.List;

/**
 * The Class GetMetaTagByTypeHandlerImpl is a implementation class with business logic for reading the
 * Metadata tag by type.
 * 
 * @author elvira
 */
public class GetMetaTagByTypeHandlerImpl extends BasicLambdaFunctionHandlerImpl<String, List<MetaTag>> {

  private BasicRepository<MetaTag> dao;

  public GetMetaTagByTypeHandlerImpl(String input, ApplicationContext context) {
    super(input, context);
  }

  @Override
  public List<MetaTag> generateResponse() {
    if (input == null || input.isEmpty())
      throw new InvalidParameterException("Invalid argument supplied");
    dao = new MetaTagRepositoryImpl(context.getDynamoDbMapper());
    DynamoDBScanExpression criteria = new DynamoDBScanExpression()
        .withFilterExpression("tType = :type").withExpressionAttributeValues(
            Collections.singletonMap(":type", new AttributeValue().withS(input)));
    List<MetaTag> tags = dao.findByDBCriteria(criteria);
    if (tags.size() > 0) {
      return tags;
    } else {
      throw new EmptyResultException("no data found");
    }
  }
}
