package com.lamark.lambda.handlers.handlers.impl;

import com.lamark.facades.SimpleMedia;
import com.lamark.filecreator.MediaIndex;
import com.lamark.repositories.MediaRepository;
import com.lamark.models.Media;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;

import java.util.Date;
import java.util.Objects;


public class PutMediaHandlerImpl extends BasicLambdaFunctionHandlerImpl<SimpleMedia, String> {

    private BasicRepository<Media> dao;

    public PutMediaHandlerImpl(SimpleMedia input, ApplicationContext applicationContext) {
        super(input, applicationContext);
    }

    @Override
    public String generateResponse() {
        dao = new MediaRepository(context.getDynamoDbMapper());

        if (input == null || input.getFileName() == null || input.getFileScreenName() == null)
            throw new IllegalArgumentException("Incomplete data");

        Media media = dao.get(input.getFileName());
        int changes = 0;

        if (!Objects.equals(input.getFileScreenName(), media.getScreenName())) {
            media.setScreenName(input.getFileScreenName());
            changes++;
        }
        if (input.getMetadata() == null) {
            media.setMetadata(null);
            changes++;
        }
        if (input.getMetadata() != null && !input.getMetadata().equals(media.getMetadata())) {
            media.setMetadata(input.getMetadata());
            changes++;
        }
        if (input.isAssignedToPage()!=media.isAssigned()) {
            media.setAssigned(input.isAssignedToPage());
            changes++;
        }
        if (input.isAssignedToPublishPage()!=media.isPublished()) {
            media.setPublished(input.isAssignedToPublishPage());
            changes++;
        }
        if (changes > 0) {
            media.setLastUpdateDate(new Date());
            dao.put(media);
            try{
                MediaIndex index = new MediaIndex();
                index.createIndexFile(dao,context, Objects.requireNonNull(media).getProductId());
            }catch (Exception e){
                context.logger.log("Index file creation failed");
            }
        }
        return "200";
    }
}
