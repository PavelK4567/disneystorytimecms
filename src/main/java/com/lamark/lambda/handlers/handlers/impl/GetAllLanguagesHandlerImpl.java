package com.lamark.lambda.handlers.handlers.impl;

import com.lamark.repositories.LanguageRepositoryImpl;
import com.lamark.models.Language;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;

import java.util.List;

public class GetAllLanguagesHandlerImpl extends BasicLambdaFunctionHandlerImpl<Object, List<Language>> {

    BasicRepository dao;

    public GetAllLanguagesHandlerImpl(Object input, ApplicationContext context) {
        super(input, context);
    }

    @SuppressWarnings("unchecked")
    @Override
    public List<Language> generateResponse() {
        dao= new LanguageRepositoryImpl(context.getDynamoDbMapper());
        return (List<Language>)dao.findAll();
    }
}
