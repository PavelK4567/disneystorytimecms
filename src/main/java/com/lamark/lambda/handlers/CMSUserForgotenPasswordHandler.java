package com.lamark.lambda.handlers;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.lamark.global.ApplicationContext;
import com.lamark.lambda.handlers.handlers.impl.CMSUserForgotenPasswordHandlerImpl;

import java.util.HashMap;

public class CMSUserForgotenPasswordHandler implements RequestHandler<HashMap<String, String>, String> {
    static final String FROM = System.getenv("CONTACT_FORM_SENDER");

    @Override
    public String handleRequest(HashMap<String, String> input, Context context) {
        context.getLogger().log("Running CMSUserForgotenPasswordHandlerImpl with Input: " + input);
        ApplicationContext apContext = new ApplicationContext(context);
        apContext.setGlobalVariables("CONTACT_FORM_SENDER", FROM);
        return new CMSUserForgotenPasswordHandlerImpl(input, apContext).getResponse();
    }

}
