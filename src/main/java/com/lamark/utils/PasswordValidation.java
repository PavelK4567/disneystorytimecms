package com.lamark.utils;

import java.util.regex.Pattern;

public class PasswordValidation {

    private static final Pattern hasUppercase = Pattern.compile("[A-Z]");
    private static final Pattern hasLowercase = Pattern.compile("[a-z]");
    private static final Pattern hasNumber = Pattern.compile("\\d");
    private static final Pattern hasSpecialChar = Pattern.compile("[^a-zA-Z0-9 ]");

    public static final String VALIDATION_PASS="Success";

    public static String validate(String password, boolean advanced) {
        if (!advanced)
            return validate(password, null, null, false, false, false, false);
        else
            return validate(password, 6, 12, true, true, true, true);
    }


    public static String validate(String password, Integer minSize, Integer maxSize, boolean upperCaseValidation, boolean lowerCaseValidation, boolean numberValidation, boolean specialCharacterValidation) {
        if (password == null) {
            return "Password is null";
        }
        StringBuilder retVal = new StringBuilder();

        if (password.isEmpty()) {
            retVal.append("Empty fields <br>");
        }

        if (minSize != null && password.length() < minSize) {
            retVal.append("Password is too short. Needs to have min").append(minSize).append(" characters <br>");
        }
        if (maxSize != null && password.length() > maxSize) {
            retVal.append("Password is too lon. Needs to have max").append(maxSize).append(" characters <br>");
        }

        if (!hasUppercase.matcher(password).find() && upperCaseValidation) {
            retVal.append("Password needs an upper case <br>");
        }

        if (!hasLowercase.matcher(password).find() && lowerCaseValidation) {
            retVal.append("Password needs a lowercase <br>");
        }

        if (!hasNumber.matcher(password).find() && numberValidation) {
            retVal.append("Password needs a number <br>");
        }

        if (!hasSpecialChar.matcher(password).find() && specialCharacterValidation) {
            retVal.append("Password needs a special character i.e. !,@,#, etc.  <br>");
        }


        if (retVal.length() == 0) {
            retVal.append(VALIDATION_PASS);
        }

        return retVal.toString();
    }

}
