package com.lamark.utils;

import com.amazonaws.regions.Regions;

import java.util.Arrays;
import java.util.List;

public class Constants {
    // statuses
    public static String BAD_REQUEST_STATUS = "Bad Request";
    public static int BAD_REQUEST_STATUS_CODE = 400;
    public static String NOT_FOUND_STATUS = "Not Found";
    public static int NOT_FOUND_STATUS_CODE = 404;
    public static String OK_STATUS = "OK";
    public static int OK_STATUS_CODE = 200;
    public static String INTERNAL_SERVER_ERROR_STATUS = "Internal Server Error";
    public static int INTERNAL_SERVER_ERROR_STATUS_CODE = 500;


    public static final String GLOBAL_S3_LOCATION = "GLOBAL_S3_LOCATION";
    public static final String STAGE_S3_LOCATION = "STAGE_S3_LOCATION";
    public static final String PUBLISH_S3_LOCATION = "PUBLISH_S3_LOCATION";
    public static final String GLOBAL_PRODUCT_ID = "GLOBAL_PRODUCT_ID";
    public static final String JSON_FILE_SUFFIX = ".json";
    public static final String DATABASE_CONNECTION = "DATABASE_CONNECTION";
    public static final String PASSWORD_PATH_LINK = "PASSWORD_PATH_LINK";

    // Frankfurt
    // public static Regions DYNAMODB_REGION = Regions.EU_CENTRAL_1;
    // public static Regions LAMBDA_REGION = Regions.EU_CENTRAL_1;
    // public static Regions BUCKET_REGION = Regions.EU_CENTRAL_1;

    public static Regions DYNAMODB_REGION = Regions.EU_WEST_1;
    public static Regions LAMBDA_REGION = Regions.EU_WEST_1;
    public static Regions BUCKET_REGION = Regions.EU_WEST_1;
    public static final String S3_PATH_1 = "https://s3.";
    public static final String S3_PATH_2 = ".amazonaws.com/";
    public static final String S3BUCKET_CONTENT_LOCATION = "S3BUCKET_CONTENT_LOCATION";
    public static final String PAGE = "page";
    public static final String IMAGE = "image";
    public static final String AUDIO = "audio";
    public static final String VIDEO = "video";
    public static final String RECOMMENDED_VERSION = "RECOMMENDED_VERSION";
    public static final String NEWEST_VERSION = "NEWEST_VERSION";
    public static String JSON_CONTENT_TYPE = "application/json";
    public static String PAGE_INDEX_FILE = "pageIndex";

    public static final List<String> productFolders = Arrays.asList(PAGE, IMAGE, AUDIO, VIDEO);

}
