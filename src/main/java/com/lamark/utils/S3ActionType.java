package com.lamark.utils;

public enum S3ActionType {
  UPLOAD("UPLOAD"), COPY("COPY"), DOWNLOAD("DOWNLOAD"), DELETE("DELETE"), LIST(
      "LIST"), CREATE_FOLDER("CREATE_FOLDER");

  private final String value;

  S3ActionType(String value) {
    this.value = value;
  }

  public String value() {
    return value;
  }
}
