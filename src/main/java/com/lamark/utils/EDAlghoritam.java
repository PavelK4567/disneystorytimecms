package com.lamark.utils;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

public class EDAlghoritam {

    public static String encodePassword(String password) throws NoSuchAlgorithmException {
        password = encodeMD5("OT:" + password + ":laMark");
        return password;
    }

    private static String encodeMD5(String input) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");

        md.update(input.getBytes());
        byte byteData[] = md.digest();
        // convert the byte to hex format
        StringBuilder sb = new StringBuilder();
        for (byte aByteData : byteData) {
            sb.append(Integer.toString((aByteData & 0xff) + 0x100, 16).substring(1));
        }
        return sb.toString();

    }

    public static String generateString(Random rng, String characters) {
        char[] text = new char[6];
        for (int i = 0; i < 6; i++) {
            text[i] = characters.charAt(rng.nextInt(characters.length()));
        }
        return new String(text);
    }

}
