package com.lamark.dto;

import com.amazonaws.services.s3.model.ObjectMetadata;
import com.lamark.global.ApplicationContext;
import com.lamark.utils.S3ActionType;

import java.io.ByteArrayInputStream;
import java.util.List;

public class S3EventDTO {
  private ApplicationContext context;
  private String sourceBucketName;
  private String sourceFolder;
  private String jsonFileName;
  private ByteArrayInputStream inputSteramToWrite;
  private ObjectMetadata meta;
  private S3ActionType s3ActionType;
  private String destinationBucketName;
  private String destinationFolder;
  private List<String> folders;

  public S3EventDTO() {
    super();
  }

  public S3EventDTO(ApplicationContext context, String sourceBucketName, String sourceFolder,
                    String jsonFileName, ByteArrayInputStream inputSteramToWrite, ObjectMetadata meta,
                    S3ActionType s3ActionType, String destinationBucketName, String destinationFolder,
                    List<String> folders) {
    super();
    this.context = context;
    this.sourceBucketName = sourceBucketName;
    this.sourceFolder = sourceFolder;
    this.jsonFileName = jsonFileName;
    this.inputSteramToWrite = inputSteramToWrite;
    this.meta = meta;
    this.s3ActionType = s3ActionType;
    this.destinationBucketName = destinationBucketName;
    this.destinationFolder = destinationFolder;
    this.folders = folders;
  }

  public ApplicationContext getContext() {
    return context;
  }

  public void setContext(ApplicationContext context) {
    this.context = context;
  }

  public String getSourceBucketName() {
    return sourceBucketName;
  }

  public void setSourceBucketName(String sourceBucketName) {
    this.sourceBucketName = sourceBucketName;
  }

  public String getSourceFolder() {
    return sourceFolder;
  }

  public void setSourceFolder(String sourceFolder) {
    this.sourceFolder = sourceFolder;
  }

  public String getJsonFileName() {
    return jsonFileName;
  }

  public void setJsonFileName(String jsonFileName) {
    this.jsonFileName = jsonFileName;
  }

  public ByteArrayInputStream getInputSteramToWrite() {
    return inputSteramToWrite;
  }

  public void setInputSteramToWrite(ByteArrayInputStream inputSteramToWrite) {
    this.inputSteramToWrite = inputSteramToWrite;
  }

  public ObjectMetadata getMeta() {
    return meta;
  }

  public void setMeta(ObjectMetadata meta) {
    this.meta = meta;
  }

  public S3ActionType getS3ActionType() {
    return s3ActionType;
  }

  public void setS3ActionType(S3ActionType s3ActionType) {
    this.s3ActionType = s3ActionType;
  }

  public String getDestinationBucketName() {
    return destinationBucketName;
  }

  public void setDestinationBucketName(String destinationBucketName) {
    this.destinationBucketName = destinationBucketName;
  }

  public String getDestinationFolder() {
    return destinationFolder;
  }

  public void setDestinationFolder(String destinationFolder) {
    this.destinationFolder = destinationFolder;
  }

  public List<String> getFolders() {
    return folders;
  }

  public void setFolders(List<String> folders) {
    this.folders = folders;
  }

  @Override
  public String toString() {
    return "S3EventDTO [context=" + context + ", sourceBucketName=" + sourceBucketName
        + ", sourceFolder=" + sourceFolder + ", jsonFileName=" + jsonFileName
        + ", inputSteramToWrite=" + inputSteramToWrite + ", meta=" + meta + ", s3ActionType="
        + s3ActionType + ", destinationBucketName=" + destinationBucketName + ", destinationFolder="
        + destinationFolder + ", folders=" + folders + "]";
  }

}
