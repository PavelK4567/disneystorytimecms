package com.lamark.dto;

import com.lamark.models.Product;

/**
 * The Class Product is a model for json response for client side.
 * 
 * @author elvira
 */
public class ProductDTO {
  private String productId;
  private String productName;
  private String productImageUrl;
  private String productLang;
  private Integer pType;

  public ProductDTO() {
    super();
  }

  public ProductDTO(Product p) {
    this.productId = p.getId();
    this.productName = p.getpName();
    this.productImageUrl = p.getpImage();
    this.productLang = p.getLang();
    this.pType = p.getpType();
  }

  public String getProductId() {
    return productId;
  }

  public void setProductId(String productId) {
    this.productId = productId;
  }

  public String getProductName() {
    return productName;
  }

  public void setProductName(String productName) {
    this.productName = productName;
  }

  public String getProductImageUrl() {
    return productImageUrl;
  }

  public void setProductImageUrl(String productImageUrl) {
    this.productImageUrl = productImageUrl;
  }

  public String getProductLang() {
    return productLang;
  }

  public void setProductLang(String productLang) {
    this.productLang = productLang;
  }

  public Integer getpType() {
    return pType;
  }

  public void setpType(Integer pType) {
    this.pType = pType;
  }

  @Override
  public String toString() {
    return "ProductDTO [productId=" + productId + ", productName=" + productName
        + ", productImageUrl=" + productImageUrl + ", productLang=" + productLang + "]";
  }

}
