package com.lamark.global;

import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;

public class DynamoTableNameResolver extends DynamoDBMapperConfig.DefaultTableNameResolver {

    private String environment = "dev";

    public DynamoTableNameResolver() {
        String environment = System.getenv("DISNEY_ENV");
        if (environment != null) {
            this.environment = environment;
        }
    }

    @Override
    public String getTableName(Class<?> clazz, DynamoDBMapperConfig config) {
        String rawTableName = super.getTableName(clazz, config);
        return this.environment != null ? this.environment + "_" + rawTableName : rawTableName;
    }
}