package com.lamark.global;

import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapper;
import com.amazonaws.services.dynamodbv2.datamodeling.DynamoDBMapperConfig;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.LambdaLogger;
import com.amazonaws.util.StringUtils;
import com.lamark.factories.DynamoDbConnectionFactory;
import com.lamark.utils.Constants;

import java.util.HashMap;

import static com.lamark.utils.Constants.*;

public class ApplicationContext {

    private static AmazonDynamoDB databaseConnection;
    private static DynamoDBMapper dynamoDbMapper;

    private final Context context;
    private final HashMap<String, String> globalVariables = new HashMap<>();

    public final LambdaLogger logger;

    public HashMap<String, String> getGlobalVariables() {
        return globalVariables;
    }

    public void setGlobalVariables(String key, String value) {
        this.globalVariables.put(key, value);
    }

    private Regions backetRegion;
    private Regions dynamoDBRegion;
    private Region region;

    public ApplicationContext(Context context) {
        this.context = context;

        logger = context.getLogger();

        initializeGlobalVariables();

        if (databaseConnection == null) {
            logger.log("DB connection creating...");

            DynamoDBMapperConfig config = DynamoDBMapperConfig.builder()
                    .withTableNameResolver(new DynamoTableNameResolver()).build();

            logger.log("DB get connection");

            databaseConnection = new DynamoDbConnectionFactory().getConnection(DynamoDbConnectionFactory.REGION, null);

            logger.log("DB get mapper");

            dynamoDbMapper = new DynamoDBMapper(databaseConnection, config);

            logger.log("DB connection created");
        }
    }

    private void initializeGlobalVariables() {
        setGlobalVariables(GLOBAL_S3_LOCATION, "GLOBAL_S3_LOCATION");
        setGlobalVariables(STAGE_S3_LOCATION, "STAGE_S3_LOCATION");
        setGlobalVariables(PUBLISH_S3_LOCATION, "PUBLISH_S3_LOCATION");
        setGlobalVariables(GLOBAL_PRODUCT_ID, "0");
        setGlobalVariables(JSON_FILE_SUFFIX, ".json");
        setGlobalVariables(DATABASE_CONNECTION, System.getenv(DATABASE_CONNECTION));
        setGlobalVariables("PASSWORD_PATH_LINK", System.getenv("PASSWORD_PATH_LINK"));

        if (!StringUtils.isNullOrEmpty(System.getenv("AWS_DEFAULT_REGION"))) {
            region = Region.getRegion(Regions.fromName(System.getenv("AWS_DEFAULT_REGION")));
            backetRegion = Regions.fromName(System.getenv("AWS_DEFAULT_REGION"));
            dynamoDBRegion = Regions.fromName(System.getenv("AWS_DEFAULT_REGION"));
        } else {
            backetRegion = Constants.BUCKET_REGION;
            dynamoDBRegion = Constants.DYNAMODB_REGION;
            region = Region.getRegion(Constants.LAMBDA_REGION);
        }

        // Region region = Regions.getCurrentRegion();
        setRegion(region);
        setBacketRegion(backetRegion);
        setDynamoDBRegion(dynamoDBRegion);
    }

    public ApplicationContext(Context context, AmazonDynamoDB databaseConnection, DynamoDBMapper dynamoDbMapper) {
        this.context = context;
        ApplicationContext.databaseConnection = databaseConnection;
        ApplicationContext.dynamoDbMapper = dynamoDbMapper;
        logger = context.getLogger();
        initializeGlobalVariables();
    }

    public Context getContext() {
        return context;
    }

    public AmazonDynamoDB getDatabaseConnection() {
        return databaseConnection;
    }

    public void setDatabaseConnection(AmazonDynamoDB databaseConnection) {
        ApplicationContext.databaseConnection = databaseConnection;
    }

    public DynamoDBMapper getDynamoDbMapper() {
        return dynamoDbMapper;
    }

    public void setDynamoDbMapper(DynamoDBMapper dynamoDbMapper) {
        ApplicationContext.dynamoDbMapper = dynamoDbMapper;
    }

    public Regions getBacketRegion() {
        return backetRegion;
    }

    public void setBacketRegion(Regions backetRegion) {
        this.backetRegion = backetRegion;
    }

    public Region getRegion() {
        return region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Regions getDynamoDBRegion() {
        return dynamoDBRegion;
    }

    public void setDynamoDBRegion(Regions dynamoDBRegion) {
        this.dynamoDBRegion = dynamoDBRegion;
        Constants.DYNAMODB_REGION = dynamoDBRegion;
    }
}
