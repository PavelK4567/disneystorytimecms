package com.lamark.annotations;

import com.amazonaws.services.dynamodbv2.model.StreamViewType;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface DynamoDbTableMapper {
  boolean mapInDynamoDB() default true;

  boolean keyStream() default false;

  StreamViewType keyStreamType() default StreamViewType.KEYS_ONLY;

  String lambdaConnect() default "";
}
