package com.lamark.s3;

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.CannedAccessControlList;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.util.StringUtils;
import com.lamark.dto.S3EventDTO;
import com.lamark.global.ApplicationContext;

import java.util.List;

/**
 * A factory for uploading/copying/deleting S3 objects.
 */
public class DisneyS3Factory {

  public static int actionsToS3(S3EventDTO s3EventDTO) {
    ApplicationContext context = s3EventDTO.getContext();
    String sourceFolder;
    String destinationFolder;
    context.logger.log("Action to S3 " + s3EventDTO);
    try {
      AmazonS3 s3client =
          AmazonS3ClientBuilder.standard().withRegion(context.getBacketRegion()).build();

      switch (s3EventDTO.getS3ActionType()) {
        case UPLOAD:
          sourceFolder = StringUtils.isNullOrEmpty(s3EventDTO.getSourceFolder()) ? ""
              : s3EventDTO.getSourceFolder() + "/";
          context.logger.log("Writing to: " + s3EventDTO.getSourceBucketName() + "/" + sourceFolder
              + s3EventDTO.getJsonFileName());
          PutObjectRequest request = new PutObjectRequest(s3EventDTO.getSourceBucketName(),
              sourceFolder + s3EventDTO.getJsonFileName(), s3EventDTO.getInputSteramToWrite(),
              s3EventDTO.getMeta());
          request.setCannedAcl(CannedAccessControlList.PublicRead);
          s3client.putObject(request);
          break;
        case COPY:
          sourceFolder = StringUtils.isNullOrEmpty(s3EventDTO.getSourceFolder()) ? ""
              : s3EventDTO.getSourceFolder() + "/";
          destinationFolder = StringUtils.isNullOrEmpty(s3EventDTO.getDestinationFolder()) ? ""
              : s3EventDTO.getDestinationFolder() + "/";
          context.logger.log("Copy from: " + s3EventDTO.getSourceBucketName() + "/" + sourceFolder
              + s3EventDTO.getJsonFileName() + " to " + s3EventDTO.getDestinationBucketName() + "/"
              + destinationFolder + s3EventDTO.getJsonFileName());
          s3client.copyObject(s3EventDTO.getSourceBucketName(),
              sourceFolder + s3EventDTO.getJsonFileName(), s3EventDTO.getDestinationBucketName(),
              destinationFolder + s3EventDTO.getJsonFileName());
          break;
        case DOWNLOAD:

          break;
        case DELETE:

          break;
        case CREATE_FOLDER:
          context.logger.log("Writing to: " + s3EventDTO.getSourceBucketName());
          sourceFolder = StringUtils.isNullOrEmpty(s3EventDTO.getSourceFolder()) ? ""
              : s3EventDTO.getSourceFolder() + "/";
          List<String> folders = s3EventDTO.getFolders();
          if (folders != null && folders.size() > 0) {
            folders.stream().forEach(element -> {
              System.out.println("folder: " + element);
              String subfolder =
                  new StringBuilder().append(sourceFolder).append(element).append("/").toString();
              PutObjectRequest request1 = new PutObjectRequest(s3EventDTO.getSourceBucketName(),
                  subfolder, s3EventDTO.getInputSteramToWrite(), s3EventDTO.getMeta());
              request1.setCannedAcl(CannedAccessControlList.PublicRead);
              s3client.putObject(request1);
            });
          } else {
            PutObjectRequest request1 = new PutObjectRequest(s3EventDTO.getSourceBucketName(),
                sourceFolder, s3EventDTO.getInputSteramToWrite(), s3EventDTO.getMeta());
            request1.setCannedAcl(CannedAccessControlList.PublicRead);
            s3client.putObject(request1);
          }
          break;

        default:
          break;
      }

      return 0;
    } catch (

    AmazonServiceException ase) {
      context.logger.log("Caught an AmazonServiceException, which " + "means your request made it "
          + "to Amazon S3, but was rejected with an error response" + " for some reason.");
      context.logger.log("Error Message:    " + ase.getMessage());
      context.logger.log("HTTP Status Code: " + ase.getStatusCode());
      context.logger.log("AWS Error Code:   " + ase.getErrorCode());
      context.logger.log("Error Type:       " + ase.getErrorType());
      context.logger.log("Request ID:       " + ase.getRequestId());
      return -1;
    } catch (AmazonClientException ace) {
      context.logger.log("Caught an AmazonClientException, which " + "means the client encountered "
          + "an internal error while trying to " + "communicate with S3, "
          + "such as not being able to access the network.");
      context.logger.log("Error Message: " + ace.getMessage());
      return -2;
    } catch (Exception e) {
      context.logger.log("Error Message: " + e.getMessage());
      return -3;
    }

  }

}
