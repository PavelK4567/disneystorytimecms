package com.lamark.s3;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.CannedAccessControlList;

public class S3AccessRigthManager {

    private AmazonS3 _s3;

    public S3AccessRigthManager(AmazonS3 s3) {
        _s3=s3;
    }

    public void grantPublicReadAccess(String bucketName, String keyName){
        _s3.setObjectAcl(bucketName, keyName, CannedAccessControlList.PublicRead);
    }


}
