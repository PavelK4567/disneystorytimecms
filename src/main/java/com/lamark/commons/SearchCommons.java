package com.lamark.commons;

public class SearchCommons {

    public static String QUERY="query";
    public static String PARAMETAR="parametar";
    public static String PAGE="page";
    public static String PAGESIZE="pageSize";


    public static final String TYPE="type";
    public static final String NAME="name";
    public static final String ID="id";
    public static final String UID="uniqueID";
    public static final String LANG="language";
    public static final String PRODUCT="product";
    public static final String TAG_VALUE="tagValue";
    public static final String SEARCH_ALL="all";
    public static final String TAG="tag";

    
    public static class SearchObject {
    	
    }
}
