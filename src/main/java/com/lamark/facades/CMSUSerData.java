package com.lamark.facades;

import com.lamark.models.DisneyCMSUser;
import jdk.nashorn.internal.ir.annotations.Ignore;

import java.util.Date;
import java.util.HashMap;

public class CMSUSerData {

    public static final String products = "products";
    public static final String tagsAndRules = "tagsAndRules";
    public static final String labelManager = "labelManager";
    public static final String userPermission = "userPermission";

    private String username;
    private int loginToken;
    private Date loginTokenExparationDate;
    private boolean rightToPublish = false;

    @Ignore
    private String ID;

    HashMap<String, String> availableButtons = new HashMap<>();

    public CMSUSerData(DisneyCMSUser user, HashMap<String, String> buttons) {
        this.username = user.getUsername();
        if (user.getPermissions() != null && user.getPermissions().isPublish())
            this.rightToPublish = true;
        if (user.getPermissions() != null && user.getPermissions().isProducts())
            availableButtons.put(products, buttons.get(products));
        if (user.getPermissions() != null && user.getPermissions().isTagsAndRules())
            availableButtons.put(tagsAndRules, buttons.get(tagsAndRules));
        if (user.getPermissions() != null && user.getPermissions().isLabelManager())
            availableButtons.put(labelManager, buttons.get(labelManager));
        if (user.getPermissions() != null && user.getPermissions().isUserManagement())
            availableButtons.put(userPermission, buttons.get(userPermission));

        this.ID=user.getId();

    }

    public static String getProducts() {
        return products;
    }

    public static String getTagsAndRules() {
        return tagsAndRules;
    }

    public static String getLabelManager() {
        return labelManager;
    }

    public static String getUserPermission() {
        return userPermission;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getLoginToken() {
        return loginToken;
    }

    public void setLoginToken(int loginToken) {
        this.loginToken = loginToken;
    }

    public boolean isRightToPublish() {
        return rightToPublish;
    }

    public void setRightToPublish(boolean rightToPublish) {
        this.rightToPublish = rightToPublish;
    }

    public HashMap<String, String> getAvailableButtons() {
        return availableButtons;
    }

    public void setAvailableButtons(HashMap<String, String> availableButtons) {
        this.availableButtons = availableButtons;
    }

    public Date getLoginTokenExparationDate() {
        return loginTokenExparationDate;
    }

    public void setLoginTokenExparationDate(Date loginTokenExparationDate) {
        this.loginTokenExparationDate = loginTokenExparationDate;
    }

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }
}
