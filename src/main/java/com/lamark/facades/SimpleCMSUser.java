package com.lamark.facades;

import com.lamark.models.DisneyCMSUser;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class SimpleCMSUser {

    String username;
    String email;
    List<String> permissions;
    Date lastUpdate;
    Date expirationDate;
    String avatarLink;
    String id;


    public SimpleCMSUser(DisneyCMSUser user) {
        this.username = user.getUsername();
        this.email = user.getEmail();
        this.lastUpdate = user.getLastUpdateDate();
        this.expirationDate = user.getExpirationDate();
        this.avatarLink = user.getAvatarLink();
        this.id=user.getId();
        permissions = new ArrayList<>();
        if (user.getPermissions() != null) {
            if (user.getPermissions().isLabelManager()) {
                permissions.add("labelManager");
            }
            if (user.getPermissions().isPublish()) {
                permissions.add("publish");
            }
            if (user.getPermissions().isTagsAndRules()) {
                permissions.add("tagsAndRules");
            }
            if (user.getPermissions().isProducts()) {
                permissions.add("products");
            }
            if (user.getPermissions().isUserManagement()) {
                permissions.add("userManagment");
            }
        }
        if (permissions.size() == 0) permissions = null;

    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public List<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }

    public String getAvatarLink() {
        return avatarLink;
    }

    public void setAvatarLink(String avatarLink) {
        this.avatarLink = avatarLink;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
