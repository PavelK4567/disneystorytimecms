package com.lamark.facades;

import com.lamark.models.Label;

import java.util.Date;
import java.util.Set;
import java.util.stream.Collectors;

public class SimpleLabel {

    private String id;
    private String name;
    private String unqiueID;
    private Set<String> languages;
    private Date lastUpdate;
    private Set<String> tags;
    private Set<String> games;

    public SimpleLabel(Label l) {
        id = l.getId();
        Integer type = l.getType();
        name = l.getName();
        unqiueID = l.getUniqueID();
        lastUpdate = l.getLastUpdateDate();
        languages = l.getLanguages() != null ? l.getLanguages().stream().map(Label.Languages::getShortCode).collect(Collectors.toSet()) : null;
        tags = l.getMetatags() != null && l.getMetatags().gettName() != null ? l.getMetatags().gettName().stream().collect(Collectors.toSet()) : null;
        games = l.getGames() != null ? l.getGames().stream().map(Label.Games::getName).collect(Collectors.toSet()) : null;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnqiueID() {
        return unqiueID;
    }

    public void setUnqiueID(String unqiueID) {
        this.unqiueID = unqiueID;
    }

    public Set<String> getLanguages() {
        return languages;
    }

    public void setLanguages(Set<String> languages) {
        this.languages = languages;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }

    public Set<String> getTags() {
        return tags;
    }

    public void setTags(Set<String> tags) {
        this.tags = tags;
    }

    public Set<String> getGames() {
        return games;
    }

    public void setGames(Set<String> games) {
        this.games = games;
    }
}
