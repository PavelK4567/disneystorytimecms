package com.lamark.facades;

import com.lamark.models.Media;
import com.lamark.models.MetaTagSimple;

import java.util.List;

public class SimpleMedia {

    private String fileName;
    private String fileScreenName;
    private boolean isAssignedToPage;
    private boolean isAssignedToPublishPage;
    private String mediaType;
    private String product;
    private String downloadLink;
    private List<MetaTagSimple> metadata;

    public SimpleMedia() {
    }

    public SimpleMedia(Media m, String path) {
        fileName = m.getMediaId();
        fileScreenName = m.getScreenName();
        isAssignedToPage = m.isAssigned();
        isAssignedToPublishPage = m.isPublished();
        mediaType = m.getType();
        product = m.getProductId();
        downloadLink=path+fileName;
        metadata=m.getMetadata();
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileScreenName() {
        return fileScreenName;
    }

    public void setFileScreenName(String fileScreenName) {
        this.fileScreenName = fileScreenName;
    }

    public boolean isAssignedToPage() {
        return isAssignedToPage;
    }

    public void setAssignedToPage(boolean assignedToPage) {
        isAssignedToPage = assignedToPage;
    }

    public boolean isAssignedToPublishPage() {
        return isAssignedToPublishPage;
    }

    public void setAssignedToPublishPage(boolean assignedToPublishPage) {
        isAssignedToPublishPage = assignedToPublishPage;
    }

    public String getMediaType() {
        return mediaType;
    }

    public void setMediaType(String mediaType) {
        this.mediaType = mediaType;
    }

    public String getProduct() {
        return product;
    }

    public void setProduct(String product) {
        this.product = product;
    }

    public String getDownloadLink() {
        return downloadLink;
    }

    public void setDownloadLink(String downloadLink) {
        this.downloadLink = downloadLink;
    }

    public List<MetaTagSimple> getMetadata() {
        return metadata;
    }

    public void setMetadata(List<MetaTagSimple> metadata) {
        this.metadata = metadata;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SimpleMedia)) return false;

        SimpleMedia that = (SimpleMedia) o;

        if (isAssignedToPage != that.isAssignedToPage) return false;
        if (isAssignedToPublishPage != that.isAssignedToPublishPage) return false;
        if (fileName != null ? !fileName.equals(that.fileName) : that.fileName != null) return false;
        if (fileScreenName != null ? !fileScreenName.equals(that.fileScreenName) : that.fileScreenName != null)
            return false;
        if (mediaType != null ? !mediaType.equals(that.mediaType) : that.mediaType != null) return false;
        if (product != null ? !product.equals(that.product) : that.product != null) return false;
        if (downloadLink != null ? !downloadLink.equals(that.downloadLink) : that.downloadLink != null) return false;
        return metadata != null ? metadata.equals(that.metadata) : that.metadata == null;
    }

    @Override
    public int hashCode() {
        int result = fileName != null ? fileName.hashCode() : 0;
        result = 31 * result + (fileScreenName != null ? fileScreenName.hashCode() : 0);
        result = 31 * result + (isAssignedToPage ? 1 : 0);
        result = 31 * result + (isAssignedToPublishPage ? 1 : 0);
        result = 31 * result + (mediaType != null ? mediaType.hashCode() : 0);
        result = 31 * result + (product != null ? product.hashCode() : 0);
        result = 31 * result + (downloadLink != null ? downloadLink.hashCode() : 0);
        result = 31 * result + (metadata != null ? metadata.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "SimpleMedia{" +
                "fileName='" + fileName + '\'' +
                ", fileScreenName='" + fileScreenName + '\'' +
                ", isAssignedToPage=" + isAssignedToPage +
                ", isAssignedToPublishPage=" + isAssignedToPublishPage +
                ", mediaType='" + mediaType + '\'' +
                ", product='" + product + '\'' +
                ", downloadLink='" + downloadLink + '\'' +
                ", metadata=" + metadata +
                '}';
    }
}
