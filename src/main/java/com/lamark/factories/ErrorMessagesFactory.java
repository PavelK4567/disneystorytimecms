package com.lamark.factories;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.util.HashMap;
import java.util.Map;

public class ErrorMessagesFactory {

    public static final int BAD_REQUEST = 400;
    public static final int UNAUORIZED = 401;

    public static final int NOT_FOUND = 404;
    public static final int METHOD_NOT_ALLOWED = 405;
    public static final int UNPROCESSABLE_ENTETY = 422;
    public static final int DEPRICATED = 410;
    public static final int INVALID_PARAMETARS = 406;

    public static String getError(int code, String id, String attachText) {
        Map<String, Object> errorPayload = new HashMap<>();
        errorPayload.put("httpStatus", code);
        errorPayload.put("requestId", id);

        attachText = attachText == null || attachText.isEmpty() ? "" : " " + attachText;

        switch (code) {
        case BAD_REQUEST:
            errorPayload.put("errorType", "BadRequest");
            errorPayload.put("message", "BAD REQUEST data not composed propery" + attachText);
            break;
        case UNAUORIZED:
            errorPayload.put("errorType", "Unauthorized");
            errorPayload.put("message", "Unauthorized access" + attachText);
            break;
        case NOT_FOUND:
            errorPayload.put("errorType", "NotFound");
            errorPayload.put("message", "DATA NOT FOUND resource missing in repositories" + attachText);
            break;
        case METHOD_NOT_ALLOWED:
            errorPayload.put("errorType", "Method Not Allowed");
            errorPayload.put("message", "NOT acceptable parameters" + attachText);
            break;
        case UNPROCESSABLE_ENTETY:
            errorPayload.put("errorType", "NotFound");
            errorPayload.put("message", "Unpocessable parameters" + attachText);
            break;
        case DEPRICATED:
            errorPayload.put("errorType", "NotFound");
            errorPayload.put("message", "This microservce is depricated" + attachText);
            break;
        case INVALID_PARAMETARS:
            errorPayload.put("errorType", "Invalid Parameters");
            errorPayload.put("message", "Invalid parameters" + attachText);
            break;
        }

        try {
            return new ObjectMapper().writeValueAsString(errorPayload);
        } catch (JsonProcessingException e) {
            return null;
        }
    }
}
