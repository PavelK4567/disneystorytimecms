package com.lamark.factories;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClient;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.lamark.utils.Constants;

public class DynamoDbConnectionFactory {

    public static final String LOCAL = "LOCAL";
    public static final String REGION = "REGION";

    @SuppressWarnings("deprecation")
    public AmazonDynamoDB getConnection(String type, String port) {
        if (type == null) {
            return null;
        }

        if (type.equalsIgnoreCase(LOCAL)) {
            AmazonDynamoDB amazonDynamoDB = new AmazonDynamoDBClient(new BasicAWSCredentials("access", "secret"));
            amazonDynamoDB.setEndpoint("http://localhost:" + port);
            return amazonDynamoDB;
        } else if (type.equalsIgnoreCase(REGION)) {
            final AmazonDynamoDBClientBuilder amazonDynamoDBClientBuilder = AmazonDynamoDBClientBuilder.standard()
                    // .withRegion(Regions.EU_CENTRAL_1);
                    .withRegion(Constants.DYNAMODB_REGION);
            return amazonDynamoDBClientBuilder.build();
        }

        return null;
    }

}
