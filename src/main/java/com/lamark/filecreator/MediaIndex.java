package com.lamark.filecreator;

import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.amazonaws.services.lambda.invoke.LambdaFunction;
import com.amazonaws.services.lambda.invoke.LambdaFunctionNameResolver;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactory;
import com.amazonaws.services.lambda.invoke.LambdaInvokerFactoryConfig;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.lamark.cache.CMSCache;
import com.lamark.commons.SearchCommons;
import com.lamark.dto.S3EventDTO;
import com.lamark.facades.SimpleMedia;
import com.lamark.lambda.handlers.responses.SearchResponse;
import com.lamark.repositories.ParametarImpl;
import com.lamark.s3.DisneyS3Factory;
import com.lamark.models.Media;
import com.lamark.models.Parametar;
import com.lamark.global.ApplicationContext;
import com.lamark.interfaces.BasicRepository;
import com.lamark.utils.Constants;
import com.lamark.utils.S3ActionType;

import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.Method;
import java.util.HashMap;

public class MediaIndex {

    private BasicRepository<Parametar> pdao;

    public void createIndexFile(BasicRepository<Media> dao, ApplicationContext context, String productID) {
        context.logger.log("creating index file for product " + productID);
        CMSCache.refreshMediaCache(dao, context.getContext().getLogger());
        pdao = new ParametarImpl(context.getDynamoDbMapper());

        HashMap<String, String> parametar= new HashMap<>();
        parametar.put(CMSCache.CACHE_PARAMETAR, CMSCache.CACHE_PARAMETAR_VALUE);
        parametar.put(SearchCommons.QUERY, SearchCommons.PRODUCT);
        parametar.put(SearchCommons.PARAMETAR, productID);

        MediaCacheService service = LambdaInvokerFactory.builder()
                .lambdaClient(AWSLambdaClientBuilder.defaultClient())
                .lambdaFunctionNameResolver(new LambdaFunctionNameResolver() {
					@Override
					public String getFunctionName(Method method, LambdaFunction annotation, LambdaInvokerFactoryConfig config) {
						String lambdaFunctionName="GetAllMediaHandlerImpl";
						try {
							String functionArn=context.getContext().getInvokedFunctionArn();
							if(functionArn != null) {
								// arn:aws:lambda:eu-west-1:<account>:function:VODLambdaConvert
								if(functionArn.lastIndexOf("function:") >= 0) {
									String fNameAndAlias=functionArn.substring(functionArn.lastIndexOf("function:"));
									if(fNameAndAlias.lastIndexOf(":") >= 0) {
										return lambdaFunctionName + fNameAndAlias.substring(fNameAndAlias.lastIndexOf(":"));
									}
								}
							}
							return lambdaFunctionName;
						} catch (Exception e) {
							return lambdaFunctionName;
						}
					}
				})
                .build(MediaCacheService.class);

        SearchResponse<SimpleMedia> sdata=service.refreshCacheAndGetData(parametar);

        context.logger.log("Found data with media objects " + sdata.getData().size());

        ObjectMetadata meta = new ObjectMetadata();

        meta.setContentType("application/json");

        String jsonFileName = "mediaIndex.json";
        String seriazibleString;
        ByteArrayInputStream bais = null;

        try {
            ObjectMapper mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            seriazibleString = mapper.writeValueAsString(sdata.getData());
            bais = new ByteArrayInputStream(seriazibleString.getBytes("UTF-8"));
        } catch (JsonProcessingException | UnsupportedEncodingException e1) {
            context.logger.log(
                    "ERROR CREATE MEDIA INEX FILE");
        }
        Parametar bucketName = pdao.getByCompositeKey(Constants.STAGE_S3_LOCATION, "0");

        S3EventDTO s3EventDTO = new S3EventDTO(context, bucketName.getParametarValue(), productID,
                jsonFileName, bais, meta, S3ActionType.UPLOAD, null, null, null);
        DisneyS3Factory.actionsToS3(s3EventDTO);

    }

    public interface MediaCacheService {
        @LambdaFunction(functionName="GetAllMediaHandlerImpl")
        SearchResponse<SimpleMedia> refreshCacheAndGetData(HashMap<String, String> input);
    }


}
