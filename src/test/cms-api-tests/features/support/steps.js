const { Given, When, Then, setDefaultTimeout } = require("cucumber");
const { expect } = require("chai");
const chai = require("chai");

// chai.use(require("chai-json-pattern"));

setDefaultTimeout(30 * 1000);

When("I do call {string} with data:", async function(req, jsonString) {
    const [method, path] = req.split(":");
    jsonString = this.setVariables(jsonString);
    await this.apiCall(method, path, JSON.parse(jsonString));
});

Then("I receives status code {string}", function(code) {
    expect(this.getLastResponse().status, "to equal", code);
});

Then("The response should contain:", function(jsonString) {
    const lastResponse = this.getLastResponse();

    expect(lastResponse.body).to.be.not.null;

    jsonString = this.setVariables(jsonString);

    const json = JSON.parse(jsonString);

    Object.keys(json).forEach(key => {
        expect(lastResponse.body[key]).to.eql(json[key]);
    });
});

Then("The response should be empty", function() {
    expect(this.getLastResponse().body).to.be.null;
});

Then("Header contains key {string}", function(headerKey) {
    expect(this.getLastResponse().headers).to.have.property(headerKey.toLowerCase());
});

Then("Header contains key {string} and value {string}", function(headerKey, headerValue) {
    const lastResponse = this.getLastResponse();

    const k = headerKey.toLowerCase()
    const v = headerValue.toLowerCase();

    expect(lastResponse.headers).to.have.property(k);
    expect(lastResponse.headers[k].length).to.equal(1);
    expect(lastResponse.headers[k][0]).to.equal(v);
});

Given("I launch application", async function() {
    await this.launchApplication();
    expect(this.user).to.be.not.undefined;
});
