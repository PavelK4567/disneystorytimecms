const { setWorldConstructor } = require("cucumber");
const requester = require("./requester");

class CustomWorld {
    constructor() {
        this.lastResponse = undefined;
        this.user = undefined;
    }

    async apiCall(method, path, body) {
        this.lastResponse = await requester.http(method, path, body);
        return this.lastResponse;
    }

    getLastResponse() {
        return this.lastResponse;
    }

    getRandomDeviceId(){
        const s = "0123456789abcdefghjklmnopqrstABCDEFGHJKLMNOPQRSTXZWYU";
        const dummy = 'auto_test_id_';
        const x = Array(Math.round(17)).join().split(',').map(function () {
            return s.charAt(Math.floor(Math.random() * s.length));
        }).join('');
        return dummy.concat(x);
    }
    async launchApplication() {
        const response = await this.apiCall("POST", "/appuser/launchapplication", {
            "productID": "disneystorytime",
            "deviceID": this.getRandomDeviceId(),//"883f69b1fd9af8caad11fd53e273d78c",
            "ipAddress": "194.183.182.12"
        });

        this.user = {
            appUserID: response.body.appUserID,
            deviceID: response.body.deviceID,
            userType: response.body.userType,
            productID: response.body.productID
        }
    }

    setVariables(template) {
        let m = null;
        const re = /\$\{([^\}]+)\}/gi;

        do {
            m = re.exec(template);
            if (!m) break;

            const param = m[1];
            const parts = param.split(".");

            let val = this;

            for (var i = 0; i < parts.length; i++) {
                const key = parts[i];
                val = val[key];
            }

            if (typeof val === "string" || val instanceof String) {
                val = `"${val}"`;
            }

            template = template.replace(`\$\{${param}\}`, val);
        } while (m);

        return template;
    }
}

setWorldConstructor(CustomWorld);
