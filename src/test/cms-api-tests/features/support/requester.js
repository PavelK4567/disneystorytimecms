const fetch = require("node-fetch");
const buildUrl = require("build-url");

const baseUrl = process.env.KANTOO_CMS_API_URL;

// Perform a HTTP request to the server
async function http(method, url, body = undefined) {
    const realUrl = buildUrl(baseUrl, {
        path: url
    });

    var fetchData = { method: method };

    switch (method) {
        case "POST":
            fetchData.body = JSON.stringify(body);
            fetchData.header = { "Content-Type": "application/json" };
            break;
    }

    const res = await fetch(realUrl, fetchData);
    const json = await res.json();

    return {
        status: res.status,
        headers: res.headers.raw(),
        body: json
    };
}

module.exports = {
    http: http
};
