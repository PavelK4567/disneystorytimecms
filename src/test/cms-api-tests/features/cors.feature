@CORS
Feature: cms.api: CORS

    Scenario Outline: Do API calls and check the response header
        When I do call "<method>:<path>" with data:
            """
            { }
            """
        Then I receives status code "200"
        And Header contains key "Access-Control-Allow-Origin" and value "*"
        And Header contains key "x-amz-apigw-id"

        Examples:
            | method | path                               |
            | POST   | /cmsUser                           |
            | GET    | /cmsUser/1                         |
            | POST   | /cmsUser/deleteCMSUser             |
            | GET    | /cmsUser/getAllCmsUsers            |
            | GET    | /cmsUser/getPassword               |
            | POST   | /cmsUser/login                     |
            | GET    | /commonParametars/getAllParametars |
            | POST   | /games                             |
            | POST   | /labels                            |
            | GET    | /languages                         |
            | GET    | /languages/getAllLanguages         |
            | POST   | /media/edit                        |
            | POST   | /media/edit/batch                  |
            | POST   | /media/filter                      |
            | GET    | /media/info/location               |
            | GET    | /metatag/getAllMetaTags            |
            | GET    | /product/getAllProducts            |

# @wip
# Examples:
#     | method | path                   |
#     | GET    | /commonParametars/test |
#     | GET    | /commonParametars/test_key/test_productId |

