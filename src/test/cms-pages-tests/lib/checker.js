const Joi = require('joi');
const axios = require('axios');
class Checker {

    /**
     * 
     * @param {Sat} errors  array of errors for email message
     * @param {Map} schemaIdPaths  List of paths to schemas files
     * @param {Set} checkedFiles files which already been checked in current iteration
     * @param {string} urlToCheck main url of first document in the queue
     */
    constructor(errors, schemaIdPaths, checkedFiles, urlToCheck) {
        this.errorsSet = errors;
        this.checkedFiles = checkedFiles;
        this.schemaIdPaths = schemaIdPaths;
        this.urlToCheck = urlToCheck;
        this.mediaRoot = undefined;
        this.mapIDUrls = new Map();
    }

    async initialRun() {
        const objectToCheck = await this._downloadFile(this.urlToCheck, 'initial');
        const schemaPath = this.schemaIdPaths.get(objectToCheck.templateID)
        const schema = require(`${process.cwd()}${schemaPath}`);
        this.mediaRoot = objectToCheck.mediaRoot;
        Object.defineProperty(this, 'mediaRoot', { writable: false, value: objectToCheck.mediaRoot });
        this.validate(schema, objectToCheck, this.urlToCheck);
    }

    async run(schema, objectToCheck, fileUrl) {
        await this.validate(schema, objectToCheck, fileUrl);
    }


    /**
     * @description return templatedId for matching schema or null if templateId doesnt present
     */
    getTemplateID(objectToCheck) {
        const obj = objectToCheck;
        if (obj.templateID === void 0) {
            console.log('no templateId finded');
        }
        return obj.templateID !== void 0 ? obj.templateID : null;
    }



    async nestedFullCheck(triemmedValue, folder, parentPageURL) {

        if (this.checkedFiles.has(triemmedValue)) {
            return;
        }
        const objectToCheck = await this._downloadFile(`${this.mediaRoot}/${folder}/${triemmedValue}.json`, parentPageURL);
        if (objectToCheck === void 0) {
            return;
        }


        this.checkedFiles.add(triemmedValue);
        const templateID = this.getTemplateID(objectToCheck);
        const schemaPath = this.schemaIdPaths.get(parseInt(templateID));
        if (schemaPath === void 0) {
            this._fillTheErrors(`Missed schema templateID ${templateID} for validation: ${this.mediaRoot}/${folder}/${triemmedValue}.json <br>`);
            return;
        }
        const schema = require(`${process.cwd()}${schemaPath}`);
        this.validate(schema, objectToCheck, `${this.mediaRoot}/${folder}/${triemmedValue}.json`);


    }

    nestedExistanseCheck(triemmedValue, folder, parentPageURL) {
        if (this.checkedFiles.has(triemmedValue)) {
            return;
        }
        this.checkedFiles.add(triemmedValue);
        const url = `${this.mediaRoot}/${folder}/${triemmedValue}`;
        axios({
            method: 'HEAD',
            url
        }).then(response => {
            if (response.headers['content-length'] === 0) {
                const message = `Empty content here: ${url} <br>`;
                this._fillTheErrors(message);
            }
        }).catch(error => {
            const message = `Content existence Error  --  Error: ${error} --- url: ${url}  parent page is: ${parentPageURL} <br>`;
            this._fillTheErrors(message);
        });
    }

    async questionsListCheck(triemmedValue, folder, parentPageURL) {
        const objectToCheck = await this._downloadFile(`${this.mediaRoot}/${folder}/${triemmedValue}.json`, parentPageURL);
        if (objectToCheck === void 0) return;
        if (this.checkedFiles.has(triemmedValue)) {
            return;
        }
        this.checkedFiles.add(triemmedValue);

        const templateID = 5000;
        const schemaPath = this.schemaIdPaths.get(parseInt(templateID));

        if (schemaPath === void 0) {
            return;
        }
        const schema = require(`${process.cwd()}${schemaPath}`);
        this.run(schema, objectToCheck, `${this.mediaRoot}/page/${triemmedValue}.json`);
    }

    async vocabsListCheck(triemmedValue, folder, parentPageURL) {
        if (this.checkedFiles.has(triemmedValue)) {
            return;
        }
        const objectToCheck = await this._downloadFile(`${this.mediaRoot}/${folder}/${triemmedValue}.json`, parentPageURL);
        if (objectToCheck === void 0) return;

        this.checkedFiles.add(triemmedValue);

        const templateID = 5001;
        const schemaPath = this.schemaIdPaths.get(parseInt(templateID));

        if (schemaPath === void 0) {
            return;
        }
        const schema = require(`${process.cwd()}${schemaPath}`);
        this.run(schema, objectToCheck, `${this.mediaRoot}/page/${triemmedValue}.json`);
    }

    /**
     * 
     * @param {*} checkingType 
     * @param {*} triemmedValue 
     * @param {*} folder 
     * @param {*} parentPageURL 
     */
    nestedChecker(checkingType, triemmedValue, folder, parentPageURL) {
        if (!triemmedValue.length) return;
        const checkingTypes = {
            nestedExistenceCheck: () => this.nestedExistanseCheck(triemmedValue, folder, parentPageURL),
            nestedFullCheck: () => this.nestedFullCheck(triemmedValue, folder, parentPageURL),
            questionsListCheck: () => this.questionsListCheck(triemmedValue, folder),
            vocabsListCheck: () => this.vocabsListCheck(triemmedValue, folder)
        }
        return checkingTypes[checkingType]()
    }
    /**
     * 
     * @param {Object} schema rules
     * @param {Object} objectToCheck downloaded object
     * @param {string} fileUrl source url
     */
    validate(schema, objectToCheck, fileUrl) {
        let joiSchema = this.schemaMaker(schema, fileUrl, objectToCheck.pageID);

        Joi.validate(objectToCheck, joiSchema, { abortEarly: false, stripUnknown: true, convert: false, context: { fileUrl: fileUrl } }, (error, value) => {
            if (error) {
                error.message = `~~~Error in JSON file: <br /> ${objectToCheck.templateID ? `templateID: ${objectToCheck.templateID}<br />` : ''} JSON link: ${fileUrl} <br /> Issues: <br /> <ul>${error.message.split('.').map(i => `<li>${i}</li>`).join('')}</ul> <br /> errorEndsHere`
                const regExp = /(?<=~~~)((.|\n|\r)*?)(?=errorEndsHere)/g;
                let message = error.message.match(regExp);
                this._fillTheErrors(message)
            }
        });
    }

    _fillTheErrors(errors) {
        this.errorsSet.add(errors);
    }

    /**
     * @param {string} url
     * @param {string} parentPageURL
     * @param {string} mainUrl
     * @param {string} funcCaller
     */
    _downloadFile(url, parentPageURL) {
        return axios.get(encodeURI(url)).then(({ data }) => {
            return data
        }).catch(() => {
            const message = `Cant download file on url: ${url}  <br/> Parent page is: ${parentPageURL}`;
            this._fillTheErrors(message);
        });
    }

    /**
     * 
     * @param {Object} schema JSON schema
     * @param {string} fileUrl 
     * @param {string} pageID
     */
    schemaMaker(schema, fileUrl, pageID) {
        const mediaRoot = this.mediaRoot;
        const self = this;
        this.mapIDUrls.set(pageID, fileUrl);
        const customJoi = () => {
            return Joi.extend((joi) => ({
                base: joi.string(),
                name: 'contentUrl',
                language: {
                    isContentExists: 'content should exists at url {{q}}',
                    stringType: 'content url should be a string'
                },
                rules: [
                    {
                        name: 'targetPageUrl',
                        validate(params, value, state, options) {
                            if (typeof value !== 'string') {
                                return this.createError('contentUrl.stringType', { v: value, q: params.q }, state, options);
                            }
                            let triemmedValue = value;
                            const folder = 'page';
                            self.nestedChecker('nestedFullCheck', triemmedValue, folder, options.context.fileUrl);
                        }
                    },
                    {
                        name: 'isImageContentExists',
                        validate(params, value, state, options) {
                            if (typeof value !== 'string') {
                                return this.createError('contentUrl.stringType', { v: value, q: params.q }, state, options);
                            }
                            let triemmedValue = value.trim();
                            const folder = 'image';
                            if (triemmedValue.length > 0) {
                                self.nestedChecker('nestedExistenceCheck', triemmedValue, folder, options.context.fileUrl);
                            }
                        }
                    },
                    {
                        name: 'isAudioContentExists',
                        validate(params, value, state, options) {

                            if (typeof value !== 'string') {
                                return this.createError('contentUrl.stringType', { v: value, q: params.q }, state, options);
                            }
                            let triemmedValue = value.trim();
                            const folder = 'audio';
                            if (triemmedValue.length > 0) {
                                self.nestedChecker('nestedExistenceCheck', triemmedValue, folder, options.context.fileUrl);
                            }
                        }
                    },
                    {
                        name: 'isVideoContentExists',
                        validate(params, value, state, options) {

                            if (typeof value !== 'string') {
                                return this.createError('contentUrl.stringType', { v: value, q: params.q }, state, options);
                            }
                            let triemmedValue = value.trim();
                            const folder = 'video';
                            if (triemmedValue.length > 0) {
                                self.nestedChecker('nestedExistenceCheck', triemmedValue, folder, options.context.fileUrl);
                            }

                        }
                    },
                    {
                        name: 'questionsListUrl',
                        validate(params, value, state, options) {

                            if (typeof value !== 'string') {
                                return this.createError('contentUrl.stringType', { v: value, q: params.q }, state, options);
                            }
                            let triemmedValue = value.trim();
                            const folder = 'page';
                            self.nestedChecker('questionsListCheck', triemmedValue, folder, options.context.fileUrl);
                        }
                    },
                    {
                        name: 'vocabsListUrl',
                        validate(params, value, state, options) {

                            if (typeof value !== 'string') {
                                return this.createError('contentUrl.stringType', { v: value, q: params.q }, state, options);
                            }
                            let triemmedValue = value.trim();
                            const folder = 'page';
                            self.nestedChecker('vocabsListCheck', triemmedValue, folder, options.context.fileUrl);
                        }
                    }
                ]
            }))
        };

        /**
         * @description updates incoming joischema if property `required` existed and true in schema 
         * @param {Object} schema 
         * @param {Joi} joischema 
         */
        const isRequired = (schema, joischema) => {
            if (schema.required) {
                joischema = joischema.required();
            }

            return joischema;
        }

        /**
         * @description updates incoming joischema if property `allowEmpty` existed and true in schema 
         * @param {Object} schema 
         * @param {Joi} joischema 
         */
        const isEmptyAllowed = (schema, joischema) => {
            if (schema.allowEmpty) {
                joischema = joischema.allow("");
            }

            return joischema;
        }

        const arrayLength = (schema, joischema) => {
            if (schema.minLength) {
                joischema = joischema.min(schema.minLength);
            }

            return joischema;
        }

        const _string = (schema) => {
            let joischema = Joi.string()
            joischema = isRequired(schema, joischema);
            joischema = isEmptyAllowed(schema, joischema);

            return joischema
        }

        const _number = (schema) => {
            let joischema = Joi.number()
            joischema = isRequired(schema, joischema);

            return joischema
        }

        const _boolean = (schema) => {
            let joischema = Joi.boolean();
            joischema = isRequired(schema, joischema);

            return joischema;
        }

        const _object = (schema) => {
            let joischema = Joi.object();
            joischema = isRequired(schema, joischema);

            if (schema.properties && !!Object.keys(schema.properties).length) {
                let properties = resolveType(schema.properties);
                joischema = joischema.keys(properties);
            }

            if (schema.rename && !!Object.keys(schema.rename).length) {
                joischema = joischema.rename('LabelID', 'labelID', { override: true, ignoreUndefined: true });
            }
            return joischema
        }

        const _array = (schema) => {
            let joischema = Joi.array();
            joischema = isRequired(schema, joischema);
            joischema = arrayLength(schema, joischema);
            if (schema.items && !!Object.keys(schema.items).length) {
                const items = Object.keys(schema.items).map(() => {
                    const i = resolveType(schema.items);
                    return i.item;
                })
                joischema = joischema.items(items);
            }
            return joischema
        }

        const _imageUrl = (schema, fileUrl) => {
            let joischema = customJoi().contentUrl().isImageContentExists()
            joischema = isEmptyAllowed(schema, joischema);
            return joischema
        }

        const _questionsListUrl = (schema, fileUrl) => {
            let joischema = customJoi().contentUrl().questionsListUrl()
            joischema = isEmptyAllowed(schema, joischema);
            return joischema
        }

        const _vocabsListUrl = (schema, fileUrl) => {
            let joischema = customJoi().contentUrl().vocabsListUrl()
            joischema = isEmptyAllowed(schema, joischema);
            return joischema
        }

        const _audioUrl = (schema, fileUrl) => {
            let joischema = customJoi().contentUrl().isAudioContentExists()
            joischema = isEmptyAllowed(schema, joischema);
            return joischema
        }

        const _videoUrl = (schema, fileUrl) => {
            let joischema = customJoi().contentUrl().isVideoContentExists()
            joischema = isEmptyAllowed(schema, joischema);
            return joischema
        }

        const _targetPageUrl = (schema, fileUrl) => {
            let joischema = customJoi().contentUrl().targetPageUrl();
            joischema = isEmptyAllowed(schema, joischema);
            return joischema
        }

        const _mediaRoot = (schema) => {
            let joischema = Joi.string(mediaRoot)
            joischema = isRequired(schema, joischema);
            joischema = isEmptyAllowed(schema, joischema);
            return joischema
        }

        let resolveType = (schema) => {
            Object.keys(schema).map(item => {
                const type = schema[item]['type'];
                switch (type) {
                    case 'array':
                        schema[item] = _array(schema[item]);
                        break;
                    case 'string':
                        schema[item] = _string(schema[item]);
                        break;
                    case 'object':
                        schema[item] = _object(schema[item]);
                        break;
                    case 'number':
                        schema[item] = _number(schema[item]);
                        break;
                    case 'boolean':
                        schema[item] = _boolean(schema[item]);
                        break;
                    case 'imageUrl':
                        schema[item] = _imageUrl(schema[item], fileUrl);
                        break;
                    case 'audioUrl':
                        schema[item] = _audioUrl(schema[item], fileUrl);
                        break;
                    case 'videoUrl':
                        schema[item] = _videoUrl(schema[item], fileUrl);
                        break;
                    case 'targetPageUrl':
                        schema[item] = _targetPageUrl(schema[item], fileUrl);
                        break;
                    case 'mediaRoot':
                        schema[item] = _mediaRoot(schema[item], mediaRoot);
                        break;
                    case 'questionsListUrl':
                        schema[item] = _questionsListUrl(schema[item], fileUrl);
                        break;
                    case 'vocabsListUrl':
                        schema[item] = _vocabsListUrl(schema[item], fileUrl);
                        break;
                }
            });

            return schema;
        }
        let resolved = resolveType(schema);
        return Joi.object().keys(resolved);
    }
}

module.exports = Checker;   