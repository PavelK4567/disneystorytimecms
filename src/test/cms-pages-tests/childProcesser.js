"use strict";
const AWS = require('aws-sdk');

AWS.config.update({
    region: "eu-west-1"
});

const ses = new AWS.SES({ apiVersion: '2010-12-01' });
const { spawn } = require('child_process');
const MAIN_EMAIL_RECIPIENTS = process.env.MAIN_EMAIL_RECIPIENTS;
const EMAIL_SENDER = process.env.EMAIL_SENDER;
let environment = {};
const folders = [
        { name: "disneystorytime-pt", appName: "disneystorytime" }, 
        { name: "disneystorytime-es", appName: "disneystorytime" }, 
        { name: "magicalenglish-pt", appName: "magicalenglish" },
        { name: "magicalenglish-es", appName: "magicalenglish" }
    ];
const env_pages = process.env.ENV_NAME === 'prod' ? 'ireland-prod-staged' : 'ireland-qa-staged';
const defaultScenariosUrls = new Map([
    ["UserTypeSelectorPage_w", `https://s3-eu-west-1.amazonaws.com/${env_pages}/{language_folder}/page/UserTypeSelectorPage_c.json`],
    ["UserTypeSelectorPage_c", `https://s3-eu-west-1.amazonaws.com/${env_pages}/{language_folder}/page/UserTypeSelectorPage_w.json`]
]);

const sendEmail = async (message) => {
    let ses_mail = "From: 'Disneystorytime CMS pages test CI' <" + EMAIL_SENDER + ">\n";
    ses_mail += "To: " + MAIN_EMAIL_RECIPIENTS + "\n";
    ses_mail += "Subject: CMS pages test notification - tests Failed\n";
    ses_mail += "MIME-Version: 1.0\n";
    ses_mail += 'Content-Type: multipart/mixed; boundary="NextPart"\n\n';
    ses_mail += "--NextPart\n";
    ses_mail += 'Content-Disposition: attachment; filename="cms_pages_errors.html"\n\n';
    ses_mail += message;
    ses_mail += "--NextPart--";

    const params = {
      RawMessage: { Data: new Buffer(ses_mail) },
      Destinations: MAIN_EMAIL_RECIPIENTS.split(','),
      Source: EMAIL_SENDER
    };
    ses.sendRawEmail(params, (error, data) => {
      if (error) {
        console.log(`Error while sending email occurs: ${error}`);
      } else {
        console.log(data);
      }
    });
};

const links = [];
const validationErrors = [];
function processesMaker(defaultScenariosUrls, folders) {
    for (const value of defaultScenariosUrls.values()) {
        for (const folder of folders) {
            let modifiedUrl = value.replace('{language_folder}', folder.name);
            links.push({ url: modifiedUrl, applicationName: folder.appName });
        }
    }

    launcher(links);
}

processesMaker(defaultScenariosUrls, folders);

function launcher(links) {
    if (links.length > 0) {
        const link = links.pop()
        environment.processUrl = link.url;
        environment.mediaRoot = link.url.replace('/page/UserTypeSelectorPage_c.json', '').replace('/page/UserTypeSelectorPage_w.json', '')
        environment.applicationName = link.applicationName;
        let child = spawn('node', ['index'], { env: environment })
        console.log(`childprocess spawned with link: ${link.url} and applicationName is ${link.applicationName}`)
        child.on('exit', () => {
            launcher(links);
        });
        child.stdout.on('data', (data) => {
            validationErrors.push(data.toString('utf8').replace(/\\"/g,'"'));
        });
        child.stderr.on('data', (data) => {
            console.log(`child stderr:\n${data}`);
        });
    } else {
        if (validationErrors.length > 0) {
            sendEmail(validationErrors.join());
        }
    }
};