"use strict";
const Checker = require('./lib/checker');

/**
 * https://lamark.atlassian.net/wiki/spaces/PROD/pages/241008734/Simple+CMS+Page+templates+-+NEW
 */
const schemaIdPaths = new Map([
    [100, "/lib/schemas/HomePageThreeItemsSchema.json"],
    [101, "/lib/schemas/HomePageThreeItemsSchema.json"],
    [200, "/lib/schemas/VideoPageSchema.json"],
    [300, "/lib/schemas/FranchisePageSchemaTemplateId300.json"],
    [301, "/lib/schemas/FranchisePageSchema.json"],
    [302, "/lib/schemas/FranchisePageSchema.json"],
    [303, "/lib/schemas/FranchisePageSchema.json"],
    [304, "/lib/schemas/FranchisePageSchema.json"],
    [305, "/lib/schemas/FranchisePageSchema.json"],
    [306, "/lib/schemas/FranchisePageSchema.json"],
    [400, "/lib/schemas/PracticePageSchemaTemplateId400.json"],
    [401, "/lib/schemas/PracticePageSchema.json"],
    [402, "/lib/schemas/PracticePageSchema.json"],
    [403, "/lib/schemas/PracticePageSchema.json"],
    [404, "/lib/schemas/PracticePageSchema.json"],
    [405, "/lib/schemas/PracticePageSchema.json"],
    [406, "/lib/schemas/PracticePageSchema.json"],
    [500, "/lib/schemas/ParentsZoneMenuThreeImagesSchema.json"],
    [501, "/lib/schemas/ParentsZonePractiseTogether6ButtonsPageSchema.json"],
    [502, "/lib/schemas/ParentsZonePractiseTogether4ButtonsPageSchema.json"],
    [504, "/lib/schemas/ComingSoonPageSchema.json"],
    [503, "/lib/schemas/ParentsZoneOurVisionPageSchema.json"],
    [505, "/lib/schemas/LegalPageBeforeLoginSchema.json"],
    [506, "/lib/schemas/AgeOptInPageSchema.json"],
    [507, "/lib/schemas/LoginRegularPageSchema.json"],
    [508, "/lib/schemas/LoginAlternativePageSchema.json"],
    [509, "/lib/schemas/SignUpPageSchema.json"],
    [510, "/lib/schemas/ForgotPasswordPageSchema.json"],
    [511, "/lib/schemas/VoucherPageSchema.json"],
    [600, "/lib/schemas/ConversionPageSchemaTemplateId600.json"],
    [601, "/lib/schemas/ConversionPageSchema.json"],
    [700, "/lib/schemas/GameMemoryPageSchemaTemplateId700.json"],
    [701, "/lib/schemas/GameMemoryPageSchema.json"],
    [702, "/lib/schemas/GameMemoryPageSchema.json"],
    [703, "/lib/schemas/GameMemoryPageSchema.json"],
    [704, "/lib/schemas/GameMemoryPageSchema.json"],
    [705, "/lib/schemas/GameMemoryPageSchema.json"],
    [706, "/lib/schemas/GameMemoryPageSchema.json"],
    [801, "/lib/schemas/SettingsPageSchema.json"],
    [803, "/lib/schemas/UserTypeSelectorPageSchema.json"],
    [5000, "/lib/schemas/QuestionsListPageSchema.json"],
    [5001, "/lib/schemas/VocabsListPageSchema.json"]
]);

const checkedFiles = new Set();
const errors = new Set();



const singleProcessSpawner = () => {
    const checker = new Checker(errors, schemaIdPaths, checkedFiles, process.env.processUrl);
    checker.initialRun();
}

singleProcessSpawner();

process.on('error', (error => {
    console.log(error, ' \n Error')
}))
process.on('beforeExit', () => {
    const arrayErrors = [`<b>Application ${process.env.applicationName}</b><br>`, ...errors,`<br>`]
    console.log(JSON.stringify(arrayErrors.join('<hr>')));
    process.exit(0);
});