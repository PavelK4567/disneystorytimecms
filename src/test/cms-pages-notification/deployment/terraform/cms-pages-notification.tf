resource "aws_lambda_function" "cms_pages_notification" {
  function_name     = "cms_pages_notification"
  handler           = "index.handler"
  role              = "arn:aws:iam::${var.aws_account_id}:role/LamarkLambdaBasicExecution"
  runtime           = "${var.runtime_nodejs8_10}"
  timeout           = "60"
  memory_size       = "512"


  filename          = "${var.filename_cms_pages_notification}"
  source_code_hash  = "${md5(file("${var.filename_cms_pages_notification}"))}"

}

# Cloudwatch Event
resource "aws_cloudwatch_event_rule" "cms_pages_notification" {
  name                = "cms_pages_notification_event"
  description         = "Every day"
  schedule_expression = "cron(00 21 * * ? *)"
}

resource "aws_cloudwatch_event_target" "cms_pages_notification" {
  rule            = "${aws_cloudwatch_event_rule.cms_pages_notification.name}"
  target_id       = "${aws_lambda_function.cms_pages_notification.function_name}"
  arn             = "${aws_lambda_function.cms_pages_notification.arn}"
}




resource "aws_lambda_permission" "cms_pages_notification" {
  action          = "lambda:InvokeFunction"
  function_name   = "${aws_lambda_function.cms_pages_notification.function_name}"
  principal       = "events.amazonaws.com"
  source_arn      = "${aws_cloudwatch_event_rule.cms_pages_notification.arn}"

}
