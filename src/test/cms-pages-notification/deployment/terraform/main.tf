variable "aws_account_id" {}
variable "aws_region" {}
variable "aws_access_key" {}
variable "aws_secret_key" {}
variable "build_number" { default = "1" }

variable "runtime_nodejs8_10" { default = "nodejs8.10" }

variable "filename_cms_pages_notification" { default = "../../kantoo.api.cms.pages.notification.zip" }

provider "aws" {
  version               = "~> 1.46.0"
  region                = "${var.aws_region}"
  access_key            = "${var.aws_access_key}"
  secret_key            = "${var.aws_secret_key}"
  allowed_account_ids   = ["${var.aws_account_id}"]
}
