const AWS = require('aws-sdk');

const DEV_BUCKET_NAME = "ireland-dev-staged";
const QA_BUCKET_NAME = "ireland-qa-staged";
const PROD_BUCKET_NAME = "ireland-prod-staged";

const s3 = new AWS.S3({
    region: process.env.AU_AWS_REGION,
    accessKeyId: process.env.AU_AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AU_AWS_SECRET_ACCESS_KEY
});

const ses = new AWS.SES({apiVersion: '2010-12-01'});


let changedFiles = {QA: [], PROD: []};
let bucketPermissions = [];

const getChangedFiles = async (bucketName, directory) => {
    const s3_params = {
        Bucket: bucketName,
        Delimiter: '/',
        Prefix: directory
    };

    try {
        const objectsList = await s3.listObjects(s3_params).promise();
        const contents = objectsList["Contents"];
        contents.map(value => {
            if (bucketName === QA_BUCKET_NAME) {
                if (isChangedInPast24(value["LastModified"])) {
                    changedFiles["QA"].push({File: value["Key"], LastModified: (value["LastModified"])});
                }
            } else if (bucketName === PROD_BUCKET_NAME) {
                if (isChangedInPast24(value["LastModified"])) {
                    changedFiles["PROD"].push({File: value["Key"], LastModified: (value["LastModified"])});
                }
            }
        });
    } catch (e) {
        console.log("Did not get objects", e.toString())
    }

};


const updateBucketPermission = async (bucketName, key) => {
    try {
            const response = await s3.getBucketAcl({Bucket: bucketName}).promise();
            console.log("Response from  " + bucketName +  " " + JSON.stringify(response));
            const grants = response["Grants"];
            const isReadAccessExists = isAccessExists("Group", "READ", grants);
            const isWriteAccessExists = isAccessExists("Group", "WRITE", grants);

            let isPermissionChanged = false;

            if (!isReadAccessExists) {
                const readPermission = formGrant("Group", "READ");
                grants.push(readPermission);
                isPermissionChanged = true;
            }

            if (!isWriteAccessExists) {
                const readPermission = formGrant("Group", "WRITE");
                grants.push(readPermission);
                isPermissionChanged = true;
            }

            if (isPermissionChanged) {
                response["Grants"] = grants;
                bucketPermissions.push({bucket: bucketName, permissionChanged: true});
                const params = formAclParam(bucketName,response);
                console.log("Updating permission of " + bucketName + " with params = " + JSON.stringify(params));
                await putAcl(params);
            }
    } catch (e) {
        console.log("Cannot get alc of " + key, e.toString())
    }
};





const putAcl = async (params) => {
    try {
        await s3.putBucketAcl(params).promise();
        console.log("Permission granted!")
    } catch (e) {
        console.log("Cannot put permission", e.toString())
    }
};

const formGrant = (type, permission) => {
    return {
        Grantee: {
            Type: type,
            URI: "http://acs.amazonaws.com/groups/global/AllUsers"
        },
        Permission: permission
    }
};

const formGrantFullControl = (type, permission, displayName, id) => {
    return {
        Grantee: {
            DisplayName: displayName,
            ID: id,
            Type: type
        },
        Permission: permission
    }
};

const formAclParam = (bucket, accessControlPolicy) => {
    return {
        Bucket: bucket,
        AccessControlPolicy: accessControlPolicy
    }
}

const isAccessExists = (type, permission, allPermissions) => {
    let isExists = false;
    allPermissions.map(value => {
        if (type === value["Grantee"]["Type"] && value["Permission"] === permission) {
            isExists = true;
        }
    });
    return isExists;
};


const isChangedInPast24 = (modifiedDate) => {
    const yesterday = new Date();
    yesterday.setDate(new Date().getDate() - 1);

    const today = new Date();

    if (modifiedDate >= yesterday && modifiedDate <= today) {
        return true;
    }
    return false;
};


const sendEmail = async (message, emails) => {


    const emailParams = {
        Destination: {
            ToAddresses: emails
        },
        Message: {
            Body: {
                Html: {
                    Charset: "UTF-8",
                    Data: message
                }
            },
            Subject: {
                Charset: 'UTF-8',
                Data: "Daily notification - updated pages in the CMS"
            }
        },
        Source: 'denysenkoa@mydigicode.com',
    };


    try {
        await ses.sendEmail(emailParams).promise();
        console.log("Email was sent");
    } catch (e) {
        console.log("Email was NOT sent", e.toString());
    }
};


const formMessage = () => {
    let message_PROD = "";
    const prodFiles = changedFiles["PROD"];

    if (prodFiles.length > 0) {
        message_PROD = "PROD".bold().concat("<br>");
        prodFiles.map(value => {
            message_PROD = message_PROD + value["File"].toString() + " was updated on " + value["LastModified"] + "<br>"
        });
    }
    let message_QA = "";
    let qaFiles = changedFiles["QA"];
    if (qaFiles.length > 0) {
        message_QA = "QA".bold().concat("<br>");
        qaFiles.map(value => {
            message_QA = message_QA + value["File"].toString() + " was updated on " + value["LastModified"] + "<br>"
        });
    }

    let bucketPermissionMessage = "";
    if (bucketPermissions.length === 0) {
        bucketPermissionMessage = "Bucket's permissions were not changed"
    } else {
        bucketPermissionMessage = "Permissions of buckets ";
        bucketPermissions.map(value => {
           bucketPermissionMessage = bucketPermissionMessage + value["bucket"] + " "
        });
        bucketPermissionMessage = bucketPermissionMessage + " were changed";
    }



    const message = message_QA + "<br>" + message_PROD + "<br>" + bucketPermissionMessage;
    message.trim();

    console.log("Message ", message);
    return message;
};


const getRecipients = async () => {
    let emails = [];

    try {
        const list = await ses.listIdentities().promise();
        const identities = list["Identities"];

        const verificationInfo = await ses.getIdentityVerificationAttributes({Identities: identities}).promise();
        const verificationAttributes = verificationInfo["VerificationAttributes"];
        const existingEmails = Object.keys(verificationInfo["VerificationAttributes"]);


        existingEmails.map(value => {
            if (verificationAttributes[value]["VerificationStatus"] === "Success") {
                emails.push(value);
            }
        });

        console.log("Emails", JSON.stringify(emails));
        return emails;
    } catch (e) {
        console.log('Impossible to get recipients');
        return emails;
    }

};

exports.handler = async () => {

    await Promise.all([
        getChangedFiles(QA_BUCKET_NAME, "disneystorytime-pt/page/"),
        getChangedFiles(QA_BUCKET_NAME, "disneystorytime-es/page/"),
        getChangedFiles(QA_BUCKET_NAME, "disneystorytime-he/page/"),
        getChangedFiles(PROD_BUCKET_NAME, "disneystorytime-pt/page/"),
        getChangedFiles(PROD_BUCKET_NAME, "disneystorytime-es/page/"),
        getChangedFiles(PROD_BUCKET_NAME, "disneystorytime-he/page/"),
    ]);

    await Promise.all([
        updateBucketPermission(PROD_BUCKET_NAME),
        updateBucketPermission(QA_BUCKET_NAME)
    ]);


    const QA = changedFiles["QA"];
    const PROD = changedFiles["PROD"];
    let message;

    if (QA.length === 0 && PROD.length === 0) {
        message = "No changes detected. You may relax ;)"
    } else {
        message = formMessage();
    }


    const emails = await getRecipients();


    await sendEmail(message, emails);
    console.log("Changed files = ", JSON.stringify(changedFiles));
    changedFiles = {QA: [], PROD: []};
    bucketPermissions = [];
};









